package com.zeyu.framework.monitors.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.core.persistence.entity.SimpleEntity;

import java.util.Date;

/**
 * 每个CPU的详细信息.
 *
 * @author zeyuphoenix
 */
public class CPUStatus extends SimpleEntity<CPUStatus> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 所属的Server
    private ServerStatus serverStatus;

    // 收集时间
    /** The time. */
    private Date time;
    // cpu的索引
    private int ind;

    // 变化信息
    // CPU总使用率 DB
    private float cpuRatio;
    // CPU系统使用率 DB
    private float cpuSysRatio;
    // CPU用户使用率 DB
    private float cpuUsrRatio;

    // 不变信息
    // 模式
    private String model;
    // 主频
    private String mhz;
    // 厂家
    private String vendor;
    // 缓冲存储器数量
    private String cacheSize;

    // 查询参数
    // 开始日期
    private Date beginDate;
    // 结束日期
    private Date endDate;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getInd() {
        return ind;
    }

    public void setInd(int ind) {
        this.ind = ind;
    }

    public float getCpuRatio() {
        return cpuRatio;
    }

    public void setCpuRatio(float cpuRatio) {
        this.cpuRatio = cpuRatio;
    }

    public float getCpuSysRatio() {
        return cpuSysRatio;
    }

    public void setCpuSysRatio(float cpuSysRatio) {
        this.cpuSysRatio = cpuSysRatio;
    }

    public float getCpuUsrRatio() {
        return cpuUsrRatio;
    }

    public void setCpuUsrRatio(float cpuUsrRatio) {
        this.cpuUsrRatio = cpuUsrRatio;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMhz() {
        return mhz;
    }

    public void setMhz(String mhz) {
        this.mhz = mhz;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(String cacheSize) {
        this.cacheSize = cacheSize;
    }

    @JsonIgnore
    public ServerStatus getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(ServerStatus serverStatus) {
        this.serverStatus = serverStatus;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
