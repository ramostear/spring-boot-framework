package com.zeyu.framework.monitors.mongodb.entity;

import com.zeyu.framework.core.persistence.entity.SimpleEntity;

/**
 * MongoDB的每个collection的状态.
 *
 * @author zeyuphoenix
 */
public class CollectionStatus extends SimpleEntity {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // collection名称
    private String collName;
    // 命名空间
    private String ns;
    // 记录数
    private long count;
    // 大小(B-KB-MB-GB)
    private long size;
    // 索引数
    private long indexes;
    // 事件数
    private long numExtents;
    // Collection状态
    private boolean status;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the coll name.
     *
     * @return the coll name
     */
    public String getCollName() {
        return collName;
    }

    /**
     * Sets the coll name.
     *
     * @param collName
     *            the new coll name
     */
    public void setCollName(String collName) {
        this.collName = collName;
    }

    /**
     * Gets the ns.
     *
     * @return the ns
     */
    public String getNs() {
        return ns;
    }

    /**
     * Sets the ns.
     *
     * @param ns
     *            the new ns
     */
    public void setNs(String ns) {
        this.ns = ns;
    }

    /**
     * Gets the count.
     *
     * @return the count
     */
    public long getCount() {
        return count;
    }

    /**
     * Sets the count.
     *
     * @param count
     *            the new count
     */
    public void setCount(long count) {
        this.count = count;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public long getSize() {
        return size;
    }

    /**
     * Sets the size.
     *
     * @param size
     *            the new size
     */
    public void setSize(long size) {
        this.size = size;
    }

    /**
     * Gets the indexes.
     *
     * @return the indexes
     */
    public long getIndexes() {
        return indexes;
    }

    /**
     * Sets the indexes.
     *
     * @param indexes
     *            the new indexes
     */
    public void setIndexes(long indexes) {
        this.indexes = indexes;
    }

    /**
     * Gets the num extents.
     *
     * @return the num extents
     */
    public long getNumExtents() {
        return numExtents;
    }

    /**
     * Sets the num extents.
     *
     * @param numExtents
     *            the new num extents
     */
    public void setNumExtents(long numExtents) {
        this.numExtents = numExtents;
    }

    /**
     * Checks if is status.
     *
     * @return true, if is status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
