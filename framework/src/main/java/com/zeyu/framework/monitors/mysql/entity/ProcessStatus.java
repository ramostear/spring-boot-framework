package com.zeyu.framework.monitors.mysql.entity;

import com.zeyu.framework.core.persistence.entity.SimpleEntity;

/**
 * Mysql的连接状态，不保存数据库，实时取得.
 *
 * @author zeyuphoenix
 */
public class ProcessStatus extends SimpleEntity {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 进程id
    private long processId;
    // 连接数据库的名称
    private String user;
    // 连接数据库的主机
    private String host;
    // 连接的数据库的名称
    private String dbName;
    // 执行的命令
    private String command;
    // 连接时间
    private long time;
    // 连接状态
    private String state;
    // 详细信息
    private String info;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the id.
     *
     * @return the id
     */
    public long getProcessId() {
        return processId;
    }

    /**
     * Sets the id.
     *
     * @param processId
     *            the new id
     */
    public void setProcessId(long processId) {
        this.processId = processId;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user
     *            the new user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * Gets the host.
     *
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * Sets the host.
     *
     * @param host
     *            the new host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Gets the db name.
     *
     * @return the db name
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * Sets the db name.
     *
     * @param dbName
     *            the new db name
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
        return command;
    }

    /**
     * Sets the command.
     *
     * @param command
     *            the new command
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * Gets the state.
     *
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state.
     *
     * @param state
     *            the new state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Gets the info.
     *
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * Sets the info.
     *
     * @param info
     *            the new info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
