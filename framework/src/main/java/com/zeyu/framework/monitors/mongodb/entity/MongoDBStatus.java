package com.zeyu.framework.monitors.mongodb.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.core.persistence.entity.SimpleEntity;

import java.util.Date;

/**
 * MongoDB的整体状态监控.
 *
 * @author zeyuphoenix
 */
public class MongoDBStatus extends SimpleEntity<MongoDBStatus> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    //收集时间
    /** The time. */
    private Date time;

    // MongoDB状态
    /** The status. */
    private boolean status;

    // 这些项目不会根据时间进行变化，每次取得，不保存数据库，节省数据库空间
    // 数据库IP
    /** The host. */
    private String host;
    // 端口
    /** The port. */
    private int port;
    // 数据库版本
    /** The version. */
    private String version;
    // 主机名称
    /** The host name. */
    private String hostName;
    // 进程Id
    /** The pid. */
    private int pid;
    // 进程名
    /** The process. */
    private String process;
    // 持续运行时间(s)
    /** The uptime. */
    private long uptime;

    /**  内存相关. */
    // 使用的物理内存大小(m)
    private long memResident;
    // 使用的虚拟内存大小(m)
    /** The mem virtual. */
    private long memVirtual;
    // 映射的内存大小(m)
    /** The mem mapped. */
    private long memMapped;
    // 具有日志的映射的内存大小
    /** The mem mapped with journal. */
    private long memMappedWithJournal;

    /**  连接相关. */
    // 当前连接数
    private int connectionsCurrent;
    // 可用连接数
    /** The connections available. */
    private int connectionsAvailable;

    /**  加载错误相关. */
    // 加载磁盘内容时发生页错误的次数
    private long extraInfoPageFaults;

    /**  流量相关. */
    // 网络读取字节数(byte)
    private long networkBytesIn;
    // 网络发送字节数(byte)
    /** The network bytes out. */
    private long networkBytesOut;

    /**  请求相关. */
    // 网络请求数
    private long networkNumRequests;

    /**  操作相关. */
    // 插入操作数
    private long opcountersInsert;
    // 查询操作数
    /** The opcounters query. */
    private long opcountersQuery;
    // 更新操作数
    /** The opcounters update. */
    private long opcountersUpdate;
    // 删除操作数
    /** The opcounters delete. */
    private long opcountersDelete;
    // 其它操作数
    /** The opcounters command. */
    private long opcountersCommand;

    /**  索引相关. */
    // 访问索引次数
    private long indexCountersAccesses;
    // 内存命中索引次数
    /** The index counters hits. */
    private long indexCountersHits;
    // 内存丢失索引次数
    /** The index counters misses. */
    private long indexCountersMisses;
    // 索引计数器重置次数
    /** The index counters resets. */
    private long indexCountersResets;

    // 查询参数
    // 开始日期
    private Date beginDate;
    // 结束日期
    private Date endDate;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the host.
     *
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * Sets the host.
     *
     * @param host
     *            the new host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Gets the port.
     *
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * Sets the port.
     *
     * @param port
     *            the new port
     */
    public void setPort(int port) {
        this.port = port;
    }


    /**
     * Gets the time.
     *
     * @return the time
     */
    public Date getTime() {
        return time;
    }

    /**
     * Sets the time.
     *
     * @param time the new time
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the new version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Gets the host name.
     *
     * @return the host name
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Sets the host name.
     *
     * @param hostName
     *            the new host name
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * Gets the pid.
     *
     * @return the pid
     */
    public int getPid() {
        return pid;
    }

    /**
     * Sets the pid.
     *
     * @param pid
     *            the new pid
     */
    public void setPid(int pid) {
        this.pid = pid;
    }

    /**
     * Gets the process.
     *
     * @return the process
     */
    public String getProcess() {
        return process;
    }

    /**
     * Sets the process.
     *
     * @param process
     *            the new process
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * Gets the uptime.
     *
     * @return the uptime
     */
    public long getUptime() {
        return uptime;
    }

    /**
     * Sets the uptime.
     *
     * @param uptime
     *            the new uptime
     */
    public void setUptime(long uptime) {
        this.uptime = uptime;
    }

    /**
     * Gets the mem resident.
     *
     * @return the mem resident
     */
    public long getMemResident() {
        return memResident;
    }

    /**
     * Sets the mem resident.
     *
     * @param memResident
     *            the new mem resident
     */
    public void setMemResident(long memResident) {
        this.memResident = memResident;
    }

    /**
     * Gets the mem virtual.
     *
     * @return the mem virtual
     */
    public long getMemVirtual() {
        return memVirtual;
    }

    /**
     * Sets the mem virtual.
     *
     * @param memVirtual
     *            the new mem virtual
     */
    public void setMemVirtual(long memVirtual) {
        this.memVirtual = memVirtual;
    }

    /**
     * Gets the mem mapped.
     *
     * @return the mem mapped
     */
    public long getMemMapped() {
        return memMapped;
    }

    /**
     * Sets the mem mapped.
     *
     * @param memMapped
     *            the new mem mapped
     */
    public void setMemMapped(long memMapped) {
        this.memMapped = memMapped;
    }

    /**
     * Gets the mem mapped with journal.
     *
     * @return the mem mapped with journal
     */
    public long getMemMappedWithJournal() {
        return memMappedWithJournal;
    }

    /**
     * Sets the mem mapped with journal.
     *
     * @param memMappedWithJournal
     *            the new mem mapped with journal
     */
    public void setMemMappedWithJournal(long memMappedWithJournal) {
        this.memMappedWithJournal = memMappedWithJournal;
    }

    /**
     * Gets the connections current.
     *
     * @return the connections current
     */
    public int getConnectionsCurrent() {
        return connectionsCurrent;
    }

    /**
     * Sets the connections current.
     *
     * @param connectionsCurrent
     *            the new connections current
     */
    public void setConnectionsCurrent(int connectionsCurrent) {
        this.connectionsCurrent = connectionsCurrent;
    }

    /**
     * Gets the connections available.
     *
     * @return the connections available
     */
    public int getConnectionsAvailable() {
        return connectionsAvailable;
    }

    /**
     * Sets the connections available.
     *
     * @param connectionsAvailable
     *            the new connections available
     */
    public void setConnectionsAvailable(int connectionsAvailable) {
        this.connectionsAvailable = connectionsAvailable;
    }

    /**
     * Gets the extra info page faults.
     *
     * @return the extra info page faults
     */
    public long getExtraInfoPageFaults() {
        return extraInfoPageFaults;
    }

    /**
     * Sets the extra info page faults.
     *
     * @param extraInfoPageFaults
     *            the new extra info page faults
     */
    public void setExtraInfoPageFaults(long extraInfoPageFaults) {
        this.extraInfoPageFaults = extraInfoPageFaults;
    }

    /**
     * Gets the network bytes in.
     *
     * @return the network bytes in
     */
    public long getNetworkBytesIn() {
        return networkBytesIn;
    }

    /**
     * Sets the network bytes in.
     *
     * @param networkBytesIn
     *            the new network bytes in
     */
    public void setNetworkBytesIn(long networkBytesIn) {
        this.networkBytesIn = networkBytesIn;
    }

    /**
     * Gets the network bytes out.
     *
     * @return the network bytes out
     */
    public long getNetworkBytesOut() {
        return networkBytesOut;
    }

    /**
     * Sets the network bytes out.
     *
     * @param networkBytesOut
     *            the new network bytes out
     */
    public void setNetworkBytesOut(long networkBytesOut) {
        this.networkBytesOut = networkBytesOut;
    }

    /**
     * Gets the network num requests.
     *
     * @return the network num requests
     */
    public long getNetworkNumRequests() {
        return networkNumRequests;
    }

    /**
     * Sets the network num requests.
     *
     * @param networkNumRequests
     *            the new network num requests
     */
    public void setNetworkNumRequests(long networkNumRequests) {
        this.networkNumRequests = networkNumRequests;
    }

    /**
     * Gets the opcounters insert.
     *
     * @return the opcounters insert
     */
    public long getOpcountersInsert() {
        return opcountersInsert;
    }

    /**
     * Sets the opcounters insert.
     *
     * @param opcountersInsert
     *            the new opcounters insert
     */
    public void setOpcountersInsert(long opcountersInsert) {
        this.opcountersInsert = opcountersInsert;
    }

    /**
     * Gets the opcounters query.
     *
     * @return the opcounters query
     */
    public long getOpcountersQuery() {
        return opcountersQuery;
    }

    /**
     * Sets the opcounters query.
     *
     * @param opcountersQuery
     *            the new opcounters query
     */
    public void setOpcountersQuery(long opcountersQuery) {
        this.opcountersQuery = opcountersQuery;
    }

    /**
     * Gets the opcounters update.
     *
     * @return the opcounters update
     */
    public long getOpcountersUpdate() {
        return opcountersUpdate;
    }

    /**
     * Sets the opcounters update.
     *
     * @param opcountersUpdate
     *            the new opcounters update
     */
    public void setOpcountersUpdate(long opcountersUpdate) {
        this.opcountersUpdate = opcountersUpdate;
    }

    /**
     * Gets the opcounters delete.
     *
     * @return the opcounters delete
     */
    public long getOpcountersDelete() {
        return opcountersDelete;
    }

    /**
     * Sets the opcounters delete.
     *
     * @param opcountersDelete
     *            the new opcounters delete
     */
    public void setOpcountersDelete(long opcountersDelete) {
        this.opcountersDelete = opcountersDelete;
    }

    /**
     * Gets the opcounters command.
     *
     * @return the opcounters command
     */
    public long getOpcountersCommand() {
        return opcountersCommand;
    }

    /**
     * Sets the opcounters command.
     *
     * @param opcountersCommand
     *            the new opcounters command
     */
    public void setOpcountersCommand(long opcountersCommand) {
        this.opcountersCommand = opcountersCommand;
    }

    /**
     * Gets the index counters accesses.
     *
     * @return the index counters accesses
     */
    public long getIndexCountersAccesses() {
        return indexCountersAccesses;
    }

    /**
     * Sets the index counters accesses.
     *
     * @param indexCountersAccesses
     *            the new index counters accesses
     */
    public void setIndexCountersAccesses(long indexCountersAccesses) {
        this.indexCountersAccesses = indexCountersAccesses;
    }

    /**
     * Gets the index counters hits.
     *
     * @return the index counters hits
     */
    public long getIndexCountersHits() {
        return indexCountersHits;
    }

    /**
     * Sets the index counters hits.
     *
     * @param indexCountersHits
     *            the new index counters hits
     */
    public void setIndexCountersHits(long indexCountersHits) {
        this.indexCountersHits = indexCountersHits;
    }

    /**
     * Gets the index counters misses.
     *
     * @return the index counters misses
     */
    public long getIndexCountersMisses() {
        return indexCountersMisses;
    }

    /**
     * Sets the index counters misses.
     *
     * @param indexCountersMisses
     *            the new index counters misses
     */
    public void setIndexCountersMisses(long indexCountersMisses) {
        this.indexCountersMisses = indexCountersMisses;
    }

    /**
     * Gets the index counters resets.
     *
     * @return the index counters resets
     */
    public long getIndexCountersResets() {
        return indexCountersResets;
    }

    /**
     * Sets the index counters resets.
     *
     * @param indexCountersResets
     *            the new index counters resets
     */
    public void setIndexCountersResets(long indexCountersResets) {
        this.indexCountersResets = indexCountersResets;
    }

    /**
     * Checks if is status.
     *
     * @return true, if is status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
