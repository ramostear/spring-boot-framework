package com.zeyu.framework.monitors.server.entity;

import com.zeyu.framework.core.persistence.entity.SimpleEntity;

/**
 * 每个磁盘的详细信息.
 *
 * @author zeyuphoenix
 */
public class DiskStatus extends SimpleEntity {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 磁盘名称
    /** The name. */
    private String name;

    // 总容量
    /** The size. */
    private String size;
    // 使用容量
    /** The used. */
    private String used;
    // 可用容量
    /** The avail. */
    private String avail;
    // 使用率
    /** The use rate. */
    private String useRate;
    // 挂载点
    /** The mounte. */
    private String mounte;
    // 磁盘类型
    // 文件系统类型，比如 FAT32、NTFS / 文件系统类型名，比如本地硬盘、光驱、网络文件系统等
    /** The type. */
    private String type;
    // 累计读总数
    private String diskReadBytes;
    // 累计写总数
    private String diskWriteBytes;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * Sets the size.
     *
     * @param size
     *            the new size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * Gets the used.
     *
     * @return the used
     */
    public String getUsed() {
        return used;
    }

    /**
     * Sets the used.
     *
     * @param used
     *            the new used
     */
    public void setUsed(String used) {
        this.used = used;
    }

    /**
     * Gets the avail.
     *
     * @return the avail
     */
    public String getAvail() {
        return avail;
    }

    /**
     * Sets the avail.
     *
     * @param avail
     *            the new avail
     */
    public void setAvail(String avail) {
        this.avail = avail;
    }

    /**
     * Gets the use rate.
     *
     * @return the use rate
     */
    public String getUseRate() {
        return useRate;
    }

    /**
     * Sets the use rate.
     *
     * @param useRate
     *            the new use rate
     */
    public void setUseRate(String useRate) {
        this.useRate = useRate;
    }

    /**
     * Gets the mounte.
     *
     * @return the mounte
     */
    public String getMounte() {
        return mounte;
    }

    /**
     * Sets the mounte.
     *
     * @param mounte
     *            the new mounte
     */
    public void setMounte(String mounte) {
        this.mounte = mounte;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getDiskReadBytes() {
        return diskReadBytes;
    }

    public void setDiskReadBytes(String diskReadBytes) {
        this.diskReadBytes = diskReadBytes;
    }

    public String getDiskWriteBytes() {
        return diskWriteBytes;
    }

    public void setDiskWriteBytes(String diskWriteBytes) {
        this.diskWriteBytes = diskWriteBytes;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
