package com.zeyu.framework.monitors.mysql.service;

import com.zeyu.framework.core.service.CrudService;
import com.zeyu.framework.monitors.mysql.dao.MysqlDao;
import com.zeyu.framework.monitors.mysql.entity.MysqlStatus;
import com.zeyu.framework.utils.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 处理mysql监控的service
 * Created by zeyuphoenix on 16/8/27.
 */
@Service
@Transactional(readOnly = true)
public class MysqlService extends CrudService<MysqlDao, MysqlStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Override
    public List<MysqlStatus> findList(MysqlStatus mysqlStatus) {

        // 设置默认时间范围，默认当前月
        if (mysqlStatus.getBeginDate() == null) {
            mysqlStatus.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
        }
        if (mysqlStatus.getEndDate() == null) {
            mysqlStatus.setEndDate(DateUtils.addMonths(mysqlStatus.getBeginDate(), 1));
        }

        return super.findList(mysqlStatus);

    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
