package com.zeyu.framework.monitors.server.entity;

import com.zeyu.framework.core.persistence.entity.SimpleEntity;

/**
 * 每个进程的详细信息.
 *
 * @author zeyuphoenix
 */
public class ProcStatus extends SimpleEntity {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 进程id
    /** The pid. */
    private String pid;
    // 进程用户
    /** The user. */
    private String user;
    // 开始时间
    /** The start time. */
    private String startTime;
    // 总虚拟内存大小
    /** The size. */
    private String size;
    // 实际使用内存大小
    /** The rss. */
    private String rss;
    // 共享内存大小
    /** The share. */
    private String share;
    // 读写状态
    /** The state. */
    private String state;
    // cpu运行时长
    /** The cpu time. */
    private String cpuTime;
    // 名称
    /** The name. */
    private String name;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the pid.
     *
     * @return the pid
     */
    public String getPid() {
        return pid;
    }

    /**
     * Sets the pid.
     *
     * @param pid
     *            the new pid
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user
     *            the new user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * Gets the start time.
     *
     * @return the start time
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * Sets the start time.
     *
     * @param startTime
     *            the new start time
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * Sets the size.
     *
     * @param size
     *            the new size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * Gets the rss.
     *
     * @return the rss
     */
    public String getRss() {
        return rss;
    }

    /**
     * Sets the rss.
     *
     * @param rss
     *            the new rss
     */
    public void setRss(String rss) {
        this.rss = rss;
    }

    /**
     * Gets the share.
     *
     * @return the share
     */
    public String getShare() {
        return share;
    }

    /**
     * Sets the share.
     *
     * @param share
     *            the new share
     */
    public void setShare(String share) {
        this.share = share;
    }

    /**
     * Gets the state.
     *
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state.
     *
     * @param state
     *            the new state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Gets the cpu time.
     *
     * @return the cpu time
     */
    public String getCpuTime() {
        return cpuTime;
    }

    /**
     * Sets the cpu time.
     *
     * @param cpuTime
     *            the new cpu time
     */
    public void setCpuTime(String cpuTime) {
        this.cpuTime = cpuTime;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
