package com.zeyu.framework.monitors.redis.service;

import com.zeyu.framework.core.service.CrudService;
import com.zeyu.framework.monitors.redis.dao.RedisDao;
import com.zeyu.framework.monitors.redis.entity.RedisStatus;
import com.zeyu.framework.utils.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 处理redis监控的service
 * Created by zeyuphoenix on 16/8/27.
 */
@Service
@Transactional(readOnly = true)
public class RedisService extends CrudService<RedisDao, RedisStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Override
    public List<RedisStatus> findList(RedisStatus redisStatus) {

        // 设置默认时间范围，默认当前月
        if (redisStatus.getBeginDate() == null) {
            redisStatus.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
        }
        if (redisStatus.getEndDate() == null) {
            redisStatus.setEndDate(DateUtils.addMonths(redisStatus.getBeginDate(), 1));
        }

        return super.findList(redisStatus);

    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
