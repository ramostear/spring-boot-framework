package com.zeyu.framework.monitors.redis.entity;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.core.persistence.entity.SimpleEntity;

import java.util.Date;

/**
 * Redis的整体状态监控.
 *
 * @author zeyuphoenix
 */
public class RedisStatus extends SimpleEntity<RedisStatus> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 收集时间
    private Date time;
    // Redis状态
    private boolean status;

    // 这些项目不会根据时间进行变化，也没有按时间统计的意义，每次取得，不保存数据库，节省数据库空间
    // Redis 服务器版本
    private String version;
    // redis地址
    private String host;
    // Redis 服务器的宿主操作系统
    private String os;
    // 架构（32 或 64 位）
    private String arch;
    // Redis 所使用的事件处理机制
    private String multiplexingApi;
    // 服务器进程的 PID
    private String process;
    // TCP/IP 监听端口
    private int port;
    // 自 Redis 服务器启动以来，经过的秒数
    private long uptime;
    //配置文件的保存路径
    private String configFile;

    // 一个标志值，记录集群功能是否已经开启
    private boolean clusterEnabled = false;
    // 在编译时指定的， Redis 所使用的内存分配器。可以是 libc 、 jemalloc 或者 tcmalloc
    private String memAllocator;

    // 这些信息属于持续增长项目,不可作图,每次获取后展示即可,也暂不保存数据库
    // 服务器已接受的连接请求数量
    private long totalConnectionsReceived;
    // 服务器已执行的命令数量
    private long totalCommandsProcessed;
    // 因为过期而被自动删除的数据库键数量
    private long expiredKeys;
    // 因为最大客户端数量限制而被拒绝的连接请求数量
    private long rejectedConnections;
    // 查找数据库键成功的次数。
    private long keyspaceHits;
    // 因为最大内存容量限制而被驱逐（evict）的键数量
    private long evictedkeys;
    // 查找数据库键失败的次数
    private long keyspacemisses;
    // 总输出字节数
    private long totalNetOutputBytes;
    // 总输入字节数
    private long totalNetInputBytes;

    // 已连接客户端的数量（不包括通过从属服务器连接的客户端）
    private int connectedClients;

    // Redis服务器耗费的系统 CPU
    private float usedCpuSys;
    // Redis服务器耗费的用户 CPU
    private float usedCpuUser;

    //由 Redis 分配器分配的内存总量，以字节（byte）为单位
    private float usedMemory;
    // 从操作系统的角度，返回 Redis 已分配的内存总量（俗称常驻集大小）。这个值和 top 、 ps等命令的输出一致。
    private float usedMemoryRss;
    // Redis 的内存消耗峰值（以字节为单位）
    private float usedMemorypeak;
    //used_memory_rss 和 used_memory 之间的比率
    private float memFragmentationRatio;

    // 服务器每秒钟执行的命令数量。
    private int instantaneousOpsPerSec;

    // 查询参数
    // 开始日期
    private Date beginDate;
    // 结束日期
    private Date endDate;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getArch() {
        return arch;
    }

    public void setArch(String arch) {
        this.arch = arch;
    }

    public String getMultiplexingApi() {
        return multiplexingApi;
    }

    public void setMultiplexingApi(String multiplexingApi) {
        this.multiplexingApi = multiplexingApi;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public long getUptime() {
        return uptime;
    }

    public void setUptime(long uptime) {
        this.uptime = uptime;
    }

    public String getConfigFile() {
        return configFile;
    }

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public boolean isClusterEnabled() {
        return clusterEnabled;
    }

    public int getConnectedClients() {
        return connectedClients;
    }

    public void setConnectedClients(int connectedClients) {
        this.connectedClients = connectedClients;
    }

    public void setClusterEnabled(boolean clusterEnabled) {
        this.clusterEnabled = clusterEnabled;
    }

    public String getMemAllocator() {
        return memAllocator;
    }

    public void setMemAllocator(String memAllocator) {
        this.memAllocator = memAllocator;
    }

    public long getTotalConnectionsReceived() {
        return totalConnectionsReceived;
    }

    public void setTotalConnectionsReceived(long totalConnectionsReceived) {
        this.totalConnectionsReceived = totalConnectionsReceived;
    }

    public long getTotalCommandsProcessed() {
        return totalCommandsProcessed;
    }

    public void setTotalCommandsProcessed(long totalCommandsProcessed) {
        this.totalCommandsProcessed = totalCommandsProcessed;
    }

    public long getExpiredKeys() {
        return expiredKeys;
    }

    public void setExpiredKeys(long expiredKeys) {
        this.expiredKeys = expiredKeys;
    }

    public long getRejectedConnections() {
        return rejectedConnections;
    }

    public void setRejectedConnections(long rejectedConnections) {
        this.rejectedConnections = rejectedConnections;
    }

    public long getKeyspaceHits() {
        return keyspaceHits;
    }

    public void setKeyspaceHits(long keyspaceHits) {
        this.keyspaceHits = keyspaceHits;
    }

    public long getEvictedkeys() {
        return evictedkeys;
    }

    public void setEvictedkeys(long evictedkeys) {
        this.evictedkeys = evictedkeys;
    }

    public long getKeyspacemisses() {
        return keyspacemisses;
    }

    public void setKeyspacemisses(long keyspacemisses) {
        this.keyspacemisses = keyspacemisses;
    }

    public long getTotalNetOutputBytes() {
        return totalNetOutputBytes;
    }

    public void setTotalNetOutputBytes(long totalNetOutputBytes) {
        this.totalNetOutputBytes = totalNetOutputBytes;
    }

    public long getTotalNetInputBytes() {
        return totalNetInputBytes;
    }

    public void setTotalNetInputBytes(long totalNetInputBytes) {
        this.totalNetInputBytes = totalNetInputBytes;
    }

    public float getUsedCpuSys() {
        return usedCpuSys;
    }

    public void setUsedCpuSys(float usedCpuSys) {
        this.usedCpuSys = usedCpuSys;
    }

    public float getUsedCpuUser() {
        return usedCpuUser;
    }

    public void setUsedCpuUser(float usedCpuUser) {
        this.usedCpuUser = usedCpuUser;
    }

    public float getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(float usedMemory) {
        this.usedMemory = usedMemory;
    }

    public float getUsedMemoryRss() {
        return usedMemoryRss;
    }

    public void setUsedMemoryRss(float usedMemoryRss) {
        this.usedMemoryRss = usedMemoryRss;
    }

    public float getUsedMemorypeak() {
        return usedMemorypeak;
    }

    public void setUsedMemorypeak(float usedMemorypeak) {
        this.usedMemorypeak = usedMemorypeak;
    }

    public float getMemFragmentationRatio() {
        return memFragmentationRatio;
    }

    public void setMemFragmentationRatio(float memFragmentationRatio) {
        this.memFragmentationRatio = memFragmentationRatio;
    }

    public int getInstantaneousOpsPerSec() {
        return instantaneousOpsPerSec;
    }

    public void setInstantaneousOpsPerSec(int instantaneousOpsPerSec) {
        this.instantaneousOpsPerSec = instantaneousOpsPerSec;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
