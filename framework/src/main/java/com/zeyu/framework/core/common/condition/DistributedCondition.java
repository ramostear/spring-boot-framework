package com.zeyu.framework.core.common.condition;

import com.zeyu.framework.core.common.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 根据配置是否为分布式决定加载的bean
 *
 * Created by zeyuphoenix on 16/7/19.
 */
public class DistributedCondition implements Condition, Constant {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(DistributedCondition.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        Environment environment = conditionContext.getEnvironment();
        boolean distributed = false;
        if (environment != null) {
            distributed = Boolean.valueOf(environment.getProperty(CONFIGURATION_DEFINED_PREFIX + "distributed", "false"));
        }
        if (logger.isDebugEnabled()) {
            logger.debug("系统初始化检测中,系统{}分布式", distributed ? "属于" : "不属于");
        }

        return distributed;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
