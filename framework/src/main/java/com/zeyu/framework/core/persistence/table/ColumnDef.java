package com.zeyu.framework.core.persistence.table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * A column definition, containing the different information used when
 * server-side processing is enabled.
 */
public class ColumnDef implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The name. */
    private String name;

    /** The sortable. */
    private boolean sortable;

    /** The sorted. */
    private boolean sorted;

    /** The filterable. */
    private boolean filterable;

    /** The filtered. */
    private boolean filtered;

    /** The regex. */
    private String regex;

    /** The search. */
    private String search;

    /** The search from. */
    private String searchFrom;

    /** The search to. */
    private String searchTo;

    /** The sort direction. */
    private SortDirection sortDirection;

    /**
     * The Enum SortDirection.
     */
    public enum SortDirection {

        /** The asc. */
        ASC,
        /** The desc. */
        DESC
    }

    /**
     * Gets the sort direction.
     *
     * @return the sort direction
     */
    public SortDirection getSortDirection() {
        return sortDirection;
    }

    /**
     * Sets the sort direction.
     *
     * @param sortDirection
     *            the new sort direction
     */
    public void setSortDirection(SortDirection sortDirection) {
        this.sortDirection = sortDirection;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Checks if is sortable.
     *
     * @return true, if is sortable
     */
    public boolean isSortable() {
        return sortable;
    }

    /**
     * Sets the sortable.
     *
     * @param sortable
     *            the new sortable
     */
    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    /**
     * Checks if is filterable.
     *
     * @return true, if is filterable
     */
    public boolean isFilterable() {
        return filterable;
    }

    /**
     * Sets the filterable.
     *
     * @param filterable
     *            the new filterable
     */
    public void setFilterable(boolean filterable) {
        this.filterable = filterable;
    }

    /**
     * Gets the regex.
     *
     * @return the regex
     */
    public String getRegex() {
        return regex;
    }

    /**
     * Sets the regex.
     *
     * @param regex
     *            the new regex
     */
    public void setRegex(String regex) {
        this.regex = regex;
    }

    /**
     * Gets the search.
     *
     * @return the search
     */
    public String getSearch() {
        return search;
    }

    /**
     * Sets the search.
     *
     * @param search
     *            the new search
     */
    public void setSearch(String search) {
        this.search = search;
    }

    /**
     * Gets the search from.
     *
     * @return the search from
     */
    public String getSearchFrom() {
        return searchFrom;
    }

    /**
     * Sets the search from.
     *
     * @param searchFrom
     *            the new search from
     */
    public void setSearchFrom(String searchFrom) {
        this.searchFrom = searchFrom;
    }

    /**
     * Gets the search to.
     *
     * @return the search to
     */
    public String getSearchTo() {
        return searchTo;
    }

    /**
     * Sets the search to.
     *
     * @param searchTo
     *            the new search to
     */
    public void setSearchTo(String searchTo) {
        this.searchTo = searchTo;
    }

    /**
     * Checks if is sorted.
     *
     * @return true, if is sorted
     */
    public boolean isSorted() {
        return sorted;
    }

    /**
     * Sets the sorted.
     *
     * @param sorted
     *            the new sorted
     */
    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    /**
     * Checks if is filtered.
     *
     * @return true, if is filtered
     */
    public boolean isFiltered() {
        return filtered;
    }

    /**
     * Sets the filtered.
     *
     * @param filtered
     *            the new filtered
     */
    public void setFiltered(boolean filtered) {
        this.filtered = filtered;
    }

    /*
     * (non-Javadoc)
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}