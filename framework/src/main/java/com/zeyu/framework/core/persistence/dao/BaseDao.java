package com.zeyu.framework.core.persistence.dao;

/**
 * DAO支持类实现
 */
public interface BaseDao {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================
}
