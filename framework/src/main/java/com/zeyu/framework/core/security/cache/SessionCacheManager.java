package com.zeyu.framework.core.security.cache;

import com.google.common.collect.Sets;

import com.zeyu.framework.core.web.servlet.Servlets;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.InvalidSessionException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

/**
 * 自定义授权缓存管理类
 * Created by zeyuphoenix on 16/6/27.
 */
public class SessionCacheManager implements CacheManager {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        return new SessionCache<>(name);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    /**
     * SESSION缓存管理类
     */
    public class SessionCache<K, V> implements Cache<K, V> {

        // ================================================================
        // Constants
        // ================================================================

        /**
         * Logger
         */
        private final Logger logger = LoggerFactory.getLogger(getClass());

        // ================================================================
        // Fields
        // ================================================================

        // cache 名称
        private String cacheKeyName = null;

        // ================================================================
        // Constructors
        // ================================================================

        /**
         */
        public SessionCache(String cacheKeyName) {
            this.cacheKeyName = cacheKeyName;
        }

        // ================================================================
        // Methods from/for super Interfaces or SuperClass
        // ================================================================

        @SuppressWarnings("unchecked")
        @Override
        public V get(K key) throws CacheException {
            if (key == null) {
                return null;
            }

            V v;
            HttpServletRequest request = Servlets.getRequest();
            if (request != null) {
                v = (V) request.getAttribute(cacheKeyName);
                if (v != null) {
                    return v;
                }
            }

            V value;
            value = (V) getSession().getAttribute(cacheKeyName);
            logger.debug("get {} {} {}", cacheKeyName, key, request != null ? request.getRequestURI() : "");

            if (request != null && value != null) {
                request.setAttribute(cacheKeyName, value);
            }
            return value;
        }

        @Override
        public V put(K key, V value) throws CacheException {
            if (key == null) {
                return null;
            }

            getSession().setAttribute(cacheKeyName, value);

            if (logger.isDebugEnabled()) {
                HttpServletRequest request = Servlets.getRequest();
                logger.debug("put {} {} {}", cacheKeyName, key, request != null ? request.getRequestURI() : "");
            }

            return value;
        }

        @SuppressWarnings("unchecked")
        @Override
        public V remove(K key) throws CacheException {

            V value;
            value = (V) getSession().removeAttribute(cacheKeyName);
            logger.debug("remove {} {}", cacheKeyName, key);

            return value;
        }

        @Override
        public void clear() throws CacheException {
            getSession().removeAttribute(cacheKeyName);
            logger.debug("clear {}", cacheKeyName);
        }

        @Override
        public int size() {
            logger.debug("invoke session size abstract size method not supported.");
            return 0;
        }

        @Override
        public Set<K> keys() {
            logger.debug("invoke session keys abstract size method not supported.");
            return Sets.newHashSet();
        }

        @Override
        public Collection<V> values() {
            logger.debug("invoke session values abstract size method not supported.");
            return Collections.emptyList();
        }

        // ================================================================
        // Private Methods
        // ================================================================

        /**
         * 获取当前用户session
         */
        private Session getSession() {
            Session session = null;
            try {
                Subject subject = SecurityUtils.getSubject();
                session = subject.getSession(false);
                if (session == null) {
                    session = subject.getSession();
                }
            } catch (InvalidSessionException e) {
                logger.error("Invalid session error", e);
            } catch (UnavailableSecurityManagerException ex) {
                logger.error("Unavailable SecurityManager error", ex);
            }
            return session;
        }
    }

    // ================================================================
    // Test Methods
    // ================================================================

}
