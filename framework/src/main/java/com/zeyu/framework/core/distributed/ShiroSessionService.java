package com.zeyu.framework.core.distributed;

import com.google.common.collect.Sets;

import com.zeyu.framework.core.distributed.notice.ShiroSessionMessage;
import com.zeyu.framework.core.distributed.notice.ShiroSessionMessageListener;

import com.zeyu.framework.utils.SerializeUtils;
import com.zeyu.framework.utils.SpringContextHolder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * 直接操作Session属性，不会被保存
 * 封装Session属性相关操作 Session属性发生改变时保存到Redis中并通知其它节点清空本地EhCache缓存
 * Created by zeyuphoenix on 16/7/6.
 */
public class ShiroSessionService extends ShiroSessionMessageListener {

    // ================================================================
    // Constants
    // ================================================================

    private static final Logger logger = LoggerFactory.getLogger(ShiroSessionService.class);

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisSessionDAO redisSessionDAO;

    private String uncacheChannel = "shiro.session.uncache";

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public void onMessage(ShiroSessionMessage message) {

        logger.debug("on message: channel {} , message {} ", message.getChannel(), message.msgBody);
        // 本机cache清除
        redisSessionDAO.uncache(message.msgBody.sessionId);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 发送session失效信息,通知各个客户端处理
     */
    public void sendUncacheSessionMessage(Serializable sessionId) {
        String nodeId = ManagementFactory.getRuntimeMXBean().getName();
        ShiroSessionMessage.MessageBody messageBody = new ShiroSessionMessage.MessageBody(sessionId, nodeId);
        // 发布消息到各个redis的listener

        redisTemplate.convertAndSend(uncacheChannel, SerializeUtils.serializeToString(messageBody));
    }

    public ShiroSession getSession() {
        return (ShiroSession) this.redisSessionDAO.doReadSessionWithoutExpire(SecurityUtils.getSubject()
                .getSession().getId());
    }

    public Collection<Session> getActiveSessions() {
        return redisSessionDAO.getActiveSessions();
    }

    public void setId(Serializable id) {
        ShiroSession session = this.getSession();
        session.setId(id);
        this.redisSessionDAO.update(session);
        // 通过发布消息通知其他节点取消本地对session的缓存
        sendUncacheSessionMessage(session.getId());
    }

    public void setStopTimestamp(Date stopTimestamp) {
        ShiroSession session = this.getSession();
        session.setStopTimestamp(stopTimestamp);
        this.redisSessionDAO.update(session);
        // 通过发布消息通知其他节点取消本地对session的缓存
        sendUncacheSessionMessage(session.getId());
    }

    public void setExpired(boolean expired) {
        ShiroSession session = this.getSession();
        session.setExpired(expired);
        this.redisSessionDAO.update(session);
        // 通过发布消息通知其他节点取消本地对session的缓存
        sendUncacheSessionMessage(session.getId());
    }

    public void setTimeout(long timeout) {
        ShiroSession session = this.getSession();
        session.setTimeout(timeout);
        this.redisSessionDAO.update(session);
        // 通过发布消息通知其他节点取消本地对session的缓存
        sendUncacheSessionMessage(session.getId());
    }

    public void setHost(String host) {
        ShiroSession session = this.getSession();
        session.setHost(host);
        this.redisSessionDAO.update(session);
        // 通过发布消息通知其他节点取消本地对session的缓存

        sendUncacheSessionMessage(session.getId());
    }

    public void setAttributes(Map<Object, Object> attributes) {
        ShiroSession session = this.getSession();
        session.setAttributes(attributes);
        this.redisSessionDAO.update(session);
        // 通过发布消息通知其他节点取消本地对session的缓存
        sendUncacheSessionMessage(session.getId());
    }

    public Map<Object, Object> getAttributes() {
        return this.getSession().getAttributes();
    }

    public void setAttribute(Object key, Object value) {
        ShiroSession session = this.getSession();
        session.setAttribute(key, value);
        this.redisSessionDAO.update(session);
        // 通过发布消息通知其他节点取消本地对session的缓存
        sendUncacheSessionMessage(session.getId());
    }

    public Object getAttribute(Object key) {
        return this.getSession().getAttribute(key);
    }

    public Collection<Object> getAttributeKeys() {
        return this.getSession().getAttributeKeys();
    }

    public Object removeAttribute(Object key) {
        ShiroSession session = this.getSession();
        Object res = session.removeAttribute(key);
        this.redisSessionDAO.update(session);
        // 通过发布消息通知其他节点取消本地对session的缓存
        sendUncacheSessionMessage(session.getId());
        return res;
    }


    /**
     * 清除所有redis缓存
     */
    public void flushRedis() {
        Collection<Session> activeSession = redisSessionDAO.getActiveSessions();
        if (activeSession != null) {
            for (Session session : activeSession) {
                try {
                    redisSessionDAO.doDelete(session);
                } catch (Exception ignored) {

                }
            }
        }
    }

    /**
     * 清除ehcache缓存
     */
    public void flushEhCache() {
        Set<Session> sessions = Sets.newHashSet();
        Collection<Session> ehCacheActiveSession = redisSessionDAO.getEhCacheActiveSessions();
        Collection<Session> activeSession = redisSessionDAO.getActiveSessions();
        if (CollectionUtils.isNotEmpty(ehCacheActiveSession)) {
            sessions.addAll(ehCacheActiveSession);
        }
        if (CollectionUtils.isNotEmpty(activeSession)) {
            sessions.addAll(activeSession);
        }
        for (Session session : sessions) {
            try {
                redisSessionDAO.uncache(session.getId());
            } catch (Exception ignored) {
            }
        }
        logger.info("flushEhCache Project EhCacheActiveSessions {} ", redisSessionDAO.getEhCacheActiveSessions().size());
    }

    /**
     * 清除所有缓存(redis和ehcache)
     */
    public void flushAll() {
        Collection<Session> activeSession = redisSessionDAO.getActiveSessions();
        if (activeSession != null) {
            for (Session session : activeSession) {
                try {
                    redisSessionDAO.delete(session);
                } catch (Exception ignored) {
                }
            }
        }
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    public void setUncacheChannel(String uncacheChannel) {
        this.uncacheChannel = uncacheChannel;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
