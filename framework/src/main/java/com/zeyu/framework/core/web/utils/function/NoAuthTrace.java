package com.zeyu.framework.core.web.utils.function;

import com.zeyu.framework.utils.Exceptions;
import com.zeyu.framework.utils.StringUtils;

import org.beetl.core.Context;
import org.beetl.core.Function;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 403异常处理
 * Created by zeyuphoenix on 2016/3/30.
 */
public class NoAuthTrace implements Function {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public Object call(Object[] paras, Context ctx) {
        // 参数为空
        if (paras == null || paras.length == 0) {
            return "";
        }
        HttpServletRequest request = (HttpServletRequest) ctx.getGlobal("request");
        HttpServletResponse response = (HttpServletResponse) ctx.getGlobal("response");

        response.setStatus(403);

        //获取异常类
        Throwable ex = Exceptions.getThrowable(request);

        StringBuilder sb = new StringBuilder("");
        if (ex != null && StringUtils.startsWith(ex.getMessage(), "msg:")) {
            sb.append("<div>").append(StringUtils.replace(ex.getMessage(), "msg:", "")).append(" <br/> <br/></div>");
        }

        // 如果是异步请求或是手机端，则直接返回信息
        if (paras[0] != null && Boolean.valueOf(paras[0].toString())) {
            try {
                if (ex != null && StringUtils.startsWith(ex.getMessage(), "msg:")) {
                    response.getOutputStream().print(StringUtils.replace(ex.getMessage(), "msg:", ""));
                } else {
                    response.getOutputStream().print("操作权限不足.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
