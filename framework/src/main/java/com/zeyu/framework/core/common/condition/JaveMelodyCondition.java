package com.zeyu.framework.core.common.condition;

import com.zeyu.framework.core.common.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 根据配置是否启动java melody监控
 * 目前程序使用shiro控制session和cache,添加melody导致冲突
 *
 * Created by zeyuphoenix on 16/7/19.
 */
public class JaveMelodyCondition implements Condition, Constant {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(JaveMelodyCondition.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        Environment environment = conditionContext.getEnvironment();
        boolean javamelody = false;
        if (environment != null) {
            javamelody = Boolean.valueOf(environment.getProperty(CONFIGURATION_DEFINED_PREFIX + "javamelody", "false"));
        }
        if (logger.isDebugEnabled()) {
            logger.debug("系统初始化检测中,系统{} java melody 监控", javamelody ? "开启" : "不开启");
        }

        return javamelody;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
