package com.zeyu.framework.core.web.utils.tag;


import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.utils.DateUtils;
import com.zeyu.framework.utils.StringUtils;

import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * beetl 使用自定义函数写了common tag功能，你可以像使用jsp的标签那样使用
 * <!--:
 * if(common.replaceHtml("sex")) {}
 * -->
 * Created by zeyuphoenix on 2016/3/23.
 */
@Service
public class CommonExt {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 标签使用时经常转换boolean错误
     */
    public static Boolean parseBoolean(Object obj) {
        if (obj == null | StringUtils.isBlank(obj.toString())) {
            return false;
        }
        if (obj instanceof Boolean) {
            return Boolean.valueOf(obj.toString());
        }
        return Boolean.valueOf(obj.toString());
    }

    /**
     * 从后边开始截取字符串
     *
     * @return 字符串
     */
    public static String getDate(String str, String separator) {
        return StringUtils.substringAfterLast(str, separator);
    }

    /**
     * 判断字符串是否以某某开头
     *
     * @return 是否
     */
    public static boolean startsWith(String str, String prefix) {
        return StringUtils.startsWith(str, prefix);
    }

    /**
     * 判断字符串是否以某某结尾
     *
     * @return 是否
     */
    public static boolean endsWith(String str, String prefix) {
        return StringUtils.endsWith(str, prefix);
    }

    /**
     * 缩写字符串，超过最大宽度用 "..." 表示
     *
     * @return 字符串
     */
    public static String abbr(String str, int length) {
        return StringUtils.abbr(str, length);
    }

    /**
     * 替换掉HTML标签
     *
     * @return 字符串
     */
    public static String replaceHtml(String html) {
        return StringUtils.replaceHtml(html);
    }

    /**
     * 转换为JS获取对象值，生成三目运算返回结果
     *
     * @return 对象值
     */
    public static String jsGetVal(String val) {
        return StringUtils.jsGetVal(val);
    }

    /**
     * 获取当前日期
     *
     * @return 日期
     */
    public static String getDate(String pattern) {
        return DateUtils.getDate(pattern);
    }

    /**
     * 获取过去的天数
     *
     * @return 过去的天数
     */
    public static long pastDays(Date date) {
        return DateUtils.pastDays(date);
    }

    /**
     * 对象转换JSON字符串
     *
     * @return JSON字符串
     */
    public static String toJson(Object object) {
        return JsonMapper.toJsonString(object);
    }

    /**
     * 对象转换Html字符串
     *
     * @return Html字符串
     */
    public static String toHtml(String object) {
        return StringUtils.toHtml(object);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
