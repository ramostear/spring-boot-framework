package com.zeyu.framework.core.security;

import com.zeyu.framework.core.common.Constant;
import com.zeyu.framework.utils.StringUtils;
import org.apache.http.entity.ContentType;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 页面加载使用的是ajax加载div方式,session过期跳转login有问题
 * Created by zeyuphoenix on 16/9/21.
 */
public class UserFilter extends org.apache.shiro.web.filter.authc.UserFilter {

    // ================================================================
    // Constants
    // ================================================================

    // 默认rest前缀
    private static final String REST_URL_PREFIX = "/rest";
    // rest请求认证失败返回值
    private static final String REST_TOKEN_ERROR = "{\"status\": \"error\",\"message\": \"user token error!\"}";

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return super.isAccessAllowed(request, response, mappedValue);
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {

        // 因为是ajax加载,暂时返回主页
        // saveRequest(request);

        // 对于rest请求,token过期和logout后我们都返回失败json,不跳转页面
        String path = WebUtils.getPathWithinApplication(WebUtils.toHttp(request));
        if (StringUtils.isNoneBlank(path) && StringUtils.startsWith(path, REST_URL_PREFIX)) {

            response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
            response.getOutputStream().write(REST_TOKEN_ERROR.getBytes(Constant.CHARSET_UTF_8));
            response.getOutputStream().flush();

            return false;
        }

        // 跳转到自定义请求
        WebUtils.issueRedirect(request, response, "/common/sessionTimeout");

        return false;
    }


    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
