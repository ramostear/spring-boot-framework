package com.zeyu.framework.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.core.persistence.dao.AutoDao;
import com.zeyu.framework.core.persistence.entity.AutoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 自动Service基类
 */
@Transactional(readOnly = true)
public abstract class AutoService<D extends AutoDao<T>, T extends AutoEntity<T>> extends BaseService {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 持久层对象
     */
    @Autowired
    protected D dao;

    /**
     * 获取单条数据
     *
     * @param id 对象Id
     * @return 数据
     */
    public T get(String id) {
        return dao.selectByPrimaryKey(id);
    }

    /**
     * 获取单条数据
     *
     * @param entity 实体对象
     * @return 数据
     */
    public T get(T entity) {
        return dao.selectOne(entity);
    }

    /**
     * 查询列表数据
     *
     * @param entity 实体对象
     * @return 数据列表
     */
    public List<T> findList(T entity) {
        return dao.select(entity);
    }

    /**
     * 查询分页数据
     *
     * @param page   分页对象
     * @param entity 实体
     * @return 查询结果
     */
    public Page<T> findPage(Page<T> page, T entity) {

        //获取第n页，m条内容，默认查询总数count
        PageHelper.startPage(page.getPageNum(), page.getMaxResults());

        // 显示为List,实际是Page对象
        entity.setPage(page);
        List<T> list = dao.select(entity);

        //用PageInfo对结果进行包装
        PageInfo<T> pageInfo = new PageInfo<>(list);
        page.setList(pageInfo.getList());
        page.setCount(pageInfo.getTotal());

        return page;
    }

    /**
     * 保存数据（插入或更新）
     *
     * @param entity 实体
     */
    @Transactional
    public void save(T entity) {
        if (entity.getIsNewRecord()) {
            entity.preInsert();
            dao.insertSelective(entity);
        } else {
            entity.preUpdate();
            dao.updateByPrimaryKeySelective(entity);
        }
    }

    /**
     * 删除数据
     *
     * @param entity 实体对象
     */
    @Transactional
    public void delete(T entity) {
        dao.delete(entity);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
