package com.zeyu.framework.core.persistence.entity;

import com.zeyu.framework.utils.IdGen;

import javax.persistence.Id;

/**
 * 简单Entity类,除了id没有任何附加字段,为了使用自动Mapper,动态生成增删改查,这里覆盖BaseEntity的属性,添加@Transient注解
 *
 * 对于自动Entity,统一使用JPA注解
 *
 * ①. 表名默认使用类名,驼峰转下划线(只对大写字母进行处理),如UserInfo默认对应的表名为user_info.
 * ②. 表名可以使用@Table(name = "tableName")进行指定,对不符合第一条默认规则的可以通过这种方式指定表名.
 * ③. 字段默认和@Column一样,都会作为表字段,表字段默认为Java对象的Field名字驼峰转下划线形式.
 * ④. 可以使用@Column(name = "fieldName")指定不符合第3条规则的字段名
 * ⑤. 使用@Transient注解可以忽略字段,添加该注解的字段不会作为表字段使用.
 * ⑥. 建议一定是有一个@Id注解作为主键的字段,可以有多个@Id注解的字段作为联合主键.
 * ⑦. 默认情况下,实体类中如果不存在包含@Id注解的字段,所有的字段都会作为主键字段进行使用(这种效率极低).
 * ⑧. 实体类可以继承使用,可以参考测试代码中的tk.mybatis.mapper.model.UserLogin2类.
 * ⑨. 由于基本类型,如int作为实体类字段时会有默认值0,而且无法消除,所以实体类中建议不要使用基本类型.
 * ⑩. @NameStyle注解，用来配置对象名/字段和表名/字段之间的转换方式，该注解优先于全局配置style，可选值：
 *      normal:    使用实体类名/属性名作为表名/字段名
 *      camelhump: 这是默认值，驼峰转换为下划线形式
 *      uppercase: 转换为大写
 *      lowercase: 转换为小写
 *
 * 注意: 实体类中包含了不是数据库表中的字段，你需要给这个字段加上@Transient注解
 */
public abstract class AutoEntity<T> extends BaseEntity<T> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    /**
     * 实体编号（唯一标识）
     */
    @Id
    protected String id;

    // ================================================================
    // Constructors
    // ================================================================

    /**
     */
    public AutoEntity() {
        super();
    }

    /**
     */
    public AutoEntity(String id) {
        super(id);
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /**
     * 插入之前执行方法，需要手动调用
     */
    @Override
    public void preInsert() {
        // 不限制ID为UUID，调用setIsNewRecord()使用自定义ID
        if (!this.isNewRecord) {
            setId(IdGen.uuid());
        }
    }

    /**
     * 更新之前执行方法，需要手动调用
     */
    @Override
    public void preUpdate() {
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
