package com.zeyu.framework.core.configuration;

import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * 异常统一处理配置
 * Created by zeyuphoenix on 16/6/28.
 */
@Configuration
public class ExceptionConfiguration {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 重新定义默认异常处理的视图,可以扩展
     */
    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return container -> {
            container.addErrorPages(new ErrorPage(HttpStatus.UNAUTHORIZED, "/error/401"));
            container.addErrorPages(new ErrorPage(HttpStatus.FORBIDDEN, "/error/403"));
            container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/error/404"));
            container.addErrorPages(new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/500"));
            // 这个会导致所有异常被捕捉,不走具体的ControllerAdvice
            container.addErrorPages(new ErrorPage(Throwable.class, "/error/500"));
        };
    }

    /**
     * 异常拦截,统一处理,这是springmvc的方式
     */
   /* @Bean
    public SimpleMappingExceptionResolver simpleMappingExceptionResolver() {
        SimpleMappingExceptionResolver exceptionResolver = new SimpleMappingExceptionResolver();

        exceptionResolver.setDefaultErrorView("error/500");

        Properties mappings = new Properties();
        // 未授权处理页
        mappings.setProperty("org.apache.shiro.authz.UnauthorizedException", "error/403");
        // 身份没有验证
        mappings.setProperty("org.apache.shiro.authz.UnauthenticatedException", "error/403");
        // 异常
        mappings.setProperty("java.lang.Throwable", "error/500");
        exceptionResolver.setExceptionMappings(mappings);

        return exceptionResolver;
    }*/

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
