package com.zeyu.framework.core.distributed.notice;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

/**
 * shiro message的监听器
 * Created by zeyuphoenix on 16/7/6.
 */
public abstract class ShiroSessionMessageListener implements MessageListener {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public final void onMessage(Message message, byte[] bytes) {

        onMessage(new ShiroSessionMessage(message.getChannel(), message.getBody()));

    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    public abstract void onMessage(ShiroSessionMessage message);

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
