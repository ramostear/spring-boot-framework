package com.zeyu.framework.core.security;

/**
 * 用户和密码（包含验证码）令牌类
 * Created by zeyuphoenix on 16/6/25.
 */
public class UsernamePasswordToken extends org.apache.shiro.authc.UsernamePasswordToken {

    // ================================================================
    // Constants
    // ================================================================

    /** 前台普通密码登录 */
    public static final int LOGIN_TYPE_SIMPLE = 0;
    /** 加密密码登录 */
    public static final int LOGIN_TYPE_ENCRYPTION = 1;

    // ================================================================
    // Fields
    // ================================================================

    private String captcha;
    private boolean mobileLogin;

    private int type = LOGIN_TYPE_SIMPLE;

    // ================================================================
    // Constructors
    // ================================================================

    public UsernamePasswordToken() {
        super();
    }

    public UsernamePasswordToken(String username, char[] password, boolean rememberMe, String host, String captcha, boolean mobileLogin) {
        super(username, password, rememberMe, host);
        this.captcha = captcha;
        this.mobileLogin = mobileLogin;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public String toString() {
        return "UsernamePasswordToken{" +
                "captcha='" + captcha + '\'' +
                ", mobileLogin=" + mobileLogin +
                ", type=" + type +
                '}';
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public boolean isMobileLogin() {
        return mobileLogin;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
