package com.zeyu.framework.core.security;

import com.zeyu.framework.core.common.Constant;
import com.zeyu.framework.core.security.interfaces.SecurityService;
import com.zeyu.framework.core.security.interfaces.SecurityUser;
import com.zeyu.framework.core.security.utils.SecurityUtils;
import com.zeyu.framework.utils.Encodes;
import com.zeyu.framework.utils.SpringContextHolder;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 系统安全认证实现类
 */
public class SystemAuthorizingRealm extends AuthorizingRealm implements Constant {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * Logger
     */
    private Logger logger = LoggerFactory.getLogger(getClass());

    // ================================================================
    // Fields
    // ================================================================

    private SecurityService securityService;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /**
     * 认证回调函数, 登录时调用
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) {
        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;

        int activeSessionSize = getSystemService().getSessionDao().getActiveSessions(false).size();
        if (logger.isDebugEnabled()) {
            logger.debug("login submit, active session size: {}, username: {}", activeSessionSize, token.getUsername());
        }

        // 校验登录验证码

        // 校验用户名密码
        SecurityUser user = getSystemService().getUserByLoginName(token.getUsername());
        if (user != null) {
            if (NO.equals(user.getLoginFlag())) {
                throw new AuthenticationException("msg:该已帐号禁止登录.");
            }
            byte[] salt = Encodes.decodeHex(user.getPassword().substring(0, 16));

            // 根据用户登录类型选择密码加密方式
            if (token.getType() == UsernamePasswordToken.LOGIN_TYPE_ENCRYPTION) {
                // 这种情况很少,每次直接放新的
                initSimpleCredentialsMatcher();
                return new SimpleAuthenticationInfo(new Principal(user, token.isMobileLogin()), user.getPassword(), getName());
            } else {
                // 这种情况很多,避免每次都new,比较一下,不一致再new
                if (!(getCredentialsMatcher() instanceof HashedCredentialsMatcher)) {
                    initCredentialsMatcher();
                }
                return new SimpleAuthenticationInfo(new Principal(user, token.isMobileLogin()), user.getPassword().substring(16), ByteSource.Util.bytes(salt), getName());
            }

        } else {
            return null;
        }
    }

    /**
     * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        Principal principal = (Principal) getAvailablePrincipal(principals);
        // 获取当前已登录的用户  user.multiAccountLogin
        if (!TRUE.equals("true")) {
            Collection<Session> sessions = getSystemService().getSessionDao().getActiveSessions(true, principal, SecurityUtils.getSession());
            if (sessions.size() > 0) {
                // 如果是登录进来的，则踢出已在线用户
                if (SecurityUtils.getSubject().isAuthenticated()) {
                    for (Session session : sessions) {
                        getSystemService().getSessionDao().delete(session);
                    }
                }
                // 记住我进来的，并且当前用户已登录，则退出当前用户提示信息。
                else {
                    SecurityUtils.getSubject().logout();
                    throw new AuthenticationException("msg:账号已在其它地方登录，请重新登录。");
                }
            }
        }
        SecurityUser user = getSystemService().getUserByLoginName(principal.getLoginName());
        if (user != null) {
            SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
            List<String> permissions = getSystemService().findAllMenuPermissions();
            // 添加基于Permission的权限信息
            for (String permission : permissions) {
                for (String item : StringUtils.split(permission, ",")) {
                    info.addStringPermission(item);
                }
            }
            // 添加用户权限
            info.addStringPermission("user");
            // 添加用户角色信息
            user.getRoleNameList().forEach(info::addRole);
            // 更新登录IP和时间
            getSystemService().updateUserLoginInfo(user);
            // 记录登录日志
            logger.info("系统登录");
            return info;
        } else {
            return null;
        }
    }

    @Override
    protected void checkPermission(Permission permission, AuthorizationInfo info) {
        authorizationValidate(permission);
        super.checkPermission(permission, info);
    }

    @Override
    protected boolean[] isPermitted(List<Permission> permissions, AuthorizationInfo info) {
        if (permissions != null && !permissions.isEmpty()) {
            permissions.forEach(this::authorizationValidate);
        }
        return super.isPermitted(permissions, info);
    }

    @Override
    public boolean isPermitted(PrincipalCollection principals, Permission permission) {
        authorizationValidate(permission);
        return super.isPermitted(principals, permission);
    }

    @Override
    protected boolean isPermittedAll(Collection<Permission> permissions, AuthorizationInfo info) {
        if (permissions != null && !permissions.isEmpty()) {
            permissions.forEach(this::authorizationValidate);
        }
        return super.isPermittedAll(permissions, info);
    }

    /**
     * 设定密码校验的Hash算法与迭代次数
     */
    @PostConstruct
    public void initCredentialsMatcher() {
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(SecurityService.HASH_ALGORITHM);
        matcher.setHashIterations(SecurityService.HASH_INTERATIONS);
        setCredentialsMatcher(matcher);
    }

    /**
     * 密码校验的默认算法,简单密码比较
     */
    public void initSimpleCredentialsMatcher() {
        setCredentialsMatcher(new SimpleCredentialsMatcher());
    }

    // /**
    // * 清空用户关联权限认证，待下次使用时重新加载
    // */
    // public void clearCachedAuthorizationInfo(Principal principal) {
    // SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
    // clearCachedAuthorizationInfo(principals);
    // }

    /**
     * 清空所有关联认证
     * <p>
     * NOTE: 不需要清空，授权缓存保存到session中
     */
    @Deprecated
    public void clearAllCachedAuthorizationInfo() {
        // Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
        // if (cache != null) {
        // for (Object key : cache.keys()) {
        // cache.remove(key);
        // }
        // }
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取系统业务对象
     */
    public SecurityService getSystemService() {
        if (securityService == null) {
            securityService = SpringContextHolder.getBean(SecurityService.class);
        }
        return securityService;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    /**
     * 授权验证方法
     *
     * @param permission 权限字符串
     */
    private void authorizationValidate(Permission permission) {
        // 模块授权预留接口
        logger.debug("授权方法 {} 验证", permission);
    }

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    /**
     * 授权用户信息
     */
    public static class Principal implements Serializable {

        // ================================================================
        // Constants
        // ================================================================

        /**
         * UID
         */
        private static final long serialVersionUID = 1L;

        // ================================================================
        // Fields
        // ================================================================

        private String id; // 编号
        private String loginName; // 登录名
        private String name; // 姓名
        private boolean mobileLogin; // 是否手机登录

        // private Map<String, Object> cacheMap;

        // ================================================================
        // Constructors
        // ================================================================

        public Principal(SecurityUser user, boolean mobileLogin) {
            this.id = user.getId();
            this.loginName = user.getLoginName();
            this.name = user.getName();
            this.mobileLogin = mobileLogin;
        }

        // ================================================================
        // Public or Protected Methods
        // ================================================================

        /**
         * 获取SESSIONID
         */
        public String getSessionid() {
            try {
                Session session = SecurityUtils.getSession();
                if (session == null) {
                    return "";
                }
                return (String) session.getId();
            } catch (Exception e) {
                return "";
            }
        }

        // ================================================================
        // Getter & Setter
        // ================================================================

        public String getId() {
            return id;
        }

        public String getLoginName() {
            return loginName;
        }

        public String getName() {
            return name;
        }

        public boolean isMobileLogin() {
            return mobileLogin;
        }

        // @JsonIgnore
        // public Map<String, Object> getCacheMap() {
        // if (cacheMap==null){
        // cacheMap = new HashMap<String, Object>();
        // }
        // return cacheMap;
        // }

        @Override
        public String toString() {
            return id;
        }
    }

    // ================================================================
    // Test Methods
    // ================================================================
}
