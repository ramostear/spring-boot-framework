package com.zeyu.framework.core.common.mapper.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class MapConvertor.
 *
 * @author zeyuphoenix
 */
@XmlType(name = "MapConvertor")
@XmlAccessorType(XmlAccessType.FIELD)
public class MapConvertor {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /**
     * The entries.
     */
    private List<MapEntry> entries = new ArrayList<>();

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Adds the entry.
     *
     * @param entry the entry
     */
    public void addEntry(MapEntry entry) {
        entries.add(entry);
    }

    /**
     * Gets the entries.
     *
     * @return the entries
     */
    public List<MapEntry> getEntries() {
        return entries;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    /**
     * The Class MapEntry.
     */
    public static class MapEntry {

        /**
         * The key.
         */
        private String key;

        /**
         * The value.
         */
        private Object value;

        /**
         * Instantiates a new map entry.
         */
        public MapEntry() {
            super();
        }

        /**
         * Instantiates a new map entry.
         *
         * @param entry the entry
         */
        public MapEntry(Map.Entry<String, Object> entry) {
            super();
            this.key = entry.getKey();
            this.value = entry.getValue();
        }

        /**
         * Instantiates a new map entry.
         *
         * @param key   the key
         * @param value the value
         */
        public MapEntry(String key, Object value) {
            super();
            this.key = key;
            this.value = value;
        }

        /**
         * Gets the key.
         *
         * @return the key
         */
        public String getKey() {
            return key;
        }

        /**
         * Sets the key.
         *
         * @param key the new key
         */
        public void setKey(String key) {
            this.key = key;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public Object getValue() {
            return value;
        }

        /**
         * Sets the value.
         *
         * @param value the new value
         */
        public void setValue(Object value) {
            this.value = value;
        }
    }

    // ================================================================
    // Test Methods
    // ================================================================
}
