package com.zeyu.framework.core.web.utils.tag;

import com.zeyu.framework.utils.Encodes;

import org.springframework.stereotype.Service;

/**
 * beetl 使用自定义函数写了encodes tag功能，你可以像使用jsp的标签那样使用
 * <!--:
 * if(encodes.urlEncode('url')) {}
 * -->
 * Created by zeyuphoenix on 2016/3/23.
 */
@Service
public class EncodesExt {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * URL 编码, Encode默认为UTF-8.
     *
     * @return encode result
     */
    public static String urlEncode(String part) {
        return Encodes.urlEncode(part);
    }

    /**
     * URL 解码, Encode默认为UTF-8.
     *
     * @return decode result
     */
    public static String urlDecode(String part) {
        return Encodes.urlDecode(part);
    }

    /**
     * Html 转码.
     *
     * @return html escape result
     */
    public static String escapeHtml(String html) {
        return Encodes.escapeHtml(html);
    }

    /**
     * Html 解码.
     *
     * @return html unescape result
     */
    public static String unescapeHtml(String html) {
        return Encodes.unescapeHtml(html);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
