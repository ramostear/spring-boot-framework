package com.zeyu.framework.core.common.condition;

import com.zeyu.framework.core.common.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 根据配置是否启动swagger生成rest api
 * Created by zeyuphoenix on 16/7/19.
 */
public class SwaggerCondition implements Condition, Constant {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(SwaggerCondition.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        Environment environment = conditionContext.getEnvironment();
        boolean swagger = false;
        if (environment != null) {
            swagger = Boolean.valueOf(environment.getProperty(CONFIGURATION_DEFINED_PREFIX + "swagger", "false"));
        }
        if (logger.isDebugEnabled()) {
            logger.debug("系统初始化检测中,系统{} swagger 生成Rest API.", swagger ? "开启" : "不开启");
        }

        return swagger;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
