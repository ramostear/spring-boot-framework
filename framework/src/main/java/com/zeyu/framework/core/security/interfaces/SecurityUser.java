package com.zeyu.framework.core.security.interfaces;

import java.util.List;

/**
 * shiro权限控制的用户接口,必须实现
 * Created by zeyuphoenix on 16/7/29.
 */
public interface SecurityUser {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * shiro的登录用户id
     */
    String getId();

    /**
     * 登录名
     */
    String getLoginName();

    /**
     * 显示名,可以和上面保存一致
     */
    String getName();

    /**
     * 用户状态获取
     */
    String getLoginFlag();

    /**
     * 用户密码获取
     */
    String getPassword();

    /**
     * 获取角色列表
     */
    List<String> getRoleNameList();
}
