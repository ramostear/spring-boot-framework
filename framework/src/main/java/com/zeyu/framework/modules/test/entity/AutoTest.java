package com.zeyu.framework.modules.test.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zeyu.framework.core.persistence.entity.AutoEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

/**
 * 测试自动生成Mapper,这个类也可自动生成
 * Created by zeyuphoenix on 2017/1/23.
 */
@Table(name = "tb_auto_test")
public class AutoTest extends AutoEntity<AutoTest> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /**
     * 时间
     */
    private Date time;

    /**
     * 状态，0:正常,1:异常
     */
    private Boolean status;

    /**
     * 总数
     */
    @Column(name = "total_count")
    private Long totalCount;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 备注信息
     */
    private Long remarks;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getRemarks() {
        return remarks;
    }

    public void setRemarks(Long remarks) {
        this.remarks = remarks;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
