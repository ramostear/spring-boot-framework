package com.zeyu.framework.modules.sys.entity;

import com.google.common.collect.Lists;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zeyu.framework.core.persistence.entity.TreeEntity;
import com.zeyu.framework.core.security.interfaces.SecurityMenu;
import com.zeyu.framework.utils.StringUtils;

import org.hibernate.validator.constraints.Length;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * 菜单Entity
 */
public class Menu extends TreeEntity<Menu> implements SecurityMenu {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 菜单类型: 菜单、操作、URL
     */
    public static final String MENU_TYPE_MENU = "menu";
    public static final String MENU_TYPE_OPERATION = "operation";
    public static final String MENU_TYPE_URL = "url";

    // ================================================================
    // Fields
    // ================================================================

    // 链接
    private String href;
    // 目标（ menu、operation）
    private String target;
    // 图标
    private String icon;
    // 是否在菜单中显示（1：显示；0：不显示）
    private String isShow;
    // 权限标识
    private String permission;
    // 是否是可用
    private String useable;
    // 菜单的一些显示效果添加
    private String attach;
    // 查询使用
    private String userId;
    // 子菜单列表
    private List<Menu> childList = Lists.newArrayList();

    // ================================================================
    // Constructors
    // ================================================================

    public Menu() {
        super();
        this.sort = 30;
        this.isShow = SHOW;
    }

    public Menu(String id) {
        super(id);
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @JsonBackReference
    @NotNull
    @Override
    public Menu getParent() {
        return parent;
    }

    @Override
    public void setParent(Menu parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return name;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    @Length(min = 1, max = 2000)
    public String getParentIds() {
        return parentIds;
    }

    public void setParentIds(String parentIds) {
        this.parentIds = parentIds;
    }

    @Length(min = 1, max = 100)
    public String getName() {
        return name;
    }

    @Length(max = 2000)
    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getUseable() {
        return useable;
    }

    public void setUseable(String useable) {
        this.useable = useable;
    }

    @Length(max = 20)
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Length(max = 100)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Length(min = 1, max = 1)
    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    @Length(max = 200)
    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getParentId() {
        return parent != null && parent.getId() != null ? parent.getId() : "0";
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    /**
     * 默认存在id为1的节点为根节点，不允许删除.
     *
     * @param id 节点id
     * @return 是否为根节点
     */
    @JsonIgnore
    public static boolean isRoot(String id) {
        return StringUtils.isBlank(id) || StringUtils.equalsIgnoreCase(id, DEFAULT_ROOT_ID);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonIgnore
    public List<Menu> getChildList() {
        return childList;
    }

    public void setChildList(List<Menu> childList) {
        this.childList = childList;
    }
// ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
