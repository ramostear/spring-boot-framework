package com.zeyu.framework.modules.sys.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.core.web.BaseController;
import com.zeyu.framework.modules.sys.entity.Log;
import com.zeyu.framework.modules.sys.entity.User;
import com.zeyu.framework.modules.sys.service.LogService;
import com.zeyu.framework.modules.sys.utils.UserUtils;
import com.zeyu.framework.utils.StringUtils;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * 日志Controller
 */
@Controller
@RequestMapping(value = "/sys/log")
public class LogController extends BaseController {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private LogService logService;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 字典列表页
     */
    @RequiresPermissions("sys:log:view")
    @RequestMapping(value = {"", "index"})
    public String index(Model model) {
        model.addAttribute("userList", UserUtils.getUserIdNameList());
        return "modules/sys/logList";
    }

    /**
     * 字典列表获取
     */
    @RequiresPermissions("sys:log:view")
    @RequestMapping(value = "list")
    @ResponseBody
    public Page<Map<String, Object>> list(HttpServletRequest request) {
        Page<Log> page = new Page<>(request);
        // 查询扩展
        String extraSearch = page.getDatatablesCriterias().getExtraSearch();
        // 转换
        Log log = JsonMapper.getInstance().fromJson(extraSearch, Log.class);
        if (log == null) {
            log = new Log();
        } else {
            // createBy.id 被json忽略,需要转换
            String createBy = readIgnoreValue(extraSearch, "createBy.id");
            if (StringUtils.isNotBlank(createBy)) {
                log.setCreateBy(new User(createBy));
            }
        }

        Page<Log> result = logService.findPage(page, log);

        List<Map<String, Object>> tranMaps = Lists.newArrayList();
        if (result != null) {
            for (Log item : result.getList()) {
                Map<String, Object> tran = Maps.newHashMap();
                if (item.getCreateBy() != null) {
                    tran.put("createName", item.getCreateBy().getName());
                } else {
                    tran.put("createName", "System");
                }
                tranMaps.add(tran);
            }
        }

        return transPage(result, tranMaps);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
