package com.zeyu.framework.modules.sys.dao;


import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.TreeDao;
import com.zeyu.framework.modules.sys.entity.Menu;

import java.util.List;

/**
 * 菜单DAO接口
 */
@MyBatisDao
public interface MenuDao extends TreeDao<Menu> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 根据用户获取菜单
     */
    List<Menu> findByUserId(Menu menu);

    /**
     * 更新菜单排序
     */
    int updateSort(Menu menu);

}
