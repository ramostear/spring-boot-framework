package com.zeyu.framework.modules.sys.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.zeyu.framework.core.web.BaseController;
import com.zeyu.framework.core.web.Result;
import com.zeyu.framework.modules.sys.entity.Menu;
import com.zeyu.framework.modules.sys.service.SystemService;
import com.zeyu.framework.modules.sys.utils.UserUtils;
import com.zeyu.framework.utils.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 菜单Controller
 */
@Controller
@RequestMapping(value = "/sys/menu")
public class MenuController extends BaseController {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private SystemService systemService;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @ModelAttribute("menu")
    public Menu get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return systemService.getMenu(id);
        } else {
            return new Menu();
        }
    }

    /**
     * 菜单列表页
     */
    @RequiresPermissions("sys:menu:view")
    @RequestMapping(value = {"", "index"})
    public String index() {
        return "modules/sys/menuList";
    }

    /**
     * 菜单列表获取
     */
    @RequiresPermissions("sys:menu:view")
    @RequestMapping(value = "list")
    @ResponseBody
    public List<Menu> list() {
        // 获取菜单列表
        List<Menu> list = Lists.newArrayList();
        List<Menu> sourceList = systemService.findAllMenu();
        // 排序
        Menu.sortList(list, sourceList, Menu.getRootId(), true);

        return list;
    }

    /**
     * 保存菜单
     */
    @RequiresPermissions("sys:menu:edit")
    @RequestMapping(value = "save")
    @ResponseBody
    public Result save(@Validated Menu menu) {
        Result result = new Result();
        if (MODE_DEMO) {
            result.setStatus(Result.ERROR);
            result.setMessage("演示模式，不允许操作!");
            return result;
        }
        if (!UserUtils.getUser().isAdmin()) {
            result.setStatus(Result.ERROR);
            result.setMessage("越权操作，只有超级管理员才能添加或修改数据!");

            return result;
        }
        // 验证

        systemService.saveMenu(menu);

        result.setStatus(Result.SUCCESS);
        result.setMessage("保存菜单'" + menu.getName() + "'成功");

        return result;
    }

    /**
     * 删除菜单
     */
    @RequiresPermissions("sys:menu:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public Result delete(Menu menu) {
        Result result = new Result();
        if (MODE_DEMO) {
            result.setStatus(Result.ERROR);
            result.setMessage("演示模式，不允许操作!");
            return result;
        }
        // 根节点不允许删除
        if (Menu.isRoot(menu.getId())) {
            result.setStatus(Result.ERROR);
            result.setMessage("删除菜单失败, 不允许删除顶级菜单或编号为空");
        } else {
            systemService.deleteMenu(menu);
            result.setStatus(Result.SUCCESS);
            result.setMessage("删除菜单成功");
        }
        return result;
    }

    /**
     * 批量修改菜单排序
     */
    @RequiresPermissions("sys:menu:edit")
    @RequestMapping(value = "updateSort")
    @ResponseBody
    public Result updateSort(String[] ids, Integer[] sorts) {
        Result result = new Result();
        if (MODE_DEMO) {
            result.setStatus(Result.ERROR);
            result.setMessage("演示模式，不允许操作!");
            return result;
        }

        // 依次更新?
        for (int i = 0; i < ids.length; i++) {
            Menu menu = new Menu(ids[i]);
            menu.setSort(sorts[i]);
            systemService.updateMenuSort(menu);
        }
        result.setStatus(Result.SUCCESS);
        result.setMessage("保存菜单排序成功");

        return result;
    }

    /**
     * isShowHide是否显示隐藏菜单
     *
     * @param extId      排除的ID
     * @param isShowHide 显示隐藏
     * @return 菜单数
     */
    @RequiresPermissions("user")
    @ResponseBody
    @RequestMapping(value = "treeData")
    public List<Map<String, Object>> treeData(@RequestParam(required = false) String extId,
                                              @RequestParam(required = false) String isShowHide) {
        List<Map<String, Object>> mapList = Lists.newArrayList();
        List<Menu> list = systemService.findAllMenu();
        for (Menu e : list) {
            if (StringUtils.isBlank(extId) || (!extId.equals(e.getId())
                    && !e.getParentIds().contains("," + extId + ","))) {
                if (isShowHide != null && isShowHide.equals("0") && e.getIsShow().equals("0")) {
                    continue;
                }
                Map<String, Object> map = Maps.newHashMap();
                map.put("id", e.getId());
                map.put("pId", e.getParentId());
                String oname = "<i class='" + UserUtils.getMenuIcon(e) + "'></i> " + StringUtils.replace(e.getName(), " ", "");
                map.put("name", oname);
                mapList.add(map);
            }
        }
        return mapList;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
