package com.zeyu.framework.modules.sys.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import com.zeyu.framework.modules.sys.dao.DictDao;
import com.zeyu.framework.modules.sys.entity.Dict;
import com.zeyu.framework.utils.CacheUtils;
import com.zeyu.framework.utils.SpringContextHolder;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 字典工具类
 */
public class DictUtils {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * 字典缓存key
     */
    public static final String CACHE_DICT_MAP = "dictMap";

    // ================================================================
    // Fields
    // ================================================================

    // 字典操作dao
    private static DictDao dictDao = SpringContextHolder.getBean(DictDao.class);

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 根据字典值和类型获取字典label
     *
     * @param value        字典值
     * @param type         字典类型
     * @param defaultValue 字典默认值
     * @return 字典label
     */
    public static String getDictLabel(String value, String type, String defaultValue) {
        if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(value)) {
            for (Dict dict : getDictList(type)) {
                if (type.equals(dict.getType()) && value.equals(dict.getValue())) {
                    return dict.getLabel();
                }
            }
        }
        return defaultValue;
    }

    /**
     * 根据字典值和类型获取字典labels
     *
     * @param values       多个字典值,","隔开
     * @param type         字典类型
     * @param defaultValue 字典默认值
     * @return 字典labels
     */
    public static String getDictLabels(String values, String type, String defaultValue) {
        if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(values)) {
            List<String> valueList = Lists.newArrayList();
            for (String value : StringUtils.split(values, ",")) {
                valueList.add(getDictLabel(value, type, defaultValue));
            }
            return StringUtils.join(valueList, ",");
        }
        return defaultValue;
    }

    /**
     * 根据字典label和类型获取字典值
     *
     * @param label        字典label
     * @param type         字典类型
     * @param defaultValue 字典默认值
     * @return 字典值
     */
    public static String getDictValue(String label, String type, String defaultValue) {
        if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(label)) {
            for (Dict dict : getDictList(type)) {
                if (type.equals(dict.getType()) && label.equals(dict.getLabel())) {
                    return dict.getValue();
                }
            }
        }
        return defaultValue;
    }

    /**
     * 根据类型获取字典列表
     * @param type 字典类型
     * @return 类型列表
     */
    public static List<Dict> getDictList(String type) {
        @SuppressWarnings("unchecked")
        Map<String, List<Dict>> dictMap = (Map<String, List<Dict>>) CacheUtils.get(CACHE_DICT_MAP);
        if (dictMap == null) {
            dictMap = Maps.newHashMap();
            for (Dict dict : dictDao.findAllList(new Dict())) {
                List<Dict> dictList = dictMap.get(dict.getType());
                if (dictList != null) {
                    dictList.add(dict);
                } else {
                    dictMap.put(dict.getType(), Lists.newArrayList(dict));
                }
            }
            CacheUtils.put(CACHE_DICT_MAP, (Serializable) dictMap);
        }
        List<Dict> dictList = dictMap.get(type);
        if (dictList == null) {
            dictList = Lists.newArrayList();
        }
        return dictList;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
