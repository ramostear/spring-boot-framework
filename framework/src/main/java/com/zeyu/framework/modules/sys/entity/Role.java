package com.zeyu.framework.modules.sys.entity;

import com.google.common.collect.Lists;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zeyu.framework.core.persistence.entity.BaseEntity;
import com.zeyu.framework.core.persistence.entity.DataEntity;
import com.zeyu.framework.core.security.interfaces.SecurityRole;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色Entity
 */
public class Role extends DataEntity<Role> implements SecurityRole {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 角色名称
    private String name;
    // 英文名称
    private String enname;
    // 权限类型
    private String type;

    // 原角色名称
    private String oldName;
    // 原英文名称
    private String oldEnname;
    // 是否是系统数据
    private String sysData;
    // 是否是可用
    private String useable;

    // 根据用户ID查询角色列表
    private User user;

    // 拥有菜单列表
    private List<Menu> menuList = Lists.newArrayList();

    // ================================================================
    // Constructors
    // ================================================================

    public Role() {
        super();
        this.useable = YES;
    }

    public Role(String id) {
        super(id);
    }

    public Role(User user) {
        this();
        this.user = user;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 默认存在id为1的节点为admin，不允许删除.
     *
     * @param id 节点id
     * @return 是否为根节点
     */
    @JsonIgnore
    public static boolean isAdmin(String id) {
        return StringUtils.isBlank(id) || StringUtils.equalsIgnoreCase(id, DEFAULT_ROOT_ID);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getUseable() {
        return useable;
    }

    public void setUseable(String useable) {
        this.useable = useable;
    }

    public String getSysData() {
        return sysData;
    }

    public void setSysData(String sysData) {
        this.sysData = sysData;
    }

    @Length(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Length(min = 1, max = 100)
    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    @Length(min = 1, max = 100)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

    public String getOldEnname() {
        return oldEnname;
    }

    public void setOldEnname(String oldEnname) {
        this.oldEnname = oldEnname;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public List<String> getMenuIdList() {
        List<String> menuIdList = Lists.newArrayList();
        menuIdList.addAll(menuList.stream().map(BaseEntity::getId).collect(Collectors.toList()));
        return menuIdList;
    }

    public void setMenuIdList(List<String> menuIdList) {
        menuList = Lists.newArrayList();
        for (String menuId : menuIdList) {
            Menu menu = new Menu();
            menu.setId(menuId);
            menuList.add(menu);
        }
    }

    public String getMenuIds() {
        return StringUtils.join(getMenuIdList(), ",");
    }

    public void setMenuIds(String menuIds) {
        menuList = Lists.newArrayList();
        if (menuIds != null) {
            String[] ids = StringUtils.split(menuIds, ",");
            setMenuIdList(Lists.newArrayList(ids));
        }
    }

    /**
     * 获取权限字符串列表
     */
    public List<String> getPermissions() {
        List<String> permissions = Lists.newArrayList();
        permissions.addAll(menuList.stream()
                .filter(menu -> menu.getPermission() != null && !"".equals(menu.getPermission()))
                .map(Menu::getPermission).collect(Collectors.toList()));
        return permissions;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
