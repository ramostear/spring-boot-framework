package com.zeyu.framework.modules.sys.dao;

import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.modules.sys.entity.User;

import java.util.List;

/**
 * 用户DAO接口
 */
@MyBatisDao
public interface UserDao extends CrudDao<User> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 根据登录名称查询用户
     *
     * @param user 查询信息
     * @return 用户
     */
    User getByLoginName(User user);

    /**
     * 通过OfficeId获取用户列表，仅返回用户id和name（树查询用户时用）
     *
     * @param user 查询信息
     * @return 用户列表
     */
    List<User> findUserByOfficeId(User user);

    /**
     * 查询全部用户数目
     *
     * @return 用户数目
     */
    long findAllCount(User user);

    /**
     * 更新用户密码
     *
     * @param user 查询信息
     * @return 更新结果
     */
    int updatePasswordById(User user);

    /**
     * 更新登录信息，如：登录IP、登录时间
     *
     * @param user 查询信息
     * @return 更新结果
     */
    int updateLoginInfo(User user);

    /**
     * 删除用户角色关联数据
     *
     * @param user 查询信息
     * @return 更新结果
     */
    int deleteUserRole(User user);

    /**
     * 插入用户角色关联数据
     *
     * @param user 查询信息
     * @return 更新结果
     */
    int insertUserRole(User user);

    /**
     * 更新用户信息
     *
     * @param user 查询信息
     * @return 更新结果
     */
    int updateUserInfo(User user);

}
