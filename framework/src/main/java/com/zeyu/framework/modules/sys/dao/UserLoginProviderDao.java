package com.zeyu.framework.modules.sys.dao;

import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.modules.sys.entity.UserLoginProvider;

import java.util.List;

/**
 * 用户第三方登录DAO接口
 */
@MyBatisDao
public interface UserLoginProviderDao extends CrudDao<UserLoginProvider> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 根据type和validate id获取信息
     *
     * @param userLoginProvider 查询信息
     * @return 信息
     */
    UserLoginProvider findProviderByTypeAndValidateId(UserLoginProvider userLoginProvider);

    /**
     * 根据type返回第三方列表
     * @param userLoginProvider 查询信息
     * @return 列表
     */
    List<UserLoginProvider> findProviderListByType(UserLoginProvider userLoginProvider);


}
