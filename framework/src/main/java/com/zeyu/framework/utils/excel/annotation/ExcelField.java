package com.zeyu.framework.utils.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Excel 注解, 用于导入导出
 * Created by zeyuphoenix on 2017/3/1.
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelField {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * 字段类型
     */
    enum Type {
        // 导入导出
        IMPORT_EXPORT,
        // 导出
        EXPORT,
        // 导入
        IMPORT
    }

    /**
     * 字段对齐
     */
    enum  Align {
        // 自动
        AUTO,
        // 靠左
        LEFT,
        // 居中
        CENTER,
        // 右侧
        RIGHT
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 导出的字段名 (默认调用当前字段的"get"方法, 如果导出的字段为对象, 需填写"对象名.对象属性", 如"office.name")
     */
    String value() default "";

    /**
     * 导出字段标题 (需要添加批注请用 "**" 分隔, 仅对导出模板有效)
     */
    String title();

    /**
     * 字段类型(0:导入导出;1:仅导出;2:仅导入)
     */
    Type type() default Type.IMPORT_EXPORT;

    /**
     * 导出字段对齐方式(0:自动;1:靠左;2:居中;3:靠右)
     */
    Align align() default Align.AUTO;

    /**
     * 排序(升序)
     */
    int sort() default 0;

    /**
     * 如果是字典类型青设置字典的type
     */
    String dictType() default "";

    /**
     * 反射类型
     */
    Class<?> fieldType() default Class.class;

    /**
     * 字段归属组 (根据分组导入导出)
     */
    int[] groups() default {};
}
