package com.zeyu.framework.utils.excel.fieldtype;

import com.google.common.collect.Lists;
import com.zeyu.framework.modules.sys.entity.Role;
import com.zeyu.framework.modules.sys.service.SystemService;
import com.zeyu.framework.utils.Collections3;
import com.zeyu.framework.utils.SpringContextHolder;
import com.zeyu.framework.utils.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色列表字段转换
 * Created by zeyuphoenix on 2017/5/23.
 */
public class RoleListType {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // 角色操作 service
    private static SystemService systemService = SpringContextHolder.getBean(SystemService.class);

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取对象值（导入）
     */
    public static Object getValue(String val) {
        List<Role> roleList = Lists.newArrayList();
        List<Role> allRoleList = systemService.findAllRole();
        for (String s : StringUtils.split(val, ",")) {
            roleList.addAll(allRoleList.stream()
                            .filter(e -> StringUtils.trimToEmpty(s).equals(e.getName()))
                            .collect(Collectors.toList()));
        }
        return roleList.size() > 0 ? roleList : null;
    }

    /**
     * 设置对象值（导出）
     */
    public static String setValue(Object val) {
        if (val != null) {
            @SuppressWarnings("unchecked")
            List<Role> roleList = (List<Role>) val;
            return Collections3.extractToString(roleList, "name", ", ");
        }
        return "";
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
