package com.zeyu.framework.utils.mybatis.generate;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;

import java.util.List;

/**
 * mapper方法类
 * Created by zeyuphoenix on 2016/11/2.
 */
public class MapperMethod {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /**
     * 方法返回值类型
     */
    private Symbol.TypeSymbol returnType;
    /**
     * 方法名称
     */
    private String methodName;
    /**
     * 类的环境信息
     */
    private DaoEnvironment daoEnvironment;
    /**
     * 方法参数
     */
    private List<Symbol.VarSymbol> params;

    // ================================================================
    // Constructors
    // ================================================================

    /**
     * 构造函数
     */
    public MapperMethod(DaoEnvironment daoEnvironment, Symbol.MethodSymbol methodSymbol) {
        this.daoEnvironment = daoEnvironment;
        if (!methodSymbol.getTypeParameters().isEmpty()) {
            throw new RuntimeException("不支持有类型参数的方法生成");
        }
        this.returnType = getRealType(methodSymbol.getReturnType());
        this.methodName = DaoGenerateHelper.getMethodName(methodSymbol);
        this.params = methodSymbol.getParameters();
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * 获取方法返回值类型
     */
    public Symbol.TypeSymbol getReturnType() {
        return returnType;
    }

    /**
     * 获取方法名称
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * 获取类的环境信息
     */
    public DaoEnvironment getDaoEnvironment() {
        return daoEnvironment;
    }

    /**
     * 获取方法参数
     */
    public List<Symbol.VarSymbol> getParams() {
        return params;
    }

    /**
     * 获取第一个参数的类型,默认只有一个参数
     */
    public Symbol.TypeSymbol getFirstParamType() {
        return getRealType(params.get(0).asType());
    }

    // ================================================================
    // Private Methods
    // ================================================================

    /**
     * 根据类型获取真正的对象类型
     */
    private Symbol.TypeSymbol getRealType(Type type) {
        //List<String> to return String
        if (type.allparams().size() > 0)
            return getRealType(type.allparams().get(0));

        if (type instanceof Type.TypeVar) {
            return daoEnvironment.getRealTypeByTypeParameter(type);
        }

        return type.tsym;
    }

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
