package com.zeyu.framework.tools.mq.receiver;

import com.zeyu.framework.tools.mq.struct.MQMessage;
import com.zeyu.framework.tools.mq.struct.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RPC接收到request请求后的实际处理类
 * Created by zeyuphoenix on 2016/12/31.
 */
public class RPCMessageHandler {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(RPCMessageHandler.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 这里模拟处理
     * @param message 请求消息
     * @return 应答消息
     */
    public MQMessage handleMessage(MQMessage message) {
        logger.info("receiver RPC request message: {}", message);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            logger.error("interrupted error: ", e);
        }
        message.getMqHeader().setType(MessageType.SERVICE_RESPONSE.value());
        message.setBody("I receiver your message and validate it!".getBytes());
        logger.info("RPC transform complete");
        return message;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
