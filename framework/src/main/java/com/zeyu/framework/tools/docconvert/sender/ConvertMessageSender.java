package com.zeyu.framework.tools.docconvert.sender;

import com.zeyu.framework.core.configuration.RabbitMqConfiguration;
import com.zeyu.framework.tools.docconvert.struct.DocFileInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 发送MQ message,不同message使用不同的模板
 * Created by zeyuphoenix on 2016/12/31.
 */
@Component
public class ConvertMessageSender {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ConvertMessageSender.class);

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    @Qualifier(value = "rabbitTemplate")
    private AmqpTemplate amqpTemplate;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 点对点-doc转换信息
     */
    public void sendTransFileInfo(DocFileInfo message) {
        logger.info("发送doc转换信息: " + message);
        this.amqpTemplate.convertAndSend(RabbitMqConfiguration.QUEUE_DOC_CONVERT, message);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
