package com.zeyu.framework.tools.report.charts;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 包含了图初始化的各种信息的扩展型ChartData
 *
 * 目前主要用于后台生产图片，包含了图片生产的所有信息，后面考虑扩展到可以后台直接根据url生产图对象.
 * Created by zeyuphoenix on 16/9/3.
 */
public class ExtendChartData implements Serializable {

    // ================================================================
    // Constants
    // ================================================================

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // remote 默认是远程模式
    // 本地生产图片使用false
    /** The remote. */
    private boolean remote = false;
    // 'line' 'bar' 'pie' ‘radar' 'gauge' 'scatter' 'other'(需要设置slefDefine,自己加载config和ajax)
    /** The type. */
    private String type = "line";
    // 数据请求url地址
    // 本地生产图片设置为空
    /** The dataurl. */
    private String dataurl = null;
    // 数据请求参数
    // 本地生产图片设置为空
    // 由于格式多变，前端使用，请传入json字符串，友使用者转换为专有json
    /** The params. */
    private String params = null;
    // 本地模式才生效
    /** The datas. */
    private ChartData datas = null;
    // 横轴的名称，也对应表格的名称列(如时间、协议名等)
    // 不设置则显示统计项
    /** The angle. */
    private String angle = "统计项";

    // 图的标题和副标题
    /** The tilte. */
    private String tilte = "";

    /** The subtitle. */
    private String subtitle = null;
    // 是否堆积,柱图\线图设置后成为堆积图
    /** The stack. */
    private boolean stack = false;
    // 是否是面积图,线图设置后成为面积图,使用默认填充
    /** The area. */
    private boolean area = false;
    // 饼图和雷达图提示的标题名称
    /** The name. */
    private String name = "";
    // 饼图和雷达图是否有特殊效果,中空、渲染等
    /** The radius. */
    private boolean radius = false;
    // 线图和柱图、雷达图必须设置 字符串数组,类似['上行流量','下行流量']
    /** The legends. */
    private List<String> legends = Lists.newArrayList();

    // 不使用自定义格式化，使用共通格式化时，是按1024还是按1000进行格式化，默认是1000
    // flow or package(other)
    /** The format type. */
    private String formatType = "package";
    // 横纵坐标、雷达、甘特图显示的格式化函数
    // 是一个java script function
    /** The label formatter. */
    private String labelFormatter = null;
    // tooltip的格式化函数
    // 是一个java script function
    /** The tooltip formatter. */
    private String tooltipFormatter = null;
    // 图表的双击事件，函数,参数为param
    /**
     * <pre>
     * function(param){
     *      //type = param.type
     *      //seriesIndex = param.seriesIndex
     *      //dataIndex = param.dataIndex
     *      console.log(param);
     * }
     * eg:
     * param结构:
     *      data: 53
     *      dataIndex: 14
     *      event: MouseEvent
     *      name: "2013-03-15 20:15:34"
     *      seriesIndex: 0
     *      seriesName: "上行流量"
     *      type: "dblclick"
     *      value: 53
     * </pre>
     *
     * 是一个java script function
     */
    private String dblclick = null;

    // 目前只是为了统一,只支持最基本的图,line、bar、pie、scatter、radar、gauge几种,如果需要其它图类,或者是复杂的图,比如 线图和饼图混合
    // 等,请将slefDefine设置为true, 注意:这个设置为true后,将不会自动为你请求数据,你需要自己发送ajax请求,按照echarts官网的例子,把
    // 值和配置依次设置到下面的config属性中
    private boolean slefDefine = false;
    // 额外接口,必须 和图的option保持一致,不要随便覆盖
    // 由于格式多变，前端使用，请传入json字符串，友使用者转换为专有json
    /** The config. */
    private String config = null;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /*
     * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Checks if is remote.
     *
     * @return true, if is remote
     */
    public boolean isRemote() {
        return remote;
    }

    /**
     * Sets the remote.
     *
     * @param remote
     *            the new remote
     */
    public void setRemote(boolean remote) {
        this.remote = remote;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the dataurl.
     *
     * @return the dataurl
     */
    public String getDataurl() {
        return dataurl;
    }

    /**
     * Sets the dataurl.
     *
     * @param dataurl
     *            the new dataurl
     */
    public void setDataurl(String dataurl) {
        this.dataurl = dataurl;
    }

    /**
     * Gets the params.
     *
     * @return the params
     */
    public String getParams() {
        return params;
    }

    /**
     * Sets the params.
     *
     * @param params
     *            the new params
     */
    public void setParams(String params) {
        this.params = params;
    }

    /**
     * Gets the datas.
     *
     * @return the datas
     */
    public ChartData getDatas() {
        return datas;
    }

    /**
     * Sets the datas.
     *
     * @param datas
     *            the new datas
     */
    public void setDatas(ChartData datas) {
        this.datas = datas;
    }

    /**
     * Gets the tilte.
     *
     * @return the tilte
     */
    public String getTilte() {
        return tilte;
    }

    /**
     * Sets the tilte.
     *
     * @param tilte
     *            the new tilte
     */
    public void setTilte(String tilte) {
        this.tilte = tilte;
    }

    /**
     * Gets the subtitle.
     *
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * Sets the subtitle.
     *
     * @param subtitle
     *            the new subtitle
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * Checks if is stack.
     *
     * @return true, if is stack
     */
    public boolean isStack() {
        return stack;
    }

    /**
     * Sets the stack.
     *
     * @param stack
     *            the new stack
     */
    public void setStack(boolean stack) {
        this.stack = stack;
    }

    /**
     * Checks if is area.
     *
     * @return true, if is area
     */
    public boolean isArea() {
        return area;
    }

    /**
     * Sets the area.
     *
     * @param area
     *            the new area
     */
    public void setArea(boolean area) {
        this.area = area;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Checks if is radius.
     *
     * @return true, if is radius
     */
    public boolean isRadius() {
        return radius;
    }

    /**
     * Sets the radius.
     *
     * @param radius
     *            the new radius
     */
    public void setRadius(boolean radius) {
        this.radius = radius;
    }

    /**
     * Gets the legends.
     *
     * @return the legends
     */
    public List<String> getLegends() {
        return legends;
    }

    /**
     * Sets the legends.
     *
     * @param legends
     *            the new legends
     */
    public void setLegends(List<String> legends) {
        this.legends = legends;
    }

    /**
     * Gets the format type.
     *
     * @return the format type
     */
    public String getFormatType() {
        return formatType;
    }

    /**
     * Sets the format type.
     *
     * @param formatType
     *            the new format type
     */
    public void setFormatType(String formatType) {
        this.formatType = formatType;
    }

    /**
     * Gets the label formatter.
     *
     * @return the label formatter
     */
    public String getLabelFormatter() {
        return labelFormatter;
    }

    /**
     * Sets the label formatter.
     *
     * @param labelFormatter
     *            the new label formatter
     */
    public void setLabelFormatter(String labelFormatter) {
        this.labelFormatter = labelFormatter;
    }

    /**
     * Gets the tooltip formatter.
     *
     * @return the tooltip formatter
     */
    public String getTooltipFormatter() {
        return tooltipFormatter;
    }

    /**
     * Sets the tooltip formatter.
     *
     * @param tooltipFormatter
     *            the new tooltip formatter
     */
    public void setTooltipFormatter(String tooltipFormatter) {
        this.tooltipFormatter = tooltipFormatter;
    }

    /**
     * Gets the dblclick.
     *
     * @return the dblclick
     */
    public String getDblclick() {
        return dblclick;
    }

    /**
     * Sets the dblclick.
     *
     * @param dblclick
     *            the new dblclick
     */
    public void setDblclick(String dblclick) {
        this.dblclick = dblclick;
    }

    public boolean isSlefDefine() {
        return slefDefine;
    }

    public void setSlefDefine(boolean slefDefine) {
        this.slefDefine = slefDefine;
    }

    /**
     * Gets the config.
     *
     * @return the config
     */
    public String getConfig() {
        return config;
    }

    /**
     * Sets the config.
     *
     * @param config
     *            the new config
     */
    public void setConfig(String config) {
        this.config = config;
    }

    /**
     * Gets the angle.
     *
     * @return the angle
     */
    public String getAngle() {
        return angle;
    }

    /**
     * Sets the angle.
     *
     * @param angle
     *            the new angle
     */
    public void setAngle(String angle) {
        this.angle = angle;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
