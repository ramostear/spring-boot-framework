package com.zeyu.framework.tools.debug;

import com.google.common.io.Resources;
import com.zeyu.framework.monitors.server.utils.OsCheck;
import com.zeyu.framework.utils.ExecUtils;
import com.zeyu.framework.utils.NetUtils;
import com.zeyu.framework.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

/**
 * 启动debug工具
 * Created by zeyuphoenix on 16/9/23.
 */
@Service
public class DebugStarter implements EnvironmentAware {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(DebugStarter.class);

    /**
     * 配置信息前缀
     */
    private static final String CONFIG_PREFIX = "framework.debugtool.";

    // ================================================================
    // Fields
    // ================================================================

    // 是否启动debug工具
    static boolean ENABLE = false;

    // debug工具端口
    static int PORT = 5678;

    // 读取配置信息
    private RelaxedPropertyResolver propertyResolver;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public void setEnvironment(Environment environment) {
        this.propertyResolver = new RelaxedPropertyResolver(environment,
                CONFIG_PREFIX);
        // 读取配置参数
        PORT = Integer.valueOf(this.propertyResolver.getProperty("port", "5678"));
        ENABLE = Boolean.valueOf(this.propertyResolver.getProperty("enable", "false"));
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 根据配置信息启动debug工具
     */
    @PostConstruct
    public void start() {
        String javahome = this.propertyResolver.getProperty("javahome", "");
        if (ENABLE) {
            try {

                // 当正常关闭服务时,log也会被关闭,但当非正常关闭server时,比如使用kill -9 pid,log无法关闭
                // 为了防止重复启动,这里验证端口如果占用,将不启动log服务
                boolean available = NetUtils.isPortAvailable(PORT);
                if (!available) {
                    logger.warn("port {} is usage, please check log service is started or not, if other service " +
                            "use this port, please change port in application.properties.", PORT);
                    return;
                }

                logger.info("load debug tools with class file");
                String file = Resources.getResource("debug").getFile();
                logger.debug("debug tool path is {}", file);
                String path = new File(file).getCanonicalPath();
                logger.debug("debug tool transaction path is {}", path);

                logger.info("启动debug工具");
                StringBuilder builder = new StringBuilder("");
                if (javahome != null && StringUtils.isNotBlank(javahome)) {
                    builder.append(javahome);
                    builder.append(File.separator);
                }
                builder.append("java");
                builder.append(" -Djava.ext.dirs=");
                // 根据系统加载不同jar
                OsCheck.OSType osType = OsCheck.getOperatingSystemType();
                String ext;
                if (osType == OsCheck.OSType.Windows) {
                    // windows
                    ext = "tools-windows";
                } else if (osType == OsCheck.OSType.MacOS) {
                    //mac
                    ext = "tools-macos";
                } else {
                    // linux
                    ext = "tools-linux";
                }
                builder.append(path);
                builder.append(File.separator);
                builder.append(ext);
                builder.append(" -jar ");
                builder.append(path);
                builder.append(File.separator);
                builder.append("log.jar");
                builder.append(" ");
                builder.append(PORT);
                builder.append(" ");
                builder.append(true);

                logger.info("start debug tools command is {}", builder.toString());
                ExecUtils.exec(builder.toString(), true);
            } catch (IOException e) {
                logger.error("启动debug工具失败", e);
            }
        }
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
