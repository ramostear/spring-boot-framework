package com.zeyu.framework.tools.param.service;

import com.zeyu.framework.core.service.TreeService;
import com.zeyu.framework.tools.param.dao.ParamDao;
import com.zeyu.framework.tools.param.entity.Param;
import com.zeyu.framework.tools.param.utils.ParamUtils;
import com.zeyu.framework.utils.CacheUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 机构Service
 */
@Service
@Transactional(readOnly = true)
public class ParamService extends TreeService<ParamDao, Param> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public List<Param> findList(Param param) {
        param.setParentIds(param.getParentIds() + "%");
        return dao.findList(param);
    }

    @Transactional
    @Override
    public void save(Param param) {
        super.save(param);
        CacheUtils.remove(ParamUtils.CACHE_PARAM_MAP);
    }

    @Transactional
    @Override
    public void delete(Param param) {
        super.delete(param);
        CacheUtils.remove(ParamUtils.CACHE_PARAM_MAP);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
