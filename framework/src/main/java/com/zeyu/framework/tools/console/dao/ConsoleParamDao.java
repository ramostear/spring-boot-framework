package com.zeyu.framework.tools.console.dao;

import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.tools.console.entity.ConsoleParam;

import java.util.List;

/**
 * console 连接配置处理dao
 * Created by zeyuphoenix on 16/8/27.
 */
@MyBatisDao
public interface ConsoleParamDao extends CrudDao<ConsoleParam> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 通过连接id获取所有配置
     */
    List<ConsoleParam> findByConnId(ConsoleParam consoleParam);

    /**
     * 删除属于连接id的所有配置
     */
    int deleteByConnId(ConsoleParam consoleParam);

    /**
     * 通过type获取所有配置
     */
    List<ConsoleParam> findByType(ConsoleParam consoleParam);

}
