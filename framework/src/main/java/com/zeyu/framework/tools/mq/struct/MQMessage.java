package com.zeyu.framework.tools.mq.struct;

import com.zeyu.framework.tools.mq.utils.EncryptUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * MQ传输的公共类,所有的message都是基于这个
 * Created by zeyuphoenix on 2016/12/31.
 */
public class MQMessage implements Serializable {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(MQMessage.class);

    // ================================================================
    // Fields
    // ================================================================

    // 消息头
    private MQHeader mqHeader;
    // 是否加密
    private boolean encrypt = false;
    // 消息体
    private byte[] body;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public String toString() {

        try {
            if (mqHeader != null && body != null) {
                if (encrypt) {
                    return "MQMessage [ header=" + mqHeader
                            + " body=" + EncryptUtils.decode(new String(body, EncryptUtils.CHART_SET)) + "]";
                } else {
                    return "MQMessage [ header=" + mqHeader
                            + " body=" + new String(body, EncryptUtils.CHART_SET) + "]";
                }
            }
        } catch (Exception e) {
            logger.error("", e);
        }
        return "MQMessage [ header=" + mqHeader + "]";
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public MQHeader getMqHeader() {
        return mqHeader;
    }

    public void setMqHeader(MQHeader mqHeader) {
        this.mqHeader = mqHeader;
    }

    public boolean isEncrypt() {
        return encrypt;
    }

    public void setEncrypt(boolean encrypt) {
        this.encrypt = encrypt;
    }

    public byte[] getBody() {
        if (encrypt) {
            try {
                this.body = EncryptUtils.decode(new String(body, EncryptUtils.CHART_SET)).getBytes();
            } catch (Exception e) {
                logger.error("", e);
            }
        }
        return body;
    }

    public void setBody(byte[] body) {
        if (encrypt) {
            try {
                this.body = EncryptUtils.encode(new String(body, EncryptUtils.CHART_SET)).getBytes();
            } catch (Exception e) {
                logger.error("", e);
            }
        } else {
            this.body = body;
        }
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
