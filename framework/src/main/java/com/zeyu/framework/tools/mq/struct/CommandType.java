package com.zeyu.framework.tools.mq.struct;

/**
 * 命令类型
 * Created by zeyuphoenix on 2016/12/31.
 */
public enum CommandType {

    // ================================================================
    // Constants
    // ================================================================

    // 业务无关类型
    // 失去连接
    LOST_CONNECTION((byte) 0),
    // 连接
    CONNECTION((byte) 1),

    // 未登录用户
    // 示例
    EXAMPLE((byte) 2),

    // 平台命令
    // 获取配置信息
    FETCH_INFO((byte) 3),
    // 平台指令重启
    DEVICE_REBOOT((byte) 4),
    // 平台指令重命名
    DEVICE_RENAME((byte) 5),
    // 平台指令恢复出厂设置
    DEVICE_FACTORY_RESET((byte) 6),
    // 平台指令升级软件版本
    DEVICE_UPGRADE_SOFT((byte) 7),
    // 平台指令修改配置信息
    DEVICE_UPDATE_CONFIG((byte) 8);

    // ================================================================
    // Fields
    // ================================================================

    // 具体值
    private byte value;

    // ================================================================
    // Constructors
    // ================================================================

    CommandType(byte value) {
        this.value = value;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    public byte value() {
        return this.value;
    }

}
