package com.zeyu.framework.tools.schedule.dao;


import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.tools.console.entity.ConsoleParam;
import com.zeyu.framework.tools.schedule.entity.ScheduleHistory;

import java.util.List;

/**
 * 任务历史DAO接口
 */
@MyBatisDao
public interface ScheduleHistoryDao extends CrudDao<ScheduleHistory> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 通过group和名称获取历史记录
     */
    List<ScheduleHistory> findByGroupAndName(ScheduleHistory scheduleHistory);
}
