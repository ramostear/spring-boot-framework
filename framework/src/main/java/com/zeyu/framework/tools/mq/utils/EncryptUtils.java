package com.zeyu.framework.tools.mq.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * 加密和解密 message 的body字符串
 * Created by zeyuphoenix on 2016/12/31.
 */
public class EncryptUtils {

    // ================================================================
    // Constants
    // ================================================================

    // 加密信息
    public static final String IV_PARA = "0103050608091224";
    public static final String CHART_SET = "UTF-8";
    // 加密Key
    public static final String SPEC_KEY = "zeyuphoenix^8!*&";
    // 加密算法
    public static final String CIPHER = "AES/CBC/PKCS5Padding";

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 加密字符串
     */
    public static String encode(String cleartext)
            throws Exception {
        IvParameterSpec zeroIv = new IvParameterSpec(IV_PARA.getBytes());
        SecretKeySpec key = new SecretKeySpec(SPEC_KEY.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance(CIPHER);
        cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
        byte[] encryptedData = cipher.doFinal(cleartext.getBytes(CHART_SET));

        return Base64.encodeBase64String(encryptedData);
    }

    /**
     * 解密字符串
     */
    public static String decode(String encrypted)
            throws Exception {
        byte[] byteMi = Base64.decodeBase64(encrypted);
        IvParameterSpec zeroIv = new IvParameterSpec(IV_PARA.getBytes());
        SecretKeySpec key = new SecretKeySpec(SPEC_KEY.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance(CIPHER);
        cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
        byte[] decryptedData = cipher.doFinal(byteMi);

        return new String(decryptedData, CHART_SET);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
