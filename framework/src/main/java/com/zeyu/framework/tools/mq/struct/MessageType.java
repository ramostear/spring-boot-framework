package com.zeyu.framework.tools.mq.struct;

/**
 * 消息类型
 * Created by zeyuphoenix on 2016/12/31.
 */
public enum MessageType {

    // ================================================================
    // Constants
    // ================================================================

    // 服务请求
    SERVICE_REQUEST((byte) 0),
    // 服务应答
    SERVICE_RESPONSE((byte) 1),
    // 普通信息
    NORMAL((byte) 2),
    // 登录请求
    LOGIN_REQUEST((byte) 3),
    // 登录应答
    LOGIN_RESPONSE((byte) 4),
    // 心跳请求
    HEARTBEAT_REQUEST((byte) 5),
    // 心跳应答
    HEARTBEAT_RESPONSE((byte) 6);

    // ================================================================
    // Fields
    // ================================================================

    // 具体值
    private byte value;

    // ================================================================
    // Constructors
    // ================================================================

    MessageType(byte value) {
        this.value = value;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    public byte value() {
        return this.value;
    }

}
