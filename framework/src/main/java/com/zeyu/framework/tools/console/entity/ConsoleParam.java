package com.zeyu.framework.tools.console.entity;

import com.zeyu.framework.core.persistence.entity.SimpleEntity;

/**
 * console 连接参数配置类
 * Created by zeyuphoenix on 2017/3/15.
 */
public class ConsoleParam extends SimpleEntity<ConsoleParam> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    /** 连接类型 */
    public static final String TYPE_ALL = "all";
    public static final String TYPE_SSH = "ssh";
    public static final String TYPE_RDP = "rdp";
    public static final String TYPE_TELNET = "telnet";
    public static final String TYPE_VNC = "vnc";
    public static final String TYPE_UNKNOWN = "unknown";

    // ================================================================
    // Fields
    // ================================================================

    // 连接ID
    private String connId;
    // 类型
    private String type = TYPE_SSH;

    // 数据值
    private String value;
    // 标签名
    private String label;
    // 备注
    private String remarks;

    // ================================================================
    // Constructors
    // ================================================================

    /**
     * 基本构造参数
     */
    public ConsoleParam() {
    }

    /**
     * 连接构造参数
     */
    public ConsoleParam(String connId) {
        this.connId = connId;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getConnId() {
        return connId;
    }

    public void setConnId(String connId) {
        this.connId = connId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
