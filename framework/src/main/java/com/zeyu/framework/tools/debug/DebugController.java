package com.zeyu.framework.tools.debug;

import com.zeyu.framework.core.web.BaseController;
import com.zeyu.framework.core.web.Result;
import com.zeyu.framework.utils.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 调试工具Controller
 */
@Controller
@RequestMapping(value = "/tools/debug")
public class DebugController extends BaseController {

    // ================================================================
    // Constants
    // ================================================================

    /** debug工具的认证用户名和密码, 考虑到这个功能极度危险,只在这里 */
    public static final String DEBUG_NAME = "debugManager";
    public static final String DEBUG_PASS = "123.com$%^";

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 调试工具页
     */
    @RequiresPermissions("tools:debug:view")
    @RequestMapping(value = {"", "index"})
    public String index() {
        return "modules/tools/debugTool";
    }

    /**
     * 工具认证
     */
    @RequiresPermissions("tools:debug:edit")
    @RequestMapping(value = "auth")
    @ResponseBody
    public Result auth(String name, String pass, HttpServletRequest request) {
        Result result = new Result();
        if (MODE_DEMO) {
            result.setStatus(Result.ERROR);
            result.setData("");
            result.setMessage("演示模式，不允许操作!");
            return result;
        }
        if (!DebugStarter.ENABLE) {

            result.setStatus(Result.ERROR);
            result.setData("");
            result.setMessage("Debug工具没有启动,请联系管理员检查服务器!");
            return result;
        }

        if (StringUtils.equals(name, DEBUG_NAME) && StringUtils.equals(pass, DEBUG_PASS)) {

            String basePath;
            // 如果是nginx代理方式,经过rewrite可以使用80处理所有请求
            String proxy = request.getHeader("X-NginX-Proxy");
            if (StringUtils.isNoneBlank(proxy) && StringUtils.equals(proxy, "true")) {
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/logtrace/index";
            } else {
                // 如果是tomcat方式,必须通过端口方式访问
                basePath = request.getScheme() + "://" + request.getServerName() + ":" + DebugStarter.PORT;
            }

            result.setData(basePath);
            result.setStatus(Result.SUCCESS);
            result.setMessage("授权认证成功");
        } else {
            result.setStatus(Result.ERROR);
            result.setData("");
            result.setMessage("授权认证失败,请重新认证");
        }

        return result;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
