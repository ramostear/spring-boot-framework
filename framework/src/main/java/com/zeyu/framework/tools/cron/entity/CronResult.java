package com.zeyu.framework.tools.cron.entity;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 页面需要的表达式结果.
 * Created by zeyuphoenix on 16/9/1.
 */
public class CronResult implements CronConst, Serializable {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /** 生产的cron表达式，类似 0 0/5 * * * ? 格式. */
    private String cronExpression;

    /** 开始时间,类似 yyyy-MM-dd HH:mm:ss 格式. */
    private String startTime;

    /**
     * 执行时间,类似 List<yyyy-MM-dd HH:mm:ss> 格式<br/>
     * 一般取得以后的八个.
     */
    private List<String> scheduleTimes = Lists.newArrayList();

    /** 选择结果. */
    private List<CronOption> cronOptions = Lists.newArrayList();

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /*
    * (non-Javadoc)
    *
    * @see java.lang.Object#toString()
    */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the cron expression.
     *
     * @return the cron expression
     */
    public String getCronExpression() {
        return cronExpression;
    }

    /**
     * Sets the cron expression.
     *
     * @param cronExpression
     *            the new cron expression
     */
    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    /**
     * Gets the start time.
     *
     * @return the start time
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * Sets the start time.
     *
     * @param startTime
     *            the new start time
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * Gets the schedule times.
     *
     * @return the schedule times
     */
    public List<String> getScheduleTimes() {
        return scheduleTimes;
    }

    /**
     * Sets the schedule times.
     *
     * @param scheduleTimes
     *            the new schedule times
     */
    public void setScheduleTimes(List<String> scheduleTimes) {
        this.scheduleTimes = scheduleTimes;
    }

    /**
     * Gets the option results.
     *
     * @return the option results
     */
    public List<CronOption> getCronOptions() {
        return cronOptions;
    }

    /**
     * Sets the option results.
     *
     * @param cronOptions
     *            the new option results
     */
    public void setCronOptions(List<CronOption> cronOptions) {
        this.cronOptions = cronOptions;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
