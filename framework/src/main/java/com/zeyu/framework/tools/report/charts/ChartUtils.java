package com.zeyu.framework.tools.report.charts;

import com.zeyu.framework.utils.FileUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.output.ByteArrayOutputStream;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 图的工具类
 * Created by zeyuphoenix on 16/9/3.
 */
public class ChartUtils {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
     *
     * @param imgFilePath 图片地址
     * @return 字节数组字符串
     */
    public static String encodeImage(String imgFilePath) throws IOException {

        // 读取图片字节数组
        byte[] data = FileUtils.readFileToByteArray(new File(imgFilePath));
        // 对字节数组Base64编码
        // 返回Base64编码过的字节数组字符串
        return Base64.encodeBase64String(data);
    }

    /**
     * 对字节数组字符串进行Base64解码并生成图片
     *
     * @param imgStr 字符串
     * @param imgFilePath 图片地址
     */
    public static boolean generateImage(String imgStr, String imgFilePath) throws IOException {
        if (imgStr == null)
            // 图像数据为空
            return false;
        // Base64解码
        byte[] bytes = Base64.decodeBase64(imgStr);
        for (int i = 0; i < bytes.length; ++i) {
            if (bytes[i] < 0) {
                // 调整异常数据
                bytes[i] += 256;
            }
        }

        // 生成图片
        convertImage(new ByteArrayInputStream(bytes), imgFilePath);

        return true;
    }

    /**
     * 对字节数组字符串进行Base64解码并生成图片数组,不保存图片,传递数组
     *
     * @param imgStr 字符串
     */
    public static byte[] generateImage(String imgStr) throws IOException {
        if (imgStr == null)
            // 图像数据为空
            return new byte[0];
        // Base64解码
        byte[] bytes = Base64.decodeBase64(imgStr);
        for (int i = 0; i < bytes.length; ++i) {
            if (bytes[i] < 0) {
                // 调整异常数据
                bytes[i] += 256;
            }
        }

        // 生成图片
        /**
         * 自动选择一个注册的ImageReader然后对给定的input进行解码，返回一个BufferedImage实例，不成功的话返回null。
         * 实际上这个方法先从input取得一个ImageInputStream对象，然后调用重载函数read(InputStream input);
         */
        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(bytes));

        /**
         * 把内存图象im按照formatName指定的格式写入文件output。RenderedImage是java.awt.
         * image中定义的一个接口，上面提到的BufferedImage类实现了这个接口。formatName可以是"bmp" "png"
         * "jpg" "gif" "tiff"等
         */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", baos);

        return baos.toByteArray();
    }

    /**
     * 转换格式
     */
    public static void convertImage(String src, String dest) throws IOException {

        File srcFile = new File(src);
        /**
         * 自动选择一个注册的ImageReader然后对给定的input进行解码，返回一个BufferedImage实例，不成功的话返回null。
         * 实际上这个方法先从input取得一个ImageInputStream对象，然后调用重载函数read(InputStream input);
         */
        BufferedImage bufferedImage = ImageIO.read(srcFile);

        File destFile = new File(dest);
        /**
         * 把内存图象im按照formatName指定的格式写入文件output。RenderedImage是java.awt.
         * image中定义的一个接口，上面提到的BufferedImage类实现了这个接口。formatName可以是"bmp" "png"
         * "jpg" "gif" "tiff"等
         */
        ImageIO.write(bufferedImage, "png", destFile);
    }

    /**
     * 转换格式
     */
    public static void convertImage(InputStream in, String dest) throws IOException {

        /**
         * 自动选择一个注册的ImageReader然后对给定的input进行解码，返回一个BufferedImage实例，不成功的话返回null。
         * 实际上这个方法先从input取得一个ImageInputStream对象，然后调用重载函数read(InputStream input);
         */
        BufferedImage bufferedImage = ImageIO.read(in);

        File destFile = new File(dest);
        /**
         * 把内存图象im按照formatName指定的格式写入文件output。RenderedImage是java.awt.
         * image中定义的一个接口，上面提到的BufferedImage类实现了这个接口。formatName可以是"bmp" "png"
         * "jpg" "gif" "tiff"等
         */
        ImageIO.write(bufferedImage, "png", destFile);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
