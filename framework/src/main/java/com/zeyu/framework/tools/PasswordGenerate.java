package com.zeyu.framework.tools;

import com.zeyu.framework.core.security.utils.Digests;
import com.zeyu.framework.utils.Encodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 生成密码,加密
 * Created by zeyuphoenix on 2016/3/17.
 */
public class PasswordGenerate {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(PasswordGenerate.class);

    /**
     * 加密常量
     */
    private static final int HASH_INTERATIONS = 1024;
    private static final int SALT_SIZE = 8;

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    /**
     * 生成安全的密码，生成随机的16位salt并经过1024次 sha-1 hash
     *
     * @param plainPassword 明文密码
     */
    public static String entryptPassword(String plainPassword) {
        byte[] salt = Digests.generateSalt(SALT_SIZE);
        byte[] hashPassword = Digests.sha1(plainPassword.getBytes(), salt, HASH_INTERATIONS);
        return Encodes.encodeHex(salt) + Encodes.encodeHex(hashPassword);
    }

    /**
     * 验证密码
     *
     * @param plainPassword 明文密码
     * @param password      密文密码
     * @return 验证成功返回true
     */
    public static boolean validatePassword(String plainPassword, String password) {
        byte[] salt = Encodes.decodeHex(password.substring(0, 16));
        byte[] hashPassword = Digests.sha1(plainPassword.getBytes(), salt, HASH_INTERATIONS);
        return password.equals(Encodes.encodeHex(salt) + Encodes.encodeHex(hashPassword));
    }

    /**
     * 字符串生成MD5
     *
     * @param plain 原始字符串
     * @return md5字符串
     */
    public static String md5(String plain) {
        byte[] arrs = plain.getBytes(Charset.forName("UTF-8"));
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(arrs);
            byte[] result = md5.digest();
            for (byte b : result) {
                if (Integer.toHexString(0xFF & b).length() == 1)
                    sb.append("0").append(Integer.toHexString(0xFF & b));
                else
                    sb.append(Integer.toHexString(0xFF & b));
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error("md5 error: ", e);
        }
        return sb.toString();
    }

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
