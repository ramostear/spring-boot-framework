package com.zeyu.framework.tools.report;

import com.google.common.collect.Maps;
import com.zeyu.framework.core.web.BaseController;
import com.zeyu.framework.tools.report.dynamic.ReportUtils;
import com.zeyu.framework.utils.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

/**
 * pdf的控制器,后台提供pdf文件,前台显示
 * Created by zeyuphoenix on 16/9/5.
 */
@Controller
@RequestMapping(value = "/tools/pdf")
public class PDFController extends BaseController {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 请求返回pdf页面,再通过ajax请求url返回真正pdf内容
     * @param url 转发的url地址
     * @param params url地址的参数,复杂参数需要base64
     * @param base64 是否base64参数
     */
    @RequestMapping(value = {"index", ""})
    public String index(String url, String params, boolean base64, Model model) {
        model.addAttribute("base64", false);
        // 接收需要展示的url请求,分为2种
        if (StringUtils.isNoneBlank(url)) {

            if (StringUtils.endsWith(url, ".pdf")) {
                // 1.普通的pdf文件,在这里统一处理,返回base64转换的pdf
                model.addAttribute("url", "/tools/pdf/content");
                try {
                    model.addAttribute("params", URLDecoder.decode(url, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    logger.error("decode error: ", e);
                }
            } else {
                // 2.其它预览类的生成请求,需要操作,先生成pdf,再返回base64转换后的pdf
                model.addAttribute("base64", base64);
                model.addAttribute("params", params);
                model.addAttribute("url", url);
            }
        } else {
            // 显示默认文件
            model.addAttribute("url", "/tools/pdf/content");
            model.addAttribute("params", "tmp/generate/Java注入方式线上调试代码.pdf");
        }

        return "modules/tools/pdf";
    }

    /**
     * 获取base64形式的pdf数据
     */
    @ResponseBody
    @RequestMapping(value = "content")
    public Map<String, String> content(String params) {
        Map<String, String> content = Maps.newHashMap();

        Resource resource = new ClassPathResource(params);
        String pdfContent = "";
        try {
            if (resource.getFile() != null && resource.getFile().exists()) {
                pdfContent = ReportUtils.pdfToBase64(resource.getFile());
            }
        } catch (IOException e) {
            logger.error("获取base64形式的pdf数据", e);
        }

        content.put("pdfData", ReportUtils.BASE64_MARKER + pdfContent);
        return content;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
