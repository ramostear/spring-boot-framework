package com.zeyu.framework.tools.report.charts;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 封装web请求的echarts的对象，为了接口的统一，都封装成一样的格式返回
 *
 * <pre>
 *
 * 目前支持的图格式:line(线)、bar(柱)、pi(e饼)、radar(雷达)、gauge(仪表)，后继有需求添加
 *
 *         1.线图、柱图、散点图的数据格式是一样的，可以互相切换
 *          {
 *              'xAxis': ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月'],
 *              'series': [
 *                   [120, 232, 301, 434, 390, 530, 320, 120, 250],
 *                   [150, 160, 190, 150, 232, 201, 154, 390, 110]
 *              ]
 *          }
 *
 *        2.对于pie图来说,每个项目只有一个值,但是为了接口统一,我们把值也放入数组中
 *          {
 *              'xAxis': ['张三', '李四', '王五', '赵六', '小明'],
 *              'series': [
 *                   [120, 232, 301, 434, 390]
 *              ]
 *          }
 *        3.对于radar图来说,每个项目的值要和indicator对应，由于
 *          indicator也需要动态，类似[{text:'角度',max:1000},{{text:'方法',max:2000}]
 *          所以项目里多了max项目，表示每个∠的max值
 *          {
 *              'xAxis': ['流量', '报文', '速率', '攻击', '病毒'],
 *              'max': [1000, 1500, 100, 300, 200],
 *              'series': [
 *                   [820, 1232, 31, 241, 100],
 *                   [320, 800, 91, 234, 10]
 *               ]
 *          }
 *
 *        4.gauge,每个图只有一个显示值，需要一个最大值和一个最小值
 *          min 和 max 是否动态 需要考虑.
 *           {
 *               "xAxis": ["速率"],
 *               "min": [0],
 *               "max": [200],
 *               "series": [
 *                   [120]
 *               ]
 *           }
 * </pre>
 * Created by zeyuphoenix on 16/9/3.
 */
public class ChartData implements Serializable {

    // ================================================================
    // Constants
    // ================================================================

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 横轴或者称之为角度
    /** The x axis. */
    private List<String> xAxis = Lists.newArrayList();
    // 纵轴或者称之为指标
    /** The series. */
    private List<List<Number>> series = Lists.newArrayList();
    // 最小值或者最左值
    /** The min. */
    private List<Number> min = Lists.newArrayList();
    // 最大值或者最右值
    /** The max. */
    private List<Number> max = Lists.newArrayList();

    // legends
    private List<String> legends = Lists.newArrayList();

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /*
     * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the x axis.
     *
     * @return the x axis
     */
    public List<String> getxAxis() {
        return xAxis;
    }

    /**
     * Sets the x axis.
     *
     * @param xAxis
     *            the new x axis
     */
    public void setxAxis(List<String> xAxis) {
        this.xAxis = xAxis;
    }

    /**
     * Gets the series.
     *
     * @return the series
     */
    public List<List<Number>> getSeries() {
        return series;
    }

    /**
     * Sets the series.
     *
     * @param series
     *            the new series
     */
    public void setSeries(List<List<Number>> series) {
        this.series = series;
    }

    /**
     * Gets the min.
     *
     * @return the min
     */
    public List<Number> getMin() {
        return min;
    }

    /**
     * Sets the min.
     *
     * @param min
     *            the new min
     */
    public void setMin(List<Number> min) {
        this.min = min;
    }

    /**
     * Gets the max.
     *
     * @return the max
     */
    public List<Number> getMax() {
        return max;
    }

    /**
     * Sets the max.
     *
     * @param max
     *            the new max
     */
    public void setMax(List<Number> max) {
        this.max = max;
    }

    public List<String> getLegends() {
        return legends;
    }

    public void setLegends(List<String> legends) {
        this.legends = legends;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
