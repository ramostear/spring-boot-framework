package com.zeyu.framework.tools.param.entity;

import com.zeyu.framework.core.persistence.entity.TreeEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 参数Entity
 * Created by zeyuphoenix on 16/9/20.
 */
public class Param extends TreeEntity<Param>{

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 类型（1：分类；2：组；3：配置）
     */
    public static final String PARAM_TYPE_ALL = "0";    // 缓存用
    public static final String PARAM_TYPE_CATEGORY = "1";
    public static final String PARAM_TYPE_GROUP = "2";
    public static final String PARAM_TYPE_INFO = "3";

    // ================================================================
    // Fields
    // ================================================================

    // 数据值
    private String value;
    // 标签名
    private String label;
    // 类型
    private String type;
    // 描述
    private String description;
    // 图标
    private String icon;
    // 是否固定, 0默认为不固定，1固定；固定就不能再去修改
    private int fixed = FIX_FLAG_NO;

    // 父名称,数据库无关,经常使用,通过对象返回
    private String parentLabel;

    // ================================================================
    // Constructors
    // ================================================================

    public Param() {
        super();
        this.type = PARAM_TYPE_CATEGORY;
    }

    public Param(String id) {
        super(id);
        this.type = PARAM_TYPE_CATEGORY;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public Param getParent() {
        return parent;
    }

    @Override
    public void setParent(Param parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return name;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Length(min = 1, max = 1)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getFixed() {
        return fixed;
    }

    public void setFixed(int fixed) {
        this.fixed = fixed;
    }

    public String getParentLabel() {
        if (parent != null) {
            return parent.getLabel();
        }
        return parentLabel;
    }

    public void setParentLabel(String parentLabel) {
        this.parentLabel = parentLabel;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
