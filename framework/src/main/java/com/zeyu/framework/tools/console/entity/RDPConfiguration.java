package com.zeyu.framework.tools.console.entity;

/**
 * RDP 协议参数配置类
 * Created by zeyuphoenix on 16/10/17.
 */
public class RDPConfiguration extends BasicConfiguration {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // Network parameters
    // RDP connections require a hostname or IP address defining the destination machine.
    // The RDP port is defined to be 3389, and will be this value in most cases.
    // You only need to specify the RDP port if you are not using port 3389.
    /**
     * The port the RDP server is listening on, usually 3389. This parameter is optional.
     * If this is not specified, the default of 3389 will be used.
     */
    private int port = 3389;

    // Authentication and security
    // RDP provides authentication through the use of a username, password, and optional domain.
    /**
     * The username to use to authenticate, if any. This parameter is optional.
     */
    private String username;
    /**
     * The password to use when attempting authentication, if any. This parameter is optional.
     */
    private String password;
    /**
     * The domain to use when attempting authentication, if any. This parameter is optional.
     */
    private String domain;
    /**
     * The security mode to use for the RDP connection. This mode dictates how data will be encrypted and what
     * type of authentication will be performed, if any. By default, standard RDP encryption is requested,
     * as it is the most widely supported.
     *
     * rdp | nla | tls | any
     */
    private String security;
    /**
     * If set to "true", the certificate returned by the server will be ignored, even if that certificate
     * cannot be validated. This is useful if you universally trust the server and your connection to the server,
     * and you know that the server's certificate cannot be validated (for example, if it is self-signed).
     */
    private boolean ignore_cert = false;
    /**
     * If set to "true", authentication will be disabled. Note that this refers to authentication that takes place
     * while connecting. Any authentication enforced by the server over the remote desktop session
     * (such as a login dialog) will still take place. By default, authentication is enabled and only used
     * when requested by the server.
     * If you are using NLA, authentication must be enabled by definition.
     */
    private boolean disable_auth = false;

    // Session settings
    // RDP sessions will typically involve the full desktop environment of a normal user. Alternatively,
    // you can manually specify a program to use instead of the RDP server's default shell,
    // or connect to the administrative console.
    /**
     * When connecting to the RDP server, Guacamole will normally provide its own hostname as the name of the client.
     * If this parameter is specified, Guacamole will use its value instead.
     *     On Windows RDP servers, this value is exposed within the session as the CLIENTNAME environment variable.
     */
    private String client_name;
    /**
     * If set to "true", you will be connected to the console (admin) session of the RDP server.
     */
    private boolean console = false;
    /**
     * The full path to the program to run immediately upon connecting. This parameter is optional.
     */
    private String initial_program;
    /**
     * The server-side keyboard layout. This is the layout of the RDP server and has nothing to do with the keyboard
     * layout in use on the client. The Guacamole client is independent of keyboard layout. The RDP protocol,
     * however, is not independent of keyboard layout, and Guacamole needs to know the keyboard layout of
     * the server in order to send the proper keys when a user is typing.
     *
     * en-us-qwerty | de-de-qwertz | fr-fr-azerty | it-it-qwerty | sv-se-qwerty | failsafe
     *
     * If your server's keyboard layout is not yet supported, this option should work in the meantime.
     */
    private String server_layout;

    // Display settings
    /**
     * The color depth to request, in bits-per-pixel. This parameter is optional.
     * If specified, this must be either 8, 16, 24, or 32. Regardless of what value is chosen here,
     * if a particular update uses less than 256 colors, Guacamole will always send that update as a 256-color PNG.
     */
    private int color_depth = -1;
    /**
     * The width of the display to request, in pixels. This parameter is optional. If this value is not specified,
     * the width of the connecting client display will be used instead.
     */
    private int width = -1;
    /**
     * The height of the display to request, in pixels. This parameter is optional. If this value is not specified,
     * the height of the connecting client display will be used instead.
     */
    private int height = -1;
    /**
     * The desired effective resolution of the client display, in DPI. This parameter is optional.
     * If this value is not specified, the resolution and size of the client display will be used together
     * to determine, heuristically, an appropriate resolution for the RDP session.
     */
    private String dpi;

    // Device redirection
    // Device redirection refers to the use of non-display devices over RDP. Console's RDP support currently
    // allows redirection of audio, printing, and disk access, some of which require additional configuration
    // in order to function properly.
    /**
     * Audio is enabled by default in both the client and in libguac-client-rdp. If you are concerned about
     * bandwidth usage, or sound is causing problems, you can explicitly disable sound by setting this parameter to "true".
     */
    private boolean disable_audio = false;
    /**
     * Printing is disabled by default, but with printing enabled, RDP users can print to a virtual printer that sends
     * a PDF containing the document printed to the Guacamole client. Enable printing by setting this parameter to "true".
     * Printing support requires GhostScript to be installed. If guacd cannot find the gs executable when printing,
     * the print attempt will fail.
     */
    private boolean enable_printing = false;
    /**
     * File transfer is disabled by default, but with file transfer enabled, RDP users can transfer files to and from
     * a virtual drive which persists on the Guacamole server. Enable file transfer support by setting this parameter to "true".
     * Files will be stored in the directory specified by the "drive-path" parameter, which is required if file
     * transfer is enabled.
     */
    private boolean enable_drive = false;
    /**
     * The directory on the Guacamole server in which transferred files should be stored. This directory must be
     * accessible by guacd and both readable and writable by the user that runs guacd. This parameter does not
     * refer to a directory on the RDP server.
     * If file transfer is not enabled, this parameter is ignored.
     */
    private String drive_path;
    /**
     * If set to "true", and file transfer is enabled, the directory specified by the drive-path parameter will
     * automatically be created if it does not yet exist. Only the final directory in the path will be
     * created - if other directories earlier in the path do not exist, automatic creation will fail, and an error
     * will be logged.
     * By default, the directory specified by the drive-path parameter will not automatically be created,
     * and attempts to transfer files to a non-existent directory will be logged as errors.
     * If file transfer is not enabled, this parameter is ignored.
     */
    private boolean create_drive_path = false;
    /**
     * If set to "true", audio will be explicitly enabled in the console (admin) session of the RDP server.
     * Setting this option to "true" only makes sense if the console parameter is also set to "true".
     */
    private boolean console_audio = false;
    /**
     * A comma-separated list of static channel names to open and expose as pipes. If you wish to communicate
     * between an application running on the remote desktop and JavaScript, this is the best way to do it.
     * Console will open an outbound pipe with the name of the static channel. If JavaScript needs to communicate
     * back in the other direction, it should respond by opening another pipe with the same name.
     */
    private String static_channels;

    // RDP + SFTP
    // Console can provide file transfer over SFTP even when the remote desktop is otherwise being accessed
    // through RDP and not SSH
    /**
     * Whether file transfer should be enabled. If set to "true", the user will be allowed to upload
     * or download files from the specified server using SFTP. If omitted, SFTP will be disabled.
     */
    private boolean enable_sftp = false;
    /**
     * The hostname or IP address of the server hosting SFTP. This parameter is optional. If omitted, the hostname of
     * the RDP server specified with the hostname parameter will be used.
     */
    private String sftp_hostname;
    /**
     * The port the SSH server providing SFTP is listening on, usually 22. This parameter is optional.
     * If omitted, the standard port of 22 will be used.
     */
    private int sftp_port = 22;
    /**
     * The username to authenticate as when connecting to the specified SSH server for SFTP. This parameter
     * is optional if a username is specified for the RDP connection. If omitted, the value provided
     * for the username parameter will be use.
     */
    private String sftp_username;
    /**
     * The password to use when authenticating with the specified SSH server for SFTP.
     */
    private String sftp_password;
    /**
     * The entire contents of the private key to use for public key authentication.
     * If this parameter is not specified, public key authentication will not be used.
     * The private key must be in OpenSSH format, as would be generated by the OpenSSH ssh-keygen utility.
     */
    private String sftp_private_key;
    /**
     * The passphrase to use to decrypt the private key for use in public key authentication.
     * This parameter is not needed if the private key does not require a passphrase.
     */
    private String sftp_passphrase;
    /**
     * The directory to upload files to if they are simply dragged and dropped, and thus otherwise lack
     * a specific upload location. This parameter is optional. If omitted, the default upload location of
     * the SSH server providing SFTP will be used.
     */
    private String sftp_directory;

    // Performance flags
    /**
     * If set to "true", enables rendering of the desktop wallpaper. By default, wallpaper will be disabled,
     * such that unnecessary bandwidth need not be spent redrawing the desktop.
     */
    private boolean enable_wallpaper = false;
    /**
     * If set to "true", enables use of theming of windows and controls. By default, theming within RDP sessions is disabled.
     */
    private boolean enable_theming = false;
    /**
     * If set to "true", text will be rendered ;with smooth edges. Text over RDP is rendered with rough edges by default,
     * as this reduces the number of colors used by text, and thus reduces the bandwidth required for the connection.
     */
    private boolean enable_font_smoothing = false;
    /**
     * If set to "true", the contents of windows will be displayed as windows are moved. By default,
     * the RDP server will only draw the window border while windows are being dragged.
     */
    private boolean enable_full_window_drag = false;
    /**
     * If set to "true", graphical effects such as transparent windows and shadows will be allowed. By default,
     * such effects, if available, are disabled.
     */
    private boolean enable_desktop_composition = false;
    /**
     * If set to "true", menu open and close animations will be allowed. Menu animations are disabled by default.
     */
    private boolean enable_menu_animations = false;

    // RemoteApp
    // Recent versions of Windows provide a feature called RemoteApp which allows individual applications to be used
    // over RDP, without providing access to the full desktop environment. If your RDP server has this feature enabled
    // and configured, you can configure connections to use those individual applications.
    /**
     * Specifies the RemoteApp to start on the remote desktop. If supported by your remote desktop server,
     * this application, and only this application, will be visible to the user.
     *    Windows requires a special notation for the names of remote applications. The names of remote applications
     * must be prefixed with two vertical bars. For example, if you have created a remote application on your server
     * for notepad.exe and have assigned it the name "notepad", you would set this parameter to: "||notepad".
     */
    private String remote_app;
    /**
     * The working directory, if any, for the remote application. This parameter has no effect if RemoteApp is not in use.
     */
    private String remote_app_dir;
    /**
     * The command-line arguments, if any, for the remote application. This parameter has no effect if RemoteApp is not in use.
     */
    private String remote_app_args;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public String getProtocol() {
        return ConsoleParam.TYPE_RDP;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    public boolean isIgnore_cert() {
        return ignore_cert;
    }

    public void setIgnore_cert(boolean ignore_cert) {
        this.ignore_cert = ignore_cert;
    }

    public boolean isDisable_auth() {
        return disable_auth;
    }

    public void setDisable_auth(boolean disable_auth) {
        this.disable_auth = disable_auth;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public boolean isConsole() {
        return console;
    }

    public void setConsole(boolean console) {
        this.console = console;
    }

    public String getInitial_program() {
        return initial_program;
    }

    public void setInitial_program(String initial_program) {
        this.initial_program = initial_program;
    }

    public String getServer_layout() {
        return server_layout;
    }

    public void setServer_layout(String server_layout) {
        this.server_layout = server_layout;
    }

    public int getColor_depth() {
        return color_depth;
    }

    public void setColor_depth(int color_depth) {
        this.color_depth = color_depth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getDpi() {
        return dpi;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }

    public boolean isDisable_audio() {
        return disable_audio;
    }

    public void setDisable_audio(boolean disable_audio) {
        this.disable_audio = disable_audio;
    }

    public boolean isEnable_printing() {
        return enable_printing;
    }

    public void setEnable_printing(boolean enable_printing) {
        this.enable_printing = enable_printing;
    }

    public boolean isEnable_drive() {
        return enable_drive;
    }

    public void setEnable_drive(boolean enable_drive) {
        this.enable_drive = enable_drive;
    }

    public String getDrive_path() {
        return drive_path;
    }

    public void setDrive_path(String drive_path) {
        this.drive_path = drive_path;
    }

    public boolean isCreate_drive_path() {
        return create_drive_path;
    }

    public void setCreate_drive_path(boolean create_drive_path) {
        this.create_drive_path = create_drive_path;
    }

    public boolean isConsole_audio() {
        return console_audio;
    }

    public void setConsole_audio(boolean console_audio) {
        this.console_audio = console_audio;
    }

    public String getStatic_channels() {
        return static_channels;
    }

    public void setStatic_channels(String static_channels) {
        this.static_channels = static_channels;
    }

    public boolean isEnable_sftp() {
        return enable_sftp;
    }

    public void setEnable_sftp(boolean enable_sftp) {
        this.enable_sftp = enable_sftp;
    }

    public String getSftp_hostname() {
        return sftp_hostname;
    }

    public void setSftp_hostname(String sftp_hostname) {
        this.sftp_hostname = sftp_hostname;
    }

    public int getSftp_port() {
        return sftp_port;
    }

    public void setSftp_port(int sftp_port) {
        this.sftp_port = sftp_port;
    }

    public String getSftp_username() {
        return sftp_username;
    }

    public void setSftp_username(String sftp_username) {
        this.sftp_username = sftp_username;
    }

    public String getSftp_password() {
        return sftp_password;
    }

    public void setSftp_password(String sftp_password) {
        this.sftp_password = sftp_password;
    }

    public String getSftp_private_key() {
        return sftp_private_key;
    }

    public void setSftp_private_key(String sftp_private_key) {
        this.sftp_private_key = sftp_private_key;
    }

    public String getSftp_passphrase() {
        return sftp_passphrase;
    }

    public void setSftp_passphrase(String sftp_passphrase) {
        this.sftp_passphrase = sftp_passphrase;
    }

    public String getSftp_directory() {
        return sftp_directory;
    }

    public void setSftp_directory(String sftp_directory) {
        this.sftp_directory = sftp_directory;
    }

    public boolean isEnable_wallpaper() {
        return enable_wallpaper;
    }

    public void setEnable_wallpaper(boolean enable_wallpaper) {
        this.enable_wallpaper = enable_wallpaper;
    }

    public boolean isEnable_theming() {
        return enable_theming;
    }

    public void setEnable_theming(boolean enable_theming) {
        this.enable_theming = enable_theming;
    }

    public boolean isEnable_font_smoothing() {
        return enable_font_smoothing;
    }

    public void setEnable_font_smoothing(boolean enable_font_smoothing) {
        this.enable_font_smoothing = enable_font_smoothing;
    }

    public boolean isEnable_full_window_drag() {
        return enable_full_window_drag;
    }

    public void setEnable_full_window_drag(boolean enable_full_window_drag) {
        this.enable_full_window_drag = enable_full_window_drag;
    }

    public boolean isEnable_desktop_composition() {
        return enable_desktop_composition;
    }

    public void setEnable_desktop_composition(boolean enable_desktop_composition) {
        this.enable_desktop_composition = enable_desktop_composition;
    }

    public boolean isEnable_menu_animations() {
        return enable_menu_animations;
    }

    public void setEnable_menu_animations(boolean enable_menu_animations) {
        this.enable_menu_animations = enable_menu_animations;
    }

    public String getRemote_app() {
        return remote_app;
    }

    public void setRemote_app(String remote_app) {
        this.remote_app = remote_app;
    }

    public String getRemote_app_dir() {
        return remote_app_dir;
    }

    public void setRemote_app_dir(String remote_app_dir) {
        this.remote_app_dir = remote_app_dir;
    }

    public String getRemote_app_args() {
        return remote_app_args;
    }

    public void setRemote_app_args(String remote_app_args) {
        this.remote_app_args = remote_app_args;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
