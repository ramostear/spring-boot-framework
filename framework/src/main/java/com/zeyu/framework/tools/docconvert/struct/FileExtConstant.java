package com.zeyu.framework.tools.docconvert.struct;

/**
 * 文件后缀常量
 * Created by zeyuphoenix on 2017/2/28.
 */
public interface FileExtConstant {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * 点
     */
    String DOT = ".";

    /**
     * txt后缀
     */
    String FILETYPE_TXT = "txt";

    /**
     * txt完整后缀
     */
    String SUFF_TXT = DOT + FILETYPE_TXT;

    /**
     * PDF后缀
     */
    String FILETYPE_PDF = "pdf";

    /**
     * PDF完整后缀
     */
    String SUFF_PDF = DOT + FILETYPE_PDF;

    /**
     * png图片后缀
     */
    String FILETYPE_PNG = "png";

    /**
     * png图片完整后缀
     */
    String SUFF_IMAGE = DOT + FILETYPE_PNG;

    /**
     * swf后缀
     */
    String FILETYPE_SWF = "swf";

    /**
     * swf完整后缀
     */
    String SUFF_SWF = DOT + FILETYPE_SWF;

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

}
