package com.zeyu.framework.tools.console.entity;

/**
 * telnet协议参数配置类
 * Telnet is a text protocol and provides similar functionality to SSH. By nature, it is not encrypted,
 * and does not provide support for file transfer.
 * Created by zeyuphoenix on 16/10/17.
 */
public class TelnetConfiguration extends BasicConfiguration {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // Network parameters
    /**
     * The port the telnet server is listening on, usually 23. This parameter is optional. If this is not specified,
     * the default of 23 will be used.
     */
    private int port = 23;

    // Authentication
    /**
     * The username to use to authenticate, if any.
     */
    private String username;
    /**
     * The password to use when attempting authentication, if any.
     */
    private String password;

    // Display settings
    /**
     * The color scheme to use for the terminal emulator used by telnet connections.
     *
     * black-white|gray-black|green-black|white-black
     */
    private String color_scheme;
    /**
     * The name of the font to use. This parameter is optional. If not specified,
     * the default of "monospace" will be used instead.
     */
    private String font_name;
    /**
     * The size of the font to use, in points. This parameter is optional. If not specified,
     * the default of 12 will be used instead.
     */
    private int font_size = 12;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public String getProtocol() {
        return ConsoleParam.TYPE_TELNET;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getColor_scheme() {
        return color_scheme;
    }

    public void setColor_scheme(String color_scheme) {
        this.color_scheme = color_scheme;
    }

    public String getFont_name() {
        return font_name;
    }

    public void setFont_name(String font_name) {
        this.font_name = font_name;
    }

    public int getFont_size() {
        return font_size;
    }

    public void setFont_size(int font_size) {
        this.font_size = font_size;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
