package com.zeyu.framework.tools.cron.entity;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Cron用到的常量
 * Created by zeyuphoenix on 16/9/1.
 */
public interface CronConst {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * cron的类型
     */
    /** 不指定 */
    int NON = 0;
    /** 每【秒、分、时、日、月、周、年】触发 */
    int EVERY = 1;
    /** 循环触发 从n秒到m秒 */
    // 目前这种形式反解析会变把-消除，变为4(DEFINED)模式,暂时不实现反解析
    int RANGE = 2;
    /** 递增触发 从n秒开始，每m秒触发一次 */
    int INCREASE = 3;
    /** 指定具体【秒、分、时、日、月、周、年】触发 */
    int DEFINED = 4;

    /** 表达式类型 */
    int SECOND = 0;
    int MINUTE = 1;
    int HOUR = 2;
    int DAY_OF_MONTH = 3;
    int MONTH = 4;
    int DAY_OF_WEEK = 5;
    int YEAR = 6;

    /** 特殊字段类型 */
    int ALL_SPEC_INT = 99;  // '*'
    int NO_SPEC_INT = 98;   // '?'
    Integer ALL_SPEC = ALL_SPEC_INT;
    Integer NO_SPEC = NO_SPEC_INT;

    /** 最大年数 */
    int MAX_YEAR = Calendar.getInstance().get(Calendar.YEAR) + 100;

    /** 月Map */
    Map<String, Integer> monthMap = new HashMap<String, Integer>() {{
        put("JAN", 0);
        put("FEB", 1);
        put("MAR", 2);
        put("APR", 3);
        put("MAY", 4);
        put("JUN", 5);
        put("JUL", 6);
        put("AUG", 7);
        put("SEP", 8);
        put("OCT", 9);
        put("NOV", 10);
        put("DEC", 11);
    }};
    /** 周Map */
    Map<String, Integer> dayMap = new HashMap<String, Integer>() {{
        put("SUN", 1);
        put("MON", 2);
        put("TUE", 3);
        put("WED", 4);
        put("THU", 5);
        put("FRI", 6);
        put("SAT", 7);
    }};

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

}
