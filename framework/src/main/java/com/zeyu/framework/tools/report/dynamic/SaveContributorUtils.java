package com.zeyu.framework.tools.report.dynamic;

import com.google.common.collect.Lists;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.util.JRClassLoader;
import net.sf.jasperreports.view.JRSaveContributor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * 报表生成方式的工具类,目前支持doc、xls、pdf、mxls
 * Created by zeyuphoenix on 16/9/4.
 */
public class SaveContributorUtils {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(SaveContributorUtils.class);

    // ================================================================
    // Fields
    // ================================================================

    /**
     * 默认的可输出类型,csv和html暂时不输出
     */
    private static final String[] DEFAULT_CONTRIBUTORS = {
            "net.sf.jasperreports.view.save.JRPdfSaveContributor",
            "net.sf.jasperreports.view.save.JRDocxSaveContributor",
            // "net.sf.jasperreports.view.save.JRHtmlSaveContributor",
            "net.sf.jasperreports.view.save.JRSingleSheetXlsSaveContributor",
            "net.sf.jasperreports.view.save.JRMultipleSheetsXlsSaveContributor"
            // ,
            // "net.sf.jasperreports.view.save.JRCsvSaveContributor"
    };

    /**
     * 构造函数
     */
    private static final Class<?>[] CONSTRUCTOR_SIGNATURE = {
            JasperReportsContext.class, Locale.class, ResourceBundle.class};

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 创建输出类型
     */
    public static List<JRSaveContributor> createBuiltinContributors(
            JasperReportsContext context, Locale locale,
            ResourceBundle resourceBundle) {
        ArrayList<JRSaveContributor> contributors = Lists.newArrayList();
        for (String contributorClassName : DEFAULT_CONTRIBUTORS) {
            try {
                Class<?> saveContribClass = JRClassLoader
                        .loadClassForName(contributorClassName);
                Constructor<?> constructor = saveContribClass
                        .getConstructor(CONSTRUCTOR_SIGNATURE);
                JRSaveContributor saveContrib = (JRSaveContributor) constructor
                        .newInstance(context, locale, resourceBundle);
                contributors.add(saveContrib);
            } catch (Exception e) {
                // shouldn't happen, but log anyway
                logger.warn("Error creating save contributor of type "
                        + contributorClassName, e);
            }
        }
        return contributors;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
