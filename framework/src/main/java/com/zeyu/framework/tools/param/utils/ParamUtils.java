package com.zeyu.framework.tools.param.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.zeyu.framework.tools.param.dao.ParamDao;
import com.zeyu.framework.tools.param.entity.Param;
import com.zeyu.framework.utils.CacheUtils;
import com.zeyu.framework.utils.SpringContextHolder;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 参数工具类
 */
public class ParamUtils {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * 参数缓存key
     */
    public static final String CACHE_PARAM_MAP = "paramMap";

    // ================================================================
    // Fields
    // ================================================================

    // 参数操作dao
    private static ParamDao paramDao = SpringContextHolder.getBean(ParamDao.class);

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 根据Param类型获取图标
     */
    public static String getIconByType(Param param) {
        if (param == null || param.getType() == null) {
            return "fa fa-gears";
        }
        if (param.getIcon() != null) {
            return param.getIcon();
        }

        // 类型（1：分类；2：组；3：配置；）
        if (StringUtils.equals(param.getType(), "1")) {
            return "fa fa-support";
        } else if (StringUtils.equals(param.getType(), "2")) {
            return "fa fa-folder-open";
        } else if (StringUtils.equals(param.getType(), "3")) {
            return "fa fa-cog";
        } else {
            return "fa fa-gears";
        }
    }

    /**
     * 根据id获取配置
     * @param id 参数id
     * @return 参数配置
     */
    public static Param getParamById(String id) {

        List<Param> alls = getParamList(Param.PARAM_TYPE_ALL);
        for (Param param : alls) {
            if (StringUtils.equals(param.getId(), id)) {
                return param;
            }
        }
        return null;
    }

    /**
     * 根据name获取配置
     * @param name 参数name
     * @return 参数配置
     */
    public static Param getParamByName(String name) {

        List<Param> alls = getParamList(Param.PARAM_TYPE_ALL);
        for (Param param : alls) {
            if (StringUtils.equals(param.getName(), name)) {
                return param;
            }
        }
        return null;
    }

    /**
     * 根据id获取子参数列表(最近一级)
     * @param id 参数id
     * @return 参数配置
     */
    public static List<Param> getChildParamList(String id) {

        List<Param> childList = Lists.newArrayList();

        List<Param> alls = getParamList(Param.PARAM_TYPE_ALL);
        childList.addAll(alls
                .stream()
                .filter(param -> StringUtils.equals(param.getParentId(), id))
                .collect(Collectors.toList())
        );

        return childList;
    }


    /**
     * 根据类型获取参数列表(type为0放置all)
     * @param type 参数类型
     * @return 类型列表
     */
    public static List<Param> getParamList(String type) {
        @SuppressWarnings("unchecked")
        Map<String, List<Param>> paramMap = (Map<String, List<Param>>) CacheUtils.get(CACHE_PARAM_MAP);
        if (paramMap == null) {
            paramMap = Maps.newHashMap();
            List<Param> alls = paramDao.findList(new Param());
            for (Param param : alls) {
                List<Param> dictList = paramMap.get(param.getType());
                if (dictList != null) {
                    dictList.add(param);
                } else {
                    paramMap.put(param.getType(), Lists.newArrayList(param));
                }
            }
            paramMap.put(Param.PARAM_TYPE_ALL, alls);

            CacheUtils.put(CACHE_PARAM_MAP, paramMap);
        }
        List<Param> paramList = paramMap.get(type);
        if (paramList == null) {
            paramList = Lists.newArrayList();
        }
        return paramList;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
