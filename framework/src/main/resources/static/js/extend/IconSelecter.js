/**
 * (c) 2015-2020
 * Author: zeyuphoenix
 * License: www.blogjava.net/zeyuphoenix
 * version: 1.0.0
 */
(function (root, factory) {

    "user strict";

    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else {
        root.IconSelecter = factory($);
    }
}(this, function ($) {

    "user strict";

    // 提供项目的默认配置项,保持风格统一,覆盖配置项
    var defaultOptions = {
        // dom 节点
        databind: null,
        // 对话框弹出对话框需要外部定义一个div
        databind_dialog: null,
        // 选择框标题
        title: '',
        // 编号,为了排重，加入了随机数
        id: '',
        // 名称 -- 提交name（表单input）
        name: '',
        // 值 -- 提交value
        value: '',
        // 值 -- 显示value
        labelValue: '无'
    };

    // 图标选择框对象,创建的唯一入口
    var IconSelecter = function (options) {
        this.options = $.extend(true, {}, defaultOptions, options);
        if(this.options.id == null || this.options.id == '') {
            this.options.id= 'id_' + Math.round(Math.random() * 10000);
        }
        this.__init();
    };

    // 图标选择框的配置
    // 图标选择框的对象方法
    IconSelecter.prototype = {

        // 构造
        constructor: IconSelecter,

        // 获取默认的常量

        // 初始化
        __init: function () {
            var self = this;
            // 取得默认配置

            // 创建表格HTML
            this.$element = $(this.options.databind);
            this.$element.html(this.createTemplate());

            // 设置默认图片到html之中, tabs和icons 一一对应
            var tabs = [
                {
                    'id': 'web',
                    'name': 'web应用',
                    'icon': 'fa fa-fw fa-lg fa-internet-explorer'
                },
                {
                    'id': 'txet',
                    'name': '文本编辑',
                    'icon': 'fa fa-fw fa-lg fa-text-width'
                },
                {
                    'id': 'dirt',
                    'name': '方向',
                    'icon': 'fa fa-fw fa-lg fa-arrows'
                },
                {
                    'id': 'form',
                    'name': '表单',
                    'icon': 'fa fa-fw fa-lg fa-check-square-o'
                },
                {
                    'id': 'hand',
                    'name': '手势',
                    'icon': 'fa fa-fw fa-lg fa-hand-o-right'
                },
                {
                    'id': 'file',
                    'name': '文件',
                    'icon': 'fa fa-fw fa-lg fa-file'
                },
                // next in drop
                {
                    'id': 'chart',
                    'name': '图',
                    'icon': 'fa fa-fw fa-lg fa-bar-chart'
                },
                {
                    'id': 'access',
                    'name': '访问性',
                    'icon': 'fa fa-fw fa-lg fa-tty'
                },
                {
                    'id': 'spinner',
                    'name': '加载',
                    'icon': 'fa fa-fw fa-lg fa-spinner'
                },
                {
                    'id': 'gender',
                    'name': '性别',
                    'icon': 'fa fa-fw fa-lg fa-mars'
                },
                {
                    'id': 'video',
                    'name': '视频播放',
                    'icon': 'fa fa-fw fa-lg fa-play'
                },
                {
                    'id': 'trans',
                    'name': '交通',
                    'icon': 'fa fa-fw fa-lg fa-car'
                },
                {
                    'id': 'pay',
                    'name': '消费',
                    'icon': 'fa fa-fw fa-lg fa-cc-visa'
                },
                {
                    'id': 'curr',
                    'name': '货币',
                    'icon': 'fa fa-fw fa-lg fa-usd'
                },
                {
                    'id': 'brand',
                    'name': '品牌Logo',
                    'icon': 'fa fa-fw fa-lg fa-git'
                },
                {
                    'id': 'medi',
                    'name': '医学',
                    'icon': 'fa fa-fw fa-lg fa-heart'
                }
            ];
            var iocns = [
                [
                    // web应用
                    {'name': 'adjust', 'icon': 'fa fa-adjust'},
                    {
                        'name': 'american-sign-language-interpreting',
                        'icon': 'fa fa-american-sign-language-interpreting'
                    },
                    {'name': 'anchor', 'icon': 'fa fa-anchor'},
                    {'name': 'archive', 'icon': 'fa fa-archive'},
                    {'name': 'area-chart', 'icon': 'fa fa-area-chart'},
                    {'name': 'arrows', 'icon': 'fa fa-arrows'},
                    {'name': 'arrows-h', 'icon': 'fa fa-arrows-h'},
                    {'name': 'arrows-v', 'icon': 'fa fa-arrows-v'},
                    {'name': 'asl-interpreting', 'icon': 'fa fa-asl-interpreting'},
                    {'name': 'assistive-listening-systems', 'icon': 'fa fa-assistive-listening-systems'},
                    {'name': 'asterisk', 'icon': 'fa fa-asterisk'},
                    {'name': 'at', 'icon': 'fa fa-at'},
                    {'name': 'audio-description', 'icon': 'fa fa-audio-description'},
                    {'name': 'automobile', 'icon': 'fa fa-automobile'},
                    {'name': 'balance-scale', 'icon': 'fa fa-balance-scale'},
                    {'name': 'ban', 'icon': 'fa fa-ban'},
                    {'name': 'bank', 'icon': 'fa fa-bank'},
                    {'name': 'bar-chart', 'icon': 'fa fa-bar-chart'},
                    {'name': 'bar-chart-o', 'icon': 'fa fa-bar-chart-o'},
                    {'name': 'barcode', 'icon': 'fa fa-barcode'},
                    {'name': 'bars', 'icon': 'fa fa-bars'},
                    {'name': 'battery-0', 'icon': 'fa fa-battery-0'},
                    {'name': 'battery-1', 'icon': 'fa fa-battery-1'},
                    {'name': 'battery-2', 'icon': 'fa fa-battery-2'},
                    {'name': 'battery-3', 'icon': 'fa fa-battery-3'},
                    {'name': 'battery-4', 'icon': 'fa fa-battery-4'},
                    {'name': 'battery-empty', 'icon': 'fa fa-battery-empty'},
                    {'name': 'battery-full', 'icon': 'fa fa-battery-full'},
                    {'name': 'battery-half', 'icon': 'fa fa-battery-half'},
                    {'name': 'battery-quarter', 'icon': 'fa fa-battery-quarter'},
                    {'name': 'battery-three-quarters', 'icon': 'fa fa-battery-three-quarters'},
                    {'name': 'bed', 'icon': 'fa fa-bed'},
                    {'name': 'beer', 'icon': 'fa fa-beer'},
                    {'name': 'bell', 'icon': 'fa fa-bell'},
                    {'name': 'bell-o', 'icon': 'fa fa-bell-o'},
                    {'name': 'bell-slash', 'icon': 'fa fa-bell-slash'},
                    {'name': 'bell-slash-o', 'icon': 'fa fa-bell-slash-o'},
                    {'name': 'bicycle', 'icon': 'fa fa-bicycle'},
                    {'name': 'binoculars', 'icon': 'fa fa-binoculars'},
                    {'name': 'birthday-cake', 'icon': 'fa fa-birthday-cake'},
                    {'name': 'blind', 'icon': 'fa fa-blind'},
                    {'name': 'bluetooth', 'icon': 'fa fa-bluetooth'},
                    {'name': 'bluetooth-b', 'icon': 'fa fa-bluetooth-b'},
                    {'name': 'bolt', 'icon': 'fa fa-bolt'},
                    {'name': 'bomb', 'icon': 'fa fa-bomb'},
                    {'name': 'book', 'icon': 'fa fa-book'},
                    {'name': 'bookmark', 'icon': 'fa fa-bookmark'},
                    {'name': 'bookmark-o', 'icon': 'fa fa-bookmark-o'},
                    {'name': 'braille', 'icon': 'fa fa-braille'},
                    {'name': 'briefcase', 'icon': 'fa fa-briefcase'},
                    {'name': 'bug', 'icon': 'fa fa-bug'},
                    {'name': 'building', 'icon': 'fa fa-building'},
                    {'name': 'building-o', 'icon': 'fa fa-building-o'},
                    {'name': 'bullhorn', 'icon': 'fa fa-bullhorn'},
                    {'name': 'bullseye', 'icon': 'fa fa-bullseye'},
                    {'name': 'bus', 'icon': 'fa fa-bus'},
                    {'name': 'cab', 'icon': 'fa fa-cab'},
                    {'name': 'calculator', 'icon': 'fa fa-calculator'},
                    {'name': 'calendar', 'icon': 'fa fa-calendar'},
                    {'name': 'calendar-check-o', 'icon': 'fa fa-calendar-check-o'},
                    {'name': 'calendar-minus-o', 'icon': 'fa fa-calendar-minus-o'},
                    {'name': 'calendar-o', 'icon': 'fa fa-calendar-o'},
                    {'name': 'calendar-plus-o', 'icon': 'fa fa-calendar-plus-o'},
                    {'name': 'calendar-times-o', 'icon': 'fa fa-calendar-times-o'},
                    {'name': 'camera', 'icon': 'fa fa-camera'},
                    {'name': 'camera-retro', 'icon': 'fa fa-camera-retro'},
                    {'name': 'car', 'icon': 'fa fa-car'},
                    {'name': 'caret-square-o-down', 'icon': 'fa fa-caret-square-o-down'},
                    {'name': 'caret-square-o-left', 'icon': 'fa fa-caret-square-o-left'},
                    {'name': 'caret-square-o-right', 'icon': 'fa fa-caret-square-o-right'},
                    {'name': 'caret-square-o-up', 'icon': 'fa fa-caret-square-o-up'},
                    {'name': 'cart-arrow-down', 'icon': 'fa fa-cart-arrow-down'},
                    {'name': 'cart-plus', 'icon': 'fa fa-cart-plus'},
                    {'name': 'cc', 'icon': 'fa fa-cc'},
                    {'name': 'certificate', 'icon': 'fa fa-certificate'},
                    {'name': 'check', 'icon': 'fa fa-check'},
                    {'name': 'check-circle', 'icon': 'fa fa-check-circle'},
                    {'name': 'check-circle-o', 'icon': 'fa fa-check-circle-o'},
                    {'name': 'check-square', 'icon': 'fa fa-check-square'},
                    {'name': 'check-square-o', 'icon': 'fa fa-check-square-o'},
                    {'name': 'child', 'icon': 'fa fa-child'},
                    {'name': 'circle', 'icon': 'fa fa-circle'},
                    {'name': 'circle-o', 'icon': 'fa fa-circle-o'},
                    {'name': 'circle-o-notch', 'icon': 'fa fa-circle-o-notch'},
                    {'name': 'circle-thin', 'icon': 'fa fa-circle-thin'},
                    {'name': 'clock-o', 'icon': 'fa fa-clock-o'},
                    {'name': 'clone', 'icon': 'fa fa-clone'},
                    {'name': 'close', 'icon': 'fa fa-close'},
                    {'name': 'cloud', 'icon': 'fa fa-cloud'},
                    {'name': 'cloud-download', 'icon': 'fa fa-cloud-download'},
                    {'name': 'cloud-upload', 'icon': 'fa fa-cloud-upload'},
                    {'name': 'code', 'icon': 'fa fa-code'},
                    {'name': 'code-fork', 'icon': 'fa fa-code-fork'},
                    {'name': 'coffee', 'icon': 'fa fa-coffee'},
                    {'name': 'cog', 'icon': 'fa fa-cog'},
                    {'name': 'cogs', 'icon': 'fa fa-cogs'},
                    {'name': 'comment', 'icon': 'fa fa-comment'},
                    {'name': 'comment-o', 'icon': 'fa fa-comment-o'},
                    {'name': 'commenting', 'icon': 'fa fa-commenting'},
                    {'name': 'commenting-o', 'icon': 'fa fa-commenting-o'},
                    {'name': 'comments', 'icon': 'fa fa-comments'},
                    {'name': 'comments-o', 'icon': 'fa fa-comments-o'},
                    {'name': 'compass', 'icon': 'fa fa-compass'},
                    {'name': 'copyright', 'icon': 'fa fa-copyright'},
                    {'name': 'creative-commons', 'icon': 'fa fa-creative-commons'},
                    {'name': 'credit-card', 'icon': 'fa fa-credit-card'},
                    {'name': 'credit-card-alt', 'icon': 'fa fa-credit-card-alt'},
                    {'name': 'crop', 'icon': 'fa fa-crop'},
                    {'name': 'crosshairs', 'icon': 'fa fa-crosshairs'},
                    {'name': 'cube', 'icon': 'fa fa-cube'},
                    {'name': 'cubes', 'icon': 'fa fa-cubes'},
                    {'name': 'cutlery', 'icon': 'fa fa-cutlery'},
                    {'name': 'dashboard', 'icon': 'fa fa-dashboard'},
                    {'name': 'database', 'icon': 'fa fa-database'},
                    {'name': 'deaf', 'icon': 'fa fa-deaf'},
                    {'name': 'deafness', 'icon': 'fa fa-deafness'},
                    {'name': 'desktop', 'icon': 'fa fa-desktop'},
                    {'name': 'diamond', 'icon': 'fa fa-diamond'},
                    {'name': 'dot-circle-o', 'icon': 'fa fa-dot-circle-o'},
                    {'name': 'download', 'icon': 'fa fa-download'},
                    {'name': 'edit', 'icon': 'fa fa-edit'},
                    {'name': 'ellipsis-h', 'icon': 'fa fa-ellipsis-h'},
                    {'name': 'ellipsis-v', 'icon': 'fa fa-ellipsis-v'},
                    {'name': 'envelope', 'icon': 'fa fa-envelope'},
                    {'name': 'envelope-o', 'icon': 'fa fa-envelope-o'},
                    {'name': 'envelope-square', 'icon': 'fa fa-envelope-square'},
                    {'name': 'eraser', 'icon': 'fa fa-eraser'},
                    {'name': 'exchange', 'icon': 'fa fa-exchange'},
                    {'name': 'exclamation', 'icon': 'fa fa-exclamation'},
                    {'name': 'exclamation-circle', 'icon': 'fa fa-exclamation-circle'},
                    {'name': 'exclamation-triangle', 'icon': 'fa fa-exclamation-triangle'},
                    {'name': 'external-link', 'icon': 'fa fa-external-link'},
                    {'name': 'external-link-square', 'icon': 'fa fa-external-link-square'},
                    {'name': 'eye', 'icon': 'fa fa-eye'},
                    {'name': 'eye-slash', 'icon': 'fa fa-eye-slash'},
                    {'name': 'eyedropper', 'icon': 'fa fa-eyedropper'},
                    {'name': 'fax', 'icon': 'fa fa-fax'},
                    {'name': 'feed', 'icon': 'fa fa-feed'},
                    {'name': 'female', 'icon': 'fa fa-female'},
                    {'name': 'fighter-jet', 'icon': 'fa fa-fighter-jet'},
                    {'name': 'file-archive-o', 'icon': 'fa fa-file-archive-o'},
                    {'name': 'file-audio-o', 'icon': 'fa fa-file-audio-o'},
                    {'name': 'file-code-o', 'icon': 'fa fa-file-code-o'},
                    {'name': 'file-excel-o', 'icon': 'fa fa-file-excel-o'},
                    {'name': 'file-image-o', 'icon': 'fa fa-file-image-o'},
                    {'name': 'file-movie-o', 'icon': 'fa fa-file-movie-o'},
                    {'name': 'file-pdf-o', 'icon': 'fa fa-file-pdf-o'},
                    {'name': 'file-photo-o', 'icon': 'fa fa-file-photo-o'},
                    {'name': 'file-picture-o', 'icon': 'fa fa-file-picture-o'},
                    {'name': 'file-powerpoint-o', 'icon': 'fa fa-file-powerpoint-o'},
                    {'name': 'file-sound-o', 'icon': 'fa fa-file-sound-o'},
                    {'name': 'file-video-o', 'icon': 'fa fa-file-video-o'},
                    {'name': 'file-word-o', 'icon': 'fa fa-file-word-o'},
                    {'name': 'file-zip-o', 'icon': 'fa fa-file-zip-o'},
                    {'name': 'film', 'icon': 'fa fa-film'},
                    {'name': 'filter', 'icon': 'fa fa-filter'},
                    {'name': 'fire', 'icon': 'fa fa-fire'},
                    {'name': 'fire-extinguisher', 'icon': 'fa fa-fire-extinguisher'},
                    {'name': 'flag', 'icon': 'fa fa-flag'},
                    {'name': 'flag-checkered', 'icon': 'fa fa-flag-checkered'},
                    {'name': 'flag-o', 'icon': 'fa fa-flag-o'},
                    {'name': 'flash', 'icon': 'fa fa-flash'},
                    {'name': 'flask', 'icon': 'fa fa-flask'},
                    {'name': 'folder', 'icon': 'fa fa-folder'},
                    {'name': 'folder-o', 'icon': 'fa fa-folder-o'},
                    {'name': 'folder-open', 'icon': 'fa fa-folder-open'},
                    {'name': 'folder-open-o', 'icon': 'fa fa-folder-open-o'},
                    {'name': 'frown-o', 'icon': 'fa fa-frown-o'},
                    {'name': 'futbol-o', 'icon': 'fa fa-futbol-o'},
                    {'name': 'gamepad', 'icon': 'fa fa-gamepad'},
                    {'name': 'gavel', 'icon': 'fa fa-gavel'},
                    {'name': 'gear', 'icon': 'fa fa-gear'},
                    {'name': 'gears', 'icon': 'fa fa-gears'},
                    {'name': 'gift', 'icon': 'fa fa-gift'},
                    {'name': 'glass', 'icon': 'fa fa-glass'},
                    {'name': 'globe', 'icon': 'fa fa-globe'},
                    {'name': 'graduation-cap', 'icon': 'fa fa-graduation-cap'},
                    {'name': 'group', 'icon': 'fa fa-group'},
                    {'name': 'hand-grab-o', 'icon': 'fa fa-hand-grab-o'},
                    {'name': 'hand-lizard-o', 'icon': 'fa fa-hand-lizard-o'},
                    {'name': 'hand-paper-o', 'icon': 'fa fa-hand-paper-o'},
                    {'name': 'hand-peace-o', 'icon': 'fa fa-hand-peace-o'},
                    {'name': 'hand-pointer-o', 'icon': 'fa fa-hand-pointer-o'},
                    {'name': 'hand-rock-o', 'icon': 'fa fa-hand-rock-o'},
                    {'name': 'hand-scissors-o', 'icon': 'fa fa-hand-scissors-o'},
                    {'name': 'hand-spock-o', 'icon': 'fa fa-hand-spock-o'},
                    {'name': 'hand-stop-o', 'icon': 'fa fa-hand-stop-o'},
                    {'name': 'hard-of-hearing', 'icon': 'fa fa-hard-of-hearing'},
                    {'name': 'hashtag', 'icon': 'fa fa-hashtag'},
                    {'name': 'hdd-o', 'icon': 'fa fa-hdd-o'},
                    {'name': 'headphones', 'icon': 'fa fa-headphones'},
                    {'name': 'heart', 'icon': 'fa fa-heart'},
                    {'name': 'heart-o', 'icon': 'fa fa-heart-o'},
                    {'name': 'heartbeat', 'icon': 'fa fa-heartbeat'},
                    {'name': 'history', 'icon': 'fa fa-history'},
                    {'name': 'home', 'icon': 'fa fa-home'},
                    {'name': 'hotel', 'icon': 'fa fa-hotel'},
                    {'name': 'hourglass', 'icon': 'fa fa-hourglass'},
                    {'name': 'hourglass-1', 'icon': 'fa fa-hourglass-1'},
                    {'name': 'hourglass-2', 'icon': 'fa fa-hourglass-2'},
                    {'name': 'hourglass-3', 'icon': 'fa fa-hourglass-3'},
                    {'name': 'hourglass-end', 'icon': 'fa fa-hourglass-end'},
                    {'name': 'hourglass-half', 'icon': 'fa fa-hourglass-half'},
                    {'name': 'hourglass-o', 'icon': 'fa fa-hourglass-o'},
                    {'name': 'hourglass-start', 'icon': 'fa fa-hourglass-start'},
                    {'name': 'i-cursor', 'icon': 'fa fa-i-cursor'},
                    {'name': 'image', 'icon': 'fa fa-image'},
                    {'name': 'inbox', 'icon': 'fa fa-inbox'},
                    {'name': 'industry', 'icon': 'fa fa-industry'},
                    {'name': 'info', 'icon': 'fa fa-info'},
                    {'name': 'info-circle', 'icon': 'fa fa-info-circle'},
                    {'name': 'institution', 'icon': 'fa fa-institution'},
                    {'name': 'key', 'icon': 'fa fa-key'},
                    {'name': 'keyboard-o', 'icon': 'fa fa-keyboard-o'},
                    {'name': 'language', 'icon': 'fa fa-language'},
                    {'name': 'laptop', 'icon': 'fa fa-laptop'},
                    {'name': 'leaf', 'icon': 'fa fa-leaf'},
                    {'name': 'legal', 'icon': 'fa fa-legal'},
                    {'name': 'lemon-o', 'icon': 'fa fa-lemon-o'},
                    {'name': 'level-down', 'icon': 'fa fa-level-down'},
                    {'name': 'level-up', 'icon': 'fa fa-level-up'},
                    {'name': 'life-bouy', 'icon': 'fa fa-life-bouy'},
                    {'name': 'life-buoy', 'icon': 'fa fa-life-buoy'},
                    {'name': 'life-ring', 'icon': 'fa fa-life-ring'},
                    {'name': 'life-saver', 'icon': 'fa fa-life-saver'},
                    {'name': 'lightbulb-o', 'icon': 'fa fa-lightbulb-o'},
                    {'name': 'line-chart', 'icon': 'fa fa-line-chart'},
                    {'name': 'location-arrow', 'icon': 'fa fa-location-arrow'},
                    {'name': 'lock', 'icon': 'fa fa-lock'},
                    {'name': 'low-vision', 'icon': 'fa fa-low-vision'},
                    {'name': 'magic', 'icon': 'fa fa-magic'},
                    {'name': 'magnet', 'icon': 'fa fa-magnet'},
                    {'name': 'mail-forward', 'icon': 'fa fa-mail-forward'},
                    {'name': 'mail-reply', 'icon': 'fa fa-mail-reply'},
                    {'name': 'mail-reply-all', 'icon': 'fa fa-mail-reply-all'},
                    {'name': 'male', 'icon': 'fa fa-male'},
                    {'name': 'map', 'icon': 'fa fa-map'},
                    {'name': 'map-marker', 'icon': 'fa fa-map-marker'},
                    {'name': 'map-o', 'icon': 'fa fa-map-o'},
                    {'name': 'map-pin', 'icon': 'fa fa-map-pin'},
                    {'name': 'map-signs', 'icon': 'fa fa-map-signs'},
                    {'name': 'meh-o', 'icon': 'fa fa-meh-o'},
                    {'name': 'microphone', 'icon': 'fa fa-microphone'},
                    {'name': 'microphone-slash', 'icon': 'fa fa-microphone-slash'},
                    {'name': 'minus', 'icon': 'fa fa-minus'},
                    {'name': 'minus-circle', 'icon': 'fa fa-minus-circle'},
                    {'name': 'minus-square', 'icon': 'fa fa-minus-square'},
                    {'name': 'minus-square-o', 'icon': 'fa fa-minus-square-o'},
                    {'name': 'mobile', 'icon': 'fa fa-mobile'},
                    {'name': 'mobile-phone', 'icon': 'fa fa-mobile-phone'},
                    {'name': 'money', 'icon': 'fa fa-money'},
                    {'name': 'moon-o', 'icon': 'fa fa-moon-o'},
                    {'name': 'mortar-board', 'icon': 'fa fa-mortar-board'},
                    {'name': 'motorcycle', 'icon': 'fa fa-motorcycle'},
                    {'name': 'mouse-pointer', 'icon': 'fa fa-mouse-pointer'},
                    {'name': 'music', 'icon': 'fa fa-music'},
                    {'name': 'navicon', 'icon': 'fa fa-navicon'},
                    {'name': 'newspaper-o', 'icon': 'fa fa-newspaper-o'},
                    {'name': 'object-group', 'icon': 'fa fa-object-group'},
                    {'name': 'object-ungroup', 'icon': 'fa fa-object-ungroup'},
                    {'name': 'paint-brush', 'icon': 'fa fa-paint-brush'},
                    {'name': 'paper-plane', 'icon': 'fa fa-paper-plane'},
                    {'name': 'paper-plane-o', 'icon': 'fa fa-paper-plane-o'},
                    {'name': 'paw', 'icon': 'fa fa-paw'},
                    {'name': 'pencil', 'icon': 'fa fa-pencil'},
                    {'name': 'pencil-square', 'icon': 'fa fa-pencil-square'},
                    {'name': 'pencil-square-o', 'icon': 'fa fa-pencil-square-o'},
                    {'name': 'percent', 'icon': 'fa fa-percent'},
                    {'name': 'phone', 'icon': 'fa fa-phone'},
                    {'name': 'phone-square', 'icon': 'fa fa-phone-square'},
                    {'name': 'photo', 'icon': 'fa fa-photo'},
                    {'name': 'picture-o', 'icon': 'fa fa-picture-o'},
                    {'name': 'pie-chart', 'icon': 'fa fa-pie-chart'},
                    {'name': 'plane', 'icon': 'fa fa-plane'},
                    {'name': 'plug', 'icon': 'fa fa-plug'},
                    {'name': 'plus', 'icon': 'fa fa-plus'},
                    {'name': 'plus-circle', 'icon': 'fa fa-plus-circle'},
                    {'name': 'plus-square', 'icon': 'fa fa-plus-square'},
                    {'name': 'plus-square-o', 'icon': 'fa fa-plus-square-o'},
                    {'name': 'power-off', 'icon': 'fa fa-power-off'},
                    {'name': 'print', 'icon': 'fa fa-print'},
                    {'name': 'puzzle-piece', 'icon': 'fa fa-puzzle-piece'},
                    {'name': 'qrcode', 'icon': 'fa fa-qrcode'},
                    {'name': 'question', 'icon': 'fa fa-question'},
                    {'name': 'question-circle', 'icon': 'fa fa-question-circle'},
                    {'name': 'question-circle-o', 'icon': 'fa fa-question-circle-o'},
                    {'name': 'quote-left', 'icon': 'fa fa-quote-left'},
                    {'name': 'quote-right', 'icon': 'fa fa-quote-right'},
                    {'name': 'random', 'icon': 'fa fa-random'},
                    {'name': 'recycle', 'icon': 'fa fa-recycle'},
                    {'name': 'refresh', 'icon': 'fa fa-refresh'},
                    {'name': 'registered', 'icon': 'fa fa-registered'},
                    {'name': 'remove', 'icon': 'fa fa-remove'},
                    {'name': 'reorder', 'icon': 'fa fa-reorder'},
                    {'name': 'reply', 'icon': 'fa fa-reply'},
                    {'name': 'reply-all', 'icon': 'fa fa-reply-all'},
                    {'name': 'retweet', 'icon': 'fa fa-retweet'},
                    {'name': 'road', 'icon': 'fa fa-road'},
                    {'name': 'rocket', 'icon': 'fa fa-rocket'},
                    {'name': 'rss', 'icon': 'fa fa-rss'},
                    {'name': 'rss-square', 'icon': 'fa fa-rss-square'},
                    {'name': 'search', 'icon': 'fa fa-search'},
                    {'name': 'search-minus', 'icon': 'fa fa-search-minus'},
                    {'name': 'search-plus', 'icon': 'fa fa-search-plus'},
                    {'name': 'send', 'icon': 'fa fa-send'},
                    {'name': 'send-o', 'icon': 'fa fa-send-o'},
                    {'name': 'server', 'icon': 'fa fa-server'},
                    {'name': 'share', 'icon': 'fa fa-share'},
                    {'name': 'share-alt', 'icon': 'fa fa-share-alt'},
                    {'name': 'share-alt-square', 'icon': 'fa fa-share-alt-square'},
                    {'name': 'share-square', 'icon': 'fa fa-share-square'},
                    {'name': 'share-square-o', 'icon': 'fa fa-share-square-o'},
                    {'name': 'shield', 'icon': 'fa fa-shield'},
                    {'name': 'ship', 'icon': 'fa fa-ship'},
                    {'name': 'shopping-bag', 'icon': 'fa fa-shopping-bag'},
                    {'name': 'shopping-basket', 'icon': 'fa fa-shopping-basket'},
                    {'name': 'shopping-cart', 'icon': 'fa fa-shopping-cart'},
                    {'name': 'sign-in', 'icon': 'fa fa-sign-in'},
                    {'name': 'sign-language', 'icon': 'fa fa-sign-language'},
                    {'name': 'sign-out', 'icon': 'fa fa-sign-out'},
                    {'name': 'signal', 'icon': 'fa fa-signal'},
                    {'name': 'signing', 'icon': 'fa fa-signing'},
                    {'name': 'sitemap', 'icon': 'fa fa-sitemap'},
                    {'name': 'sliders', 'icon': 'fa fa-sliders'},
                    {'name': 'smile-o', 'icon': 'fa fa-smile-o'},
                    {'name': 'soccer-ball-o', 'icon': 'fa fa-soccer-ball-o'},
                    {'name': 'sort', 'icon': 'fa fa-sort'},
                    {'name': 'sort-alpha-asc', 'icon': 'fa fa-sort-alpha-asc'},
                    {'name': 'sort-alpha-desc', 'icon': 'fa fa-sort-alpha-desc'},
                    {'name': 'sort-amount-asc', 'icon': 'fa fa-sort-amount-asc'},
                    {'name': 'sort-amount-desc', 'icon': 'fa fa-sort-amount-desc'},
                    {'name': 'sort-asc', 'icon': 'fa fa-sort-asc'},
                    {'name': 'sort-desc', 'icon': 'fa fa-sort-desc'},
                    {'name': 'sort-down', 'icon': 'fa fa-sort-down'},
                    {'name': 'sort-numeric-asc', 'icon': 'fa fa-sort-numeric-asc'},
                    {'name': 'sort-numeric-desc', 'icon': 'fa fa-sort-numeric-desc'},
                    {'name': 'sort-up', 'icon': 'fa fa-sort-up'},
                    {'name': 'space-shuttle', 'icon': 'fa fa-space-shuttle'},
                    {'name': 'spinner', 'icon': 'fa fa-spinner'},
                    {'name': 'spoon', 'icon': 'fa fa-spoon'},
                    {'name': 'square', 'icon': 'fa fa-square'},
                    {'name': 'square-o', 'icon': 'fa fa-square-o'},
                    {'name': 'star', 'icon': 'fa fa-star'},
                    {'name': 'star-half', 'icon': 'fa fa-star-half'},
                    {'name': 'star-half-empty', 'icon': 'fa fa-star-half-empty'},
                    {'name': 'star-half-full', 'icon': 'fa fa-star-half-full'},
                    {'name': 'star-half-o', 'icon': 'fa fa-star-half-o'},
                    {'name': 'star-o', 'icon': 'fa fa-star-o'},
                    {'name': 'sticky-note', 'icon': 'fa fa-sticky-note'},
                    {'name': 'sticky-note-o', 'icon': 'fa fa-sticky-note-o'},
                    {'name': 'street-view', 'icon': 'fa fa-street-view'},
                    {'name': 'suitcase', 'icon': 'fa fa-suitcase'},
                    {'name': 'sun-o', 'icon': 'fa fa-sun-o'},
                    {'name': 'support', 'icon': 'fa fa-support'},
                    {'name': 'tablet', 'icon': 'fa fa-tablet'},
                    {'name': 'tachometer', 'icon': 'fa fa-tachometer'},
                    {'name': 'tag', 'icon': 'fa fa-tag'},
                    {'name': 'tags', 'icon': 'fa fa-tags'},
                    {'name': 'tasks', 'icon': 'fa fa-tasks'},
                    {'name': 'taxi', 'icon': 'fa fa-taxi'},
                    {'name': 'television', 'icon': 'fa fa-television'},
                    {'name': 'terminal', 'icon': 'fa fa-terminal'},
                    {'name': 'thumb-tack', 'icon': 'fa fa-thumb-tack'},
                    {'name': 'thumbs-down', 'icon': 'fa fa-thumbs-down'},
                    {'name': 'thumbs-o-down', 'icon': 'fa fa-thumbs-o-down'},
                    {'name': 'thumbs-o-up', 'icon': 'fa fa-thumbs-o-up'},
                    {'name': 'thumbs-up', 'icon': 'fa fa-thumbs-up'},
                    {'name': 'ticket', 'icon': 'fa fa-ticket'},
                    {'name': 'times', 'icon': 'fa fa-times'},
                    {'name': 'times-circle', 'icon': 'fa fa-times-circle'},
                    {'name': 'times-circle-o', 'icon': 'fa fa-times-circle-o'},
                    {'name': 'tint', 'icon': 'fa fa-tint'},
                    {'name': 'toggle-down', 'icon': 'fa fa-toggle-down'},
                    {'name': 'toggle-left', 'icon': 'fa fa-toggle-left'},
                    {'name': 'toggle-off', 'icon': 'fa fa-toggle-off'},
                    {'name': 'toggle-on', 'icon': 'fa fa-toggle-on'},
                    {'name': 'toggle-right', 'icon': 'fa fa-toggle-right'},
                    {'name': 'toggle-up', 'icon': 'fa fa-toggle-up'},
                    {'name': 'trademark', 'icon': 'fa fa-trademark'},
                    {'name': 'trash', 'icon': 'fa fa-trash'},
                    {'name': 'trash-o', 'icon': 'fa fa-trash-o'},
                    {'name': 'tree', 'icon': 'fa fa-tree'},
                    {'name': 'trophy', 'icon': 'fa fa-trophy'},
                    {'name': 'truck', 'icon': 'fa fa-truck'},
                    {'name': 'tty', 'icon': 'fa fa-tty'},
                    {'name': 'tv', 'icon': 'fa fa-tv'},
                    {'name': 'umbrella', 'icon': 'fa fa-umbrella'},
                    {'name': 'universal-access', 'icon': 'fa fa-universal-access'},
                    {'name': 'university', 'icon': 'fa fa-university'},
                    {'name': 'unlock', 'icon': 'fa fa-unlock'},
                    {'name': 'unlock-alt', 'icon': 'fa fa-unlock-alt'},
                    {'name': 'unsorted', 'icon': 'fa fa-unsorted'},
                    {'name': 'upload', 'icon': 'fa fa-upload'},
                    {'name': 'user', 'icon': 'fa fa-user'},
                    {'name': 'user-plus', 'icon': 'fa fa-user-plus'},
                    {'name': 'user-secret', 'icon': 'fa fa-user-secret'},
                    {'name': 'user-times', 'icon': 'fa fa-user-times'},
                    {'name': 'users', 'icon': 'fa fa-users'},
                    {'name': 'video-camera', 'icon': 'fa fa-video-camera'},
                    {'name': 'volume-control-phone', 'icon': 'fa fa-volume-control-phone'},
                    {'name': 'volume-down', 'icon': 'fa fa-volume-down'},
                    {'name': 'volume-off', 'icon': 'fa fa-volume-off'},
                    {'name': 'volume-up', 'icon': 'fa fa-volume-up'},
                    {'name': 'warning', 'icon': 'fa fa-warning'},
                    {'name': 'wheelchair', 'icon': 'fa fa-wheelchair'},
                    {'name': 'wheelchair-alt', 'icon': 'fa fa-wheelchair-alt'},
                    {'name': 'wifi', 'icon': 'fa fa-wifi'},
                    {'name': 'wrench', 'icon': 'fa fa-wrench'}
                ],
                [
                    // 文本编辑
                    {'name': 'align-center', 'icon': 'fa fa-align-center'},
                    {'name': 'align-justify', 'icon': 'fa fa-align-justify'},
                    {'name': 'align-left', 'icon': 'fa fa-align-left'},
                    {'name': 'align-right', 'icon': 'fa fa-align-right'},
                    {'name': 'bold', 'icon': 'fa fa-bold'},
                    {'name': 'chain', 'icon': 'fa fa-chain'},
                    {'name': 'chain-broken', 'icon': 'fa fa-chain-broken'},
                    {'name': 'clipboard', 'icon': 'fa fa-clipboard'},
                    {'name': 'columns', 'icon': 'fa fa-columns'},
                    {'name': 'copy', 'icon': 'fa fa-copy'},
                    {'name': 'cut', 'icon': 'fa fa-cut'},
                    {'name': 'dedent', 'icon': 'fa fa-dedent'},
                    {'name': 'eraser', 'icon': 'fa fa-eraser'},
                    {'name': 'file', 'icon': 'fa fa-file'},
                    {'name': 'file-o', 'icon': 'fa fa-file-o'},
                    {'name': 'file-text', 'icon': 'fa fa-file-text'},
                    {'name': 'file-text-o', 'icon': 'fa fa-file-text-o'},
                    {'name': 'files-o', 'icon': 'fa fa-files-o'},
                    {'name': 'floppy-o', 'icon': 'fa fa-floppy-o'},
                    {'name': 'font', 'icon': 'fa fa-font'},
                    {'name': 'header', 'icon': 'fa fa-header'},
                    {'name': 'indent', 'icon': 'fa fa-indent'},
                    {'name': 'italic', 'icon': 'fa fa-italic'},
                    {'name': 'link', 'icon': 'fa fa-link'},
                    {'name': 'list', 'icon': 'fa fa-list'},
                    {'name': 'list-alt', 'icon': 'fa fa-list-alt'},
                    {'name': 'list-ol', 'icon': 'fa fa-list-ol'},
                    {'name': 'list-ul', 'icon': 'fa fa-list-ul'},
                    {'name': 'outdent', 'icon': 'fa fa-outdent'},
                    {'name': 'paperclip', 'icon': 'fa fa-paperclip'},
                    {'name': 'paragraph', 'icon': 'fa fa-paragraph'},
                    {'name': 'paste', 'icon': 'fa fa-paste'},
                    {'name': 'repeat', 'icon': 'fa fa-repeat'},
                    {'name': 'rotate-left', 'icon': 'fa fa-rotate-left'},
                    {'name': 'rotate-right', 'icon': 'fa fa-rotate-right'},
                    {'name': 'save', 'icon': 'fa fa-save'},
                    {'name': 'scissors', 'icon': 'fa fa-scissors'},
                    {'name': 'strikethrough', 'icon': 'fa fa-strikethrough'},
                    {'name': 'subscript', 'icon': 'fa fa-subscript'},
                    {'name': 'superscript', 'icon': 'fa fa-superscript'},
                    {'name': 'table', 'icon': 'fa fa-table'},
                    {'name': 'text-height', 'icon': 'fa fa-text-height'},
                    {'name': 'text-width', 'icon': 'fa fa-text-width'},
                    {'name': 'th', 'icon': 'fa fa-th'},
                    {'name': 'th-large', 'icon': 'fa fa-th-large'},
                    {'name': 'th-list', 'icon': 'fa fa-th-list'},
                    {'name': 'underline', 'icon': 'fa fa-underline'},
                    {'name': 'undo', 'icon': 'fa fa-undo'},
                    {'name': 'unlink', 'icon': 'fa fa-unlink'}
                ],
                [
                    // 方向
                    {'name': 'angle-double-down', 'icon': 'fa fa-angle-double-down'},
                    {'name': 'angle-double-left', 'icon': 'fa fa-angle-double-left'},
                    {'name': 'angle-double-right', 'icon': 'fa fa-angle-double-right'},
                    {'name': 'angle-double-up', 'icon': 'fa fa-angle-double-up'},
                    {'name': 'angle-down', 'icon': 'fa fa-angle-down'},
                    {'name': 'angle-left', 'icon': 'fa fa-angle-left'},
                    {'name': 'angle-right', 'icon': 'fa fa-angle-right'},
                    {'name': 'angle-up', 'icon': 'fa fa-angle-up'},
                    {'name': 'arrow-circle-down', 'icon': 'fa fa-arrow-circle-down'},
                    {'name': 'arrow-circle-left', 'icon': 'fa fa-arrow-circle-left'},
                    {'name': 'arrow-circle-o-down', 'icon': 'fa fa-arrow-circle-o-down'},
                    {'name': 'arrow-circle-o-left', 'icon': 'fa fa-arrow-circle-o-left'},
                    {'name': 'arrow-circle-o-right', 'icon': 'fa fa-arrow-circle-o-right'},
                    {'name': 'arrow-circle-o-up', 'icon': 'fa fa-arrow-circle-o-up'},
                    {'name': 'arrow-circle-right', 'icon': 'fa fa-arrow-circle-right'},
                    {'name': 'arrow-circle-up', 'icon': 'fa fa-arrow-circle-up'},
                    {'name': 'arrow-down', 'icon': 'fa fa-arrow-down'},
                    {'name': 'arrow-left', 'icon': 'fa fa-arrow-left'},
                    {'name': 'arrow-right', 'icon': 'fa fa-arrow-right'},
                    {'name': 'arrow-up', 'icon': 'fa fa-arrow-up'},
                    {'name': 'arrows', 'icon': 'fa fa-arrows'},
                    {'name': 'arrows-alt', 'icon': 'fa fa-arrows-alt'},
                    {'name': 'arrows-h', 'icon': 'fa fa-arrows-h'},
                    {'name': 'arrows-v', 'icon': 'fa fa-arrows-v'},
                    {'name': 'caret-down', 'icon': 'fa fa-caret-down'},
                    {'name': 'caret-left', 'icon': 'fa fa-caret-left'},
                    {'name': 'caret-right', 'icon': 'fa fa-caret-right'},
                    {'name': 'caret-square-o-down', 'icon': 'fa fa-caret-square-o-down'},
                    {'name': 'caret-square-o-left', 'icon': 'fa fa-caret-square-o-left'},
                    {'name': 'caret-square-o-right', 'icon': 'fa fa-caret-square-o-right'},
                    {'name': 'caret-square-o-up', 'icon': 'fa fa-caret-square-o-up'},
                    {'name': 'caret-up', 'icon': 'fa fa-caret-up'},
                    {'name': 'chevron-circle-down', 'icon': 'fa fa-chevron-circle-down'},
                    {'name': 'chevron-circle-left', 'icon': 'fa fa-chevron-circle-left'},
                    {'name': 'chevron-circle-right', 'icon': 'fa fa-chevron-circle-right'},
                    {'name': 'chevron-circle-up', 'icon': 'fa fa-chevron-circle-up'},
                    {'name': 'chevron-down', 'icon': 'fa fa-chevron-down'},
                    {'name': 'chevron-left', 'icon': 'fa fa-chevron-left'},
                    {'name': 'chevron-right', 'icon': 'fa fa-chevron-right'},
                    {'name': 'chevron-up', 'icon': 'fa fa-chevron-up'},
                    {'name': 'exchange', 'icon': 'fa fa-exchange'},
                    {'name': 'hand-o-down', 'icon': 'fa fa-hand-o-down'},
                    {'name': 'hand-o-left', 'icon': 'fa fa-hand-o-left'},
                    {'name': 'hand-o-right', 'icon': 'fa fa-hand-o-right'},
                    {'name': 'hand-o-up', 'icon': 'fa fa-hand-o-up'},
                    {'name': 'long-arrow-down', 'icon': 'fa fa-long-arrow-down'},
                    {'name': 'long-arrow-left', 'icon': 'fa fa-long-arrow-left'},
                    {'name': 'long-arrow-right', 'icon': 'fa fa-long-arrow-right'},
                    {'name': 'long-arrow-up', 'icon': 'fa fa-long-arrow-up'},
                    {'name': 'toggle-down', 'icon': 'fa fa-toggle-down'},
                    {'name': 'toggle-left', 'icon': 'fa fa-toggle-left'},
                    {'name': 'toggle-right', 'icon': 'fa fa-toggle-right'},
                    {'name': 'toggle-up', 'icon': 'fa fa-toggle-up'}
                ],
                [
                    // 表单
                    {'name': 'check-square', 'icon': 'fa fa-check-square'},
                    {'name': 'check-square-o', 'icon': 'fa fa-check-square-o'},
                    {'name': 'circle', 'icon': 'fa fa-circle'},
                    {'name': 'circle-o', 'icon': 'fa fa-circle-o'},
                    {'name': 'dot-circle-o', 'icon': 'fa fa-dot-circle-o'},
                    {'name': 'minus-square', 'icon': 'fa fa-minus-square'},
                    {'name': 'minus-square-o', 'icon': 'fa fa-minus-square-o'},
                    {'name': 'plus-square', 'icon': 'fa fa-plus-square'},
                    {'name': 'plus-square-o', 'icon': 'fa fa-plus-square-o'},
                    {'name': 'square', 'icon': 'fa fa-square'},
                    {'name': 'square-o', 'icon': 'fa fa-square-o'}
                ],
                [
                    // 手势
                    {'name': 'hand-grab-o', 'icon': 'fa fa-hand-grab-o'},
                    {'name': 'hand-lizard-o', 'icon': 'fa fa-hand-lizard-o'},
                    {'name': 'hand-o-down', 'icon': 'fa fa-hand-o-down'},
                    {'name': 'hand-o-left', 'icon': 'fa fa-hand-o-left'},
                    {'name': 'hand-o-right', 'icon': 'fa fa-hand-o-right'},
                    {'name': 'hand-o-up', 'icon': 'fa fa-hand-o-up'},
                    {'name': 'hand-paper-o', 'icon': 'fa fa-hand-paper-o'},
                    {'name': 'hand-peace-o', 'icon': 'fa fa-hand-peace-o'},
                    {'name': 'hand-pointer-o', 'icon': 'fa fa-hand-pointer-o'},
                    {'name': 'hand-rock-o', 'icon': 'fa fa-hand-rock-o'},
                    {'name': 'hand-scissors-o', 'icon': 'fa fa-hand-scissors-o'},
                    {'name': 'hand-spock-o', 'icon': 'fa fa-hand-spock-o'},
                    {'name': 'hand-stop-o', 'icon': 'fa fa-hand-stop-o'},
                    {'name': 'thumbs-down', 'icon': 'fa fa-thumbs-down'},
                    {'name': 'thumbs-o-down', 'icon': 'fa fa-thumbs-o-down'},
                    {'name': 'thumbs-o-up', 'icon': 'fa fa-thumbs-o-up'},
                    {'name': 'thumbs-up', 'icon': 'fa fa-thumbs-up'}
                ],
                [
                    // 文件
                    {'name': 'file', 'icon': 'fa fa-file'},
                    {'name': 'file-archive-o', 'icon': 'fa fa-file-archive-o'},
                    {'name': 'file-audio-o', 'icon': 'fa fa-file-audio-o'},
                    {'name': 'file-code-o', 'icon': 'fa fa-file-code-o'},
                    {'name': 'file-excel-o', 'icon': 'fa fa-file-excel-o'},
                    {'name': 'file-image-o', 'icon': 'fa fa-file-image-o'},
                    {'name': 'file-movie-o', 'icon': 'fa fa-file-movie-o'},
                    {'name': 'file-o', 'icon': 'fa fa-file-o'},
                    {'name': 'file-pdf-o', 'icon': 'fa fa-file-pdf-o'},
                    {'name': 'file-photo-o', 'icon': 'fa fa-file-photo-o'},
                    {'name': 'file-picture-o', 'icon': 'fa fa-file-picture-o'},
                    {'name': 'file-powerpoint-o', 'icon': 'fa fa-file-powerpoint-o'},
                    {'name': 'file-sound-o', 'icon': 'fa fa-file-sound-o'},
                    {'name': 'file-text', 'icon': 'fa fa-file-text'},
                    {'name': 'file-text-o', 'icon': 'fa fa-file-text-o'},
                    {'name': 'file-video-o', 'icon': 'fa fa-file-video-o'},
                    {'name': 'file-word-o', 'icon': 'fa fa-file-word-o'},
                    {'name': 'file-zip-o', 'icon': 'fa fa-file-zip-o'}
                ],
                // next in drop
                [
                    // 图
                    {'name': 'area-chart', 'icon': 'fa fa-area-chart'},
                    {'name': 'bar-chart', 'icon': 'fa fa-bar-chart'},
                    {'name': 'bar-chart-o', 'icon': 'fa fa-bar-chart-o'},
                    {'name': 'line-chart', 'icon': 'fa fa-line-chart'},
                    {'name': 'pie-chart', 'icon': 'fa fa-pie-chart'}
                ],
                [
                    // 访问性
                    {
                        'name': 'american-sign-language-interpreting',
                        'icon': 'fa fa-american-sign-language-interpreting'
                    },
                    {'name': 'asl-interpreting', 'icon': 'fa fa-asl-interpreting'},
                    {'name': 'assistive-listening-systems', 'icon': 'fa fa-assistive-listening-systems'},
                    {'name': 'audio-description', 'icon': 'fa fa-audio-description'},
                    {'name': 'blind', 'icon': 'fa fa-blind'},
                    {'name': 'braille', 'icon': 'fa fa-braille'},
                    {'name': 'cc', 'icon': 'fa fa-cc'},
                    {'name': 'deaf', 'icon': 'fa fa-deaf'},
                    {'name': 'deafness', 'icon': 'fa fa-deafness'},
                    {'name': 'hard-of-hearing', 'icon': 'fa fa-hard-of-hearing'},
                    {'name': 'low-vision', 'icon': 'fa fa-low-vision'},
                    {'name': 'question-circle-o', 'icon': 'fa fa-question-circle-o'},
                    {'name': 'sign-language', 'icon': 'fa fa-sign-language'},
                    {'name': 'signing', 'icon': 'fa fa-signing'},
                    {'name': 'tty', 'icon': 'fa fa-tty'},
                    {'name': 'universal-access', 'icon': 'fa fa-universal-access'},
                    {'name': 'volume-control-phone', 'icon': 'fa fa-volume-control-phone'},
                    {'name': 'wheelchair', 'icon': 'fa fa-wheelchair'},
                    {'name': 'wheelchair-alt', 'icon': 'fa fa-wheelchair-alt'}
                ],
                [
                    // 加载
                    {'name': 'circle-o-notch', 'icon': 'fa fa-circle-o-notch'},
                    {'name': 'cog', 'icon': 'fa fa-cog'},
                    {'name': 'gear', 'icon': 'fa fa-gear'},
                    {'name': 'refresh', 'icon': 'fa fa-refresh'},
                    {'name': 'spinner', 'icon': 'fa fa-spinner'}
                ],
                [
                    // 性别
                    {'name': 'genderless', 'icon': 'fa fa-genderless'},
                    {'name': 'intersex', 'icon': 'fa fa-intersex'},
                    {'name': 'mars', 'icon': 'fa fa-mars'},
                    {'name': 'mars-double', 'icon': 'fa fa-mars-double'},
                    {'name': 'mars-stroke', 'icon': 'fa fa-mars-stroke'},
                    {'name': 'mars-stroke-h', 'icon': 'fa fa-mars-stroke-h'},
                    {'name': 'mars-stroke-v', 'icon': 'fa fa-mars-stroke-v'},
                    {'name': 'mercury', 'icon': 'fa fa-mercury'},
                    {'name': 'neuter', 'icon': 'fa fa-neuter'},
                    {'name': 'transgender', 'icon': 'fa fa-transgender'},
                    {'name': 'transgender-alt', 'icon': 'fa fa-transgender-alt'},
                    {'name': 'venus', 'icon': 'fa fa-venus'},
                    {'name': 'venus-double', 'icon': 'fa fa-venus-double'},
                    {'name': 'venus-mars', 'icon': 'fa fa-venus-mars'}
                ],
                [
                    // 视频播放
                    {'name': 'arrows-alt', 'icon': 'fa fa-arrows-alt'},
                    {'name': 'backward', 'icon': 'fa fa-backward'},
                    {'name': 'compress', 'icon': 'fa fa-compress'},
                    {'name': 'eject', 'icon': 'fa fa-eject'},
                    {'name': 'expand', 'icon': 'fa fa-expand'},
                    {'name': 'fast-backward', 'icon': 'fa fa-fast-backward'},
                    {'name': 'fast-forward', 'icon': 'fa fa-fast-forward'},
                    {'name': 'forward', 'icon': 'fa fa-forward'},
                    {'name': 'pause', 'icon': 'fa fa-pause'},
                    {'name': 'pause-circle', 'icon': 'fa fa-pause-circle'},
                    {'name': 'pause-circle-o', 'icon': 'fa fa-pause-circle-o'},
                    {'name': 'play', 'icon': 'fa fa-play'},
                    {'name': 'play-circle', 'icon': 'fa fa-play-circle'},
                    {'name': 'play-circle-o', 'icon': 'fa fa-play-circle-o'},
                    {'name': 'random', 'icon': 'fa fa-random'},
                    {'name': 'step-backward', 'icon': 'fa fa-step-backward'},
                    {'name': 'step-forward', 'icon': 'fa fa-step-forward'},
                    {'name': 'stop', 'icon': 'fa fa-stop'},
                    {'name': 'stop-circle', 'icon': 'fa fa-stop-circle'},
                    {'name': 'stop-circle-o', 'icon': 'fa fa-stop-circle-o'},
                    {'name': 'youtube-play', 'icon': 'fa fa-youtube-play'}
                ],
                [
                    // 交通
                    {'name': 'ambulance', 'icon': 'fa fa-ambulance'},
                    {'name': 'automobile', 'icon': 'fa fa-automobile'},
                    {'name': 'bicycle', 'icon': 'fa fa-bicycle'},
                    {'name': 'bus', 'icon': 'fa fa-bus'},
                    {'name': 'cab', 'icon': 'fa fa-cab'},
                    {'name': 'car', 'icon': 'fa fa-car'},
                    {'name': 'fighter-jet', 'icon': 'fa fa-fighter-jet'},
                    {'name': 'motorcycle', 'icon': 'fa fa-motorcycle'},
                    {'name': 'plane', 'icon': 'fa fa-plane'},
                    {'name': 'rocket', 'icon': 'fa fa-rocket'},
                    {'name': 'ship', 'icon': 'fa fa-ship'},
                    {'name': 'space-shuttle', 'icon': 'fa fa-space-shuttle'},
                    {'name': 'subway', 'icon': 'fa fa-subway'},
                    {'name': 'taxi', 'icon': 'fa fa-taxi'},
                    {'name': 'train', 'icon': 'fa fa-train'},
                    {'name': 'truck', 'icon': 'fa fa-truck'},
                    {'name': 'wheelchair', 'icon': 'fa fa-wheelchair'},
                    {'name': 'wheelchair-alt', 'icon': 'fa fa-wheelchair-alt'}
                ],
                [
                    // 消费
                    {'name': 'cc-amex', 'icon': 'fa fa-cc-amex'},
                    {'name': 'cc-diners-club', 'icon': 'fa fa-cc-diners-club'},
                    {'name': 'cc-discover', 'icon': 'fa fa-cc-discover'},
                    {'name': 'cc-jcb', 'icon': 'fa fa-cc-jcb'},
                    {'name': 'cc-mastercard', 'icon': 'fa fa-cc-mastercard'},
                    {'name': 'cc-paypal', 'icon': 'fa fa-cc-paypal'},
                    {'name': 'cc-stripe', 'icon': 'fa fa-cc-stripe'},
                    {'name': 'cc-visa', 'icon': 'fa fa-cc-visa'},
                    {'name': 'credit-card', 'icon': 'fa fa-credit-card'},
                    {'name': 'credit-card-alt', 'icon': 'fa fa-credit-card-alt'},
                    {'name': 'google-wallet', 'icon': 'fa fa-google-wallet'},
                    {'name': 'paypal', 'icon': 'fa fa-paypal'}
                ],
                [
                    // 货币
                    {'name': 'bitcoin', 'icon': 'fa fa-bitcoin'},
                    {'name': 'btc', 'icon': 'fa fa-btc'},
                    {'name': 'cny', 'icon': 'fa fa-cny'},
                    {'name': 'dollar', 'icon': 'fa fa-dollar'},
                    {'name': 'eur', 'icon': 'fa fa-eur'},
                    {'name': 'euro', 'icon': 'fa fa-euro'},
                    {'name': 'gbp', 'icon': 'fa fa-gbp'},
                    {'name': 'gg', 'icon': 'fa fa-gg'},
                    {'name': 'gg-circle', 'icon': 'fa fa-gg-circle'},
                    {'name': 'ils', 'icon': 'fa fa-ils'},
                    {'name': 'inr', 'icon': 'fa fa-inr'},
                    {'name': 'jpy', 'icon': 'fa fa-jpy'},
                    {'name': 'krw', 'icon': 'fa fa-krw'},
                    {'name': 'money', 'icon': 'fa fa-money'},
                    {'name': 'rmb', 'icon': 'fa fa-rmb'},
                    {'name': 'rouble', 'icon': 'fa fa-rouble'},
                    {'name': 'rub', 'icon': 'fa fa-rub'},
                    {'name': 'ruble', 'icon': 'fa fa-ruble'},
                    {'name': 'rupee', 'icon': 'fa fa-rupee'},
                    {'name': 'shekel', 'icon': 'fa fa-shekel'},
                    {'name': 'sheqel', 'icon': 'fa fa-sheqel'},
                    {'name': 'try', 'icon': 'fa fa-try'},
                    {'name': 'turkish-lira', 'icon': 'fa fa-turkish-lira'},
                    {'name': 'usd', 'icon': 'fa fa-usd'},
                    {'name': 'won', 'icon': 'fa fa-won'},
                    {'name': 'yen', 'icon': 'fa fa-yen'}
                ],
                [
                    // 品牌
                    {'name': '500px', 'icon': 'fa fa-500px'},
                    {'name': 'adn', 'icon': 'fa fa-adn'},
                    {'name': 'amazon', 'icon': 'fa fa-amazon'},
                    {'name': 'android', 'icon': 'fa fa-android'},
                    {'name': 'angellist', 'icon': 'fa fa-angellist'},
                    {'name': 'apple', 'icon': 'fa fa-apple'},
                    {'name': 'behance', 'icon': 'fa fa-behance'},
                    {'name': 'behance-square', 'icon': 'fa fa-behance-square'},
                    {'name': 'bitbucket', 'icon': 'fa fa-bitbucket'},
                    {'name': 'bitbucket-square', 'icon': 'fa fa-bitbucket-square'},
                    {'name': 'bitcoin', 'icon': 'fa fa-bitcoin'},
                    {'name': 'black-tie', 'icon': 'fa fa-black-tie'},
                    {'name': 'bluetooth', 'icon': 'fa fa-bluetooth'},
                    {'name': 'bluetooth-b', 'icon': 'fa fa-bluetooth-b'},
                    {'name': 'btc', 'icon': 'fa fa-btc'},
                    {'name': 'buysellads', 'icon': 'fa fa-buysellads'},
                    {'name': 'cc-amex', 'icon': 'fa fa-cc-amex'},
                    {'name': 'cc-diners-club', 'icon': 'fa fa-cc-diners-club'},
                    {'name': 'cc-discover', 'icon': 'fa fa-cc-discover'},
                    {'name': 'cc-jcb', 'icon': 'fa fa-cc-jcb'},
                    {'name': 'cc-mastercard', 'icon': 'fa fa-cc-mastercard'},
                    {'name': 'cc-paypal', 'icon': 'fa fa-cc-paypal'},
                    {'name': 'cc-stripe', 'icon': 'fa fa-cc-stripe'},
                    {'name': 'cc-visa', 'icon': 'fa fa-cc-visa'},
                    {'name': 'chrome', 'icon': 'fa fa-chrome'},
                    {'name': 'codepen', 'icon': 'fa fa-codepen'},
                    {'name': 'codiepie', 'icon': 'fa fa-codiepie'},
                    {'name': 'connectdevelop', 'icon': 'fa fa-connectdevelop'},
                    {'name': 'contao', 'icon': 'fa fa-contao'},
                    {'name': 'css3', 'icon': 'fa fa-css3'},
                    {'name': 'dashcube', 'icon': 'fa fa-dashcube'},
                    {'name': 'delicious', 'icon': 'fa fa-delicious'},
                    {'name': 'deviantart', 'icon': 'fa fa-deviantart'},
                    {'name': 'digg', 'icon': 'fa fa-digg'},
                    {'name': 'dribbble', 'icon': 'fa fa-dribbble'},
                    {'name': 'dropbox', 'icon': 'fa fa-dropbox'},
                    {'name': 'drupal', 'icon': 'fa fa-drupal'},
                    {'name': 'edge', 'icon': 'fa fa-edge'},
                    {'name': 'empire', 'icon': 'fa fa-empire'},
                    {'name': 'envira', 'icon': 'fa fa-envira'},
                    {'name': 'expeditedssl', 'icon': 'fa fa-expeditedssl'},
                    {'name': 'fa', 'icon': 'fa fa-fa'},
                    {'name': 'facebook', 'icon': 'fa fa-facebook'},
                    {'name': 'facebook-f', 'icon': 'fa fa-facebook-f'},
                    {'name': 'facebook-official', 'icon': 'fa fa-facebook-official'},
                    {'name': 'facebook-square', 'icon': 'fa fa-facebook-square'},
                    {'name': 'firefox', 'icon': 'fa fa-firefox'},
                    {'name': 'first-order', 'icon': 'fa fa-first-order'},
                    {'name': 'flickr', 'icon': 'fa fa-flickr'},
                    {'name': 'font-awesome', 'icon': 'fa fa-font-awesome'},
                    {'name': 'fonticons', 'icon': 'fa fa-fonticons'},
                    {'name': 'fort-awesome', 'icon': 'fa fa-fort-awesome'},
                    {'name': 'forumbee', 'icon': 'fa fa-forumbee'},
                    {'name': 'foursquare', 'icon': 'fa fa-foursquare'},
                    {'name': 'ge', 'icon': 'fa fa-ge'},
                    {'name': 'get-pocket', 'icon': 'fa fa-get-pocket'},
                    {'name': 'gg', 'icon': 'fa fa-gg'},
                    {'name': 'gg-circle', 'icon': 'fa fa-gg-circle'},
                    {'name': 'git', 'icon': 'fa fa-git'},
                    {'name': 'git-square', 'icon': 'fa fa-git-square'},
                    {'name': 'github', 'icon': 'fa fa-github'},
                    {'name': 'github-alt', 'icon': 'fa fa-github-alt'},
                    {'name': 'github-square', 'icon': 'fa fa-github-square'},
                    {'name': 'gitlab', 'icon': 'fa fa-gitlab'},
                    {'name': 'gittip', 'icon': 'fa fa-gittip'},
                    {'name': 'glide', 'icon': 'fa fa-glide'},
                    {'name': 'glide-g', 'icon': 'fa fa-glide-g'},
                    {'name': 'google', 'icon': 'fa fa-google'},
                    {'name': 'google-plus', 'icon': 'fa fa-google-plus'},
                    {'name': 'google-plus-circle', 'icon': 'fa fa-google-plus-circle'},
                    {'name': 'google-plus-official', 'icon': 'fa fa-google-plus-official'},
                    {'name': 'google-plus-square', 'icon': 'fa fa-google-plus-square'},
                    {'name': 'google-wallet', 'icon': 'fa fa-google-wallet'},
                    {'name': 'gratipay', 'icon': 'fa fa-gratipay'},
                    {'name': 'hacker-news', 'icon': 'fa fa-hacker-news'},
                    {'name': 'houzz', 'icon': 'fa fa-houzz'},
                    {'name': 'html5', 'icon': 'fa fa-html5'},
                    {'name': 'instagram', 'icon': 'fa fa-instagram'},
                    {'name': 'internet-explorer', 'icon': 'fa fa-internet-explorer'},
                    {'name': 'ioxhost', 'icon': 'fa fa-ioxhost'},
                    {'name': 'joomla', 'icon': 'fa fa-joomla'},
                    {'name': 'jsfiddle', 'icon': 'fa fa-jsfiddle'},
                    {'name': 'lastfm', 'icon': 'fa fa-lastfm'},
                    {'name': 'lastfm-square', 'icon': 'fa fa-lastfm-square'},
                    {'name': 'leanpub', 'icon': 'fa fa-leanpub'},
                    {'name': 'linkedin', 'icon': 'fa fa-linkedin'},
                    {'name': 'linkedin-square', 'icon': 'fa fa-linkedin-square'},
                    {'name': 'linux', 'icon': 'fa fa-linux'},
                    {'name': 'maxcdn', 'icon': 'fa fa-maxcdn'},
                    {'name': 'meanpath', 'icon': 'fa fa-meanpath'},
                    {'name': 'medium', 'icon': 'fa fa-medium'},
                    {'name': 'mixcloud', 'icon': 'fa fa-mixcloud'},
                    {'name': 'modx', 'icon': 'fa fa-modx'},
                    {'name': 'odnoklassniki', 'icon': 'fa fa-odnoklassniki'},
                    {'name': 'odnoklassniki-square', 'icon': 'fa fa-odnoklassniki-square'},
                    {'name': 'opencart', 'icon': 'fa fa-opencart'},
                    {'name': 'openid', 'icon': 'fa fa-openid'},
                    {'name': 'opera', 'icon': 'fa fa-opera'},
                    {'name': 'optin-monster', 'icon': 'fa fa-optin-monster'},
                    {'name': 'pagelines', 'icon': 'fa fa-pagelines'},
                    {'name': 'paypal', 'icon': 'fa fa-paypal'},
                    {'name': 'pied-piper', 'icon': 'fa fa-pied-piper'},
                    {'name': 'pied-piper-alt', 'icon': 'fa fa-pied-piper-alt'},
                    {'name': 'pied-piper-pp', 'icon': 'fa fa-pied-piper-pp'},
                    {'name': 'pinterest', 'icon': 'fa fa-pinterest'},
                    {'name': 'pinterest-p', 'icon': 'fa fa-pinterest-p'},
                    {'name': 'pinterest-square', 'icon': 'fa fa-pinterest-square'},
                    {'name': 'product-hunt', 'icon': 'fa fa-product-hunt'},
                    {'name': 'qq', 'icon': 'fa fa-qq'},
                    {'name': 'ra', 'icon': 'fa fa-ra'},
                    {'name': 'rebel', 'icon': 'fa fa-rebel'},
                    {'name': 'reddit', 'icon': 'fa fa-reddit'},
                    {'name': 'reddit-alien', 'icon': 'fa fa-reddit-alien'},
                    {'name': 'reddit-square', 'icon': 'fa fa-reddit-square'},
                    {'name': 'renren', 'icon': 'fa fa-renren'},
                    {'name': 'resistance', 'icon': 'fa fa-resistance'},
                    {'name': 'safari', 'icon': 'fa fa-safari'},
                    {'name': 'scribd', 'icon': 'fa fa-scribd'},
                    {'name': 'sellsy', 'icon': 'fa fa-sellsy'},
                    {'name': 'share-alt', 'icon': 'fa fa-share-alt'},
                    {'name': 'share-alt-square', 'icon': 'fa fa-share-alt-square'},
                    {'name': 'shirtsinbulk', 'icon': 'fa fa-shirtsinbulk'},
                    {'name': 'simplybuilt', 'icon': 'fa fa-simplybuilt'},
                    {'name': 'skyatlas', 'icon': 'fa fa-skyatlas'},
                    {'name': 'skype', 'icon': 'fa fa-skype'},
                    {'name': 'slack', 'icon': 'fa fa-slack'},
                    {'name': 'slideshare', 'icon': 'fa fa-slideshare'},
                    {'name': 'snapchat', 'icon': 'fa fa-snapchat'},
                    {'name': 'snapchat-ghost', 'icon': 'fa fa-snapchat-ghost'},
                    {'name': 'snapchat-square', 'icon': 'fa fa-snapchat-square'},
                    {'name': 'soundcloud', 'icon': 'fa fa-soundcloud'},
                    {'name': 'spotify', 'icon': 'fa fa-spotify'},
                    {'name': 'stack-exchange', 'icon': 'fa fa-stack-exchange'},
                    {'name': 'stack-overflow', 'icon': 'fa fa-stack-overflow'},
                    {'name': 'steam', 'icon': 'fa fa-steam'},
                    {'name': 'steam-square', 'icon': 'fa fa-steam-square'},
                    {'name': 'stumbleupon', 'icon': 'fa fa-stumbleupon'},
                    {'name': 'stumbleupon-circle', 'icon': 'fa fa-stumbleupon-circle'},
                    {'name': 'tencent-weibo', 'icon': 'fa fa-tencent-weibo'},
                    {'name': 'themeisle', 'icon': 'fa fa-themeisle'},
                    {'name': 'trello', 'icon': 'fa fa-trello'},
                    {'name': 'tripadvisor', 'icon': 'fa fa-tripadvisor'},
                    {'name': 'tumblr', 'icon': 'fa fa-tumblr'},
                    {'name': 'tumblr-square', 'icon': 'fa fa-tumblr-square'},
                    {'name': 'twitch', 'icon': 'fa fa-twitch'},
                    {'name': 'twitter', 'icon': 'fa fa-twitter'},
                    {'name': 'twitter-square', 'icon': 'fa fa-twitter-square'},
                    {'name': 'usb', 'icon': 'fa fa-usb'},
                    {'name': 'viacoin', 'icon': 'fa fa-viacoin'},
                    {'name': 'viadeo', 'icon': 'fa fa-viadeo'},
                    {'name': 'viadeo-square', 'icon': 'fa fa-viadeo-square'},
                    {'name': 'vimeo', 'icon': 'fa fa-vimeo'},
                    {'name': 'vimeo-square', 'icon': 'fa fa-vimeo-square'},
                    {'name': 'vine', 'icon': 'fa fa-vine'},
                    {'name': 'vk', 'icon': 'fa fa-vk'},
                    {'name': 'wechat', 'icon': 'fa fa-wechat'},
                    {'name': 'weibo', 'icon': 'fa fa-weibo'},
                    {'name': 'weixin', 'icon': 'fa fa-weixin'},
                    {'name': 'whatsapp', 'icon': 'fa fa-whatsapp'},
                    {'name': 'wikipedia-w', 'icon': 'fa fa-wikipedia-w'},
                    {'name': 'windows', 'icon': 'fa fa-windows'},
                    {'name': 'wordpress', 'icon': 'fa fa-wordpress'},
                    {'name': 'wpbeginner', 'icon': 'fa fa-wpbeginner'},
                    {'name': 'wpforms', 'icon': 'fa fa-wpforms'},
                    {'name': 'xing', 'icon': 'fa fa-xing'},
                    {'name': 'xing-square', 'icon': 'fa fa-xing-square'},
                    {'name': 'y-combinator', 'icon': 'fa fa-y-combinator'},
                    {'name': 'y-combinator-square', 'icon': 'fa fa-y-combinator-square'},
                    {'name': 'yahoo', 'icon': 'fa fa-yahoo'},
                    {'name': 'yc', 'icon': 'fa fa-yc'},
                    {'name': 'yc-square', 'icon': 'fa fa-yc-square'},
                    {'name': 'yelp', 'icon': 'fa fa-yelp'},
                    {'name': 'yoast', 'icon': 'fa fa-yoast'},
                    {'name': 'youtube', 'icon': 'fa fa-youtube'},
                    {'name': 'youtube-play', 'icon': 'fa fa-youtube-play'},
                    {'name': 'youtube-square', 'icon': 'fa fa-youtube-square'}
                ],
                [
                    // 医学
                    {'name': 'ambulance', 'icon': 'fa fa-ambulance'},
                    {'name': 'h-square', 'icon': 'fa fa-h-square'},
                    {'name': 'heart', 'icon': 'fa fa-heart'},
                    {'name': 'heart-o', 'icon': 'fa fa-heart-o'},
                    {'name': 'heartbeat', 'icon': 'fa fa-heartbeat'},
                    {'name': 'hospital-o', 'icon': 'fa fa-hospital-o'},
                    {'name': 'medkit', 'icon': 'fa fa-medkit'},
                    {'name': 'plus-square', 'icon': 'fa fa-plus-square'},
                    {'name': 'stethoscope', 'icon': 'fa fa-stethoscope'},
                    {'name': 'user-md', 'icon': 'fa fa-user-md'},
                    {'name': 'wheelchair', 'icon': 'fa fa-wheelchair'},
                    {'name': 'wheelchair-alt', 'icon': 'fa fa-wheelchair-alt'}
                ]
            ];

            // 自动加载tab
            var $layer = $("#" + self.options.id + "Layer-icon").closest('.layer-dialog');
            var liMerge = '';
            $.each(tabs, function (index, value) {
                var act = index == 0 ? 'active' : '';
                var numcls = "badge bg-color-blue txt-color-white";
                if (index >= 6 && index % 2 == 0) {
                    numcls = "badge bg-color-green txt-color-white";
                }

                // add tab
                var li = '<li class="' + act + '">'
                    + '      <a href="#' + self.options.id + value['id'] + '" data-toggle="tab"><i class="' + value['icon'] + '"></i>' + value['name'] + ' <span class="' + numcls + '">' + iocns[index].length + '</span>'
                    + '     </a> '
                    + '   </li>';
                if (index < 5) {
                    liMerge += li;
                } else if (index == 5) {
                    liMerge += li;
                    $layer.find(".nav, .nav-tabs").find('#icon-select-temp').replaceWith(liMerge);
                } else {
                    if (index == 8 || index == 11) {
                        $layer.find(".nav, .nav-tabs").find('.dropdown-menu').append('<li class="divider"></li>');
                    }
                    $layer.find(".nav, .nav-tabs").find('.dropdown-menu').append(li);
                }

                // add tab content
                var tabCls = "tab-pane fade";
                if (index == 0) {
                    tabCls = "tab-pane fade in active";
                }
                var tabDiv = ''
                    + ' <div class="' + tabCls + '" id="' + self.options.id + value['id'] + '">'
                    + '     <section>'
                    + '         <div class="row fontawesome-icon-list">'
                    + '         </div>'
                    + '     </section>'
                    + ' </div>';
                $layer.find('.tab-content').append(tabDiv);
            });

            // 获取前五项
            var lines = "";
            $.each(iocns, function (index, value) {
                if (index < 5) {
                    lines += value.length;
                    if (index != 4) {
                        lines += ",";
                    }
                } else {
                    return false;
                }
            });
            // 设置line值
            $layer.find('.sparkline').html(lines);

            // 自动加载icons
            $.each(iocns, function (index, value) {
                $.each(value, function (i, v) {
                    var iconDiv = ''
                        + ' <div class="fa-hover col-md-3 col-sm-4">'
                        + '    <a href="javascript:void(0);"><i class="' + v['icon'] + '" aria-hidden="true"></i> ' + v['name']
                        + '    </a>'
                        + ' </div>'
                    $layer.find("#" + self.options.id + tabs[index]['id']).find('.fontawesome-icon-list').append(iconDiv);
                });
            });

            // 对话框的html暂时不显示
            $layer.hide();

            //注册事件
            this.registerEvent();

            $layer.find(".layer-icon-list .fontawesome-icon-list a").click(function () {
                $layer.find(".layer-icon-list .fontawesome-icon-list a").removeClass("icon-active");
                $(this).addClass("icon-active");
                $("#" + self.options.id + "Layer-icon").val($(this).find("i").attr('class'));
                $("#" + self.options.id + "Layer-iconName").val($(this).text());
            });
            $layer.find(".layer-icon-list .fontawesome-icon-list a").dblclick(function () {
                $layer.closest('.layui-layer').find('.layui-layer-btn0').trigger("click");
            });
        },

        /**
         * 创建表单html内容
         */
        createTemplate: function () {

            var iconClass = 'hide';
            if (this.options.value != null && this.options.value != '') {
                iconClass = this.options.value;
            }

            // 加入参数，注意key重复
            var formTemplate = ''
                + ' <div style="margin: 4px auto;">'
                + '     <i id="' + this.options.id + 'Icon" class="' + iconClass + '"></i>&nbsp;'
                + '     <label id="' + this.options.id + 'IconLabel">' + this.options.labelValue + '</label>&nbsp;'
                + '     <input id="' + this.options.id + '" name="' + this.options.name + '" type="hidden" value="' + this.options.value + '"/>'
                + '     <a id="' + this.options.id + 'Button" href="javascript:" class="btn btn-info">图标选择</a>&nbsp;&nbsp;'
                + ' </div>';
            var iconTemplate = ''
                + ' <div class="layer-dialog">'
                + '     <input type="hidden" id="' + this.options.id + 'Layer-icon" value="" />'
                + '     <input type="hidden" id="' + this.options.id + 'Layer-iconName" value="" />'
                + '     <div class="layer-icon-list">'
                + '         <ul class="nav nav-tabs">'
                + '             <li id="icon-select-temp"></li>'
                + '             <li class="dropdown pull-left">'
                + '                 <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-lg fa-gear"></i> <b class="caret"></b>' +
                '                   </a>'
                + '                 <ul class="dropdown-menu">'
                + '                 </ul>'
                + '             </li>'
                + '             <li class="pull-right">'
                + '                 <a href="javascript:void(0);">'
                + '                     <div class="txt-color-pinkDark text-align-right"><i class="fa fa-gear"></i>'
                + '                     </div> '
                + '                 </a>'
                + '             </li>'
                + '         </ul>'
                + '         <div class="tab-content padding-10 margin-right-5">' // padding-10
                + '         </div>'
                + '     </div>'
                + ' </div>';

            if (this.options.databind_dialog != null) {
                $(this.options.databind_dialog).html(iconTemplate);
                return formTemplate;
            }

            return formTemplate + iconTemplate;
        },

        /**
         * 因为不能每次都初始化，也不能使用模板，所以需要改变输入内容时调用这个方法
         */
        refresh: function (options) {
            // 覆盖自己输入属性
            this.options = $.extend(true, {}, this.options, options);
            // id、name 固定
            // 设置icon
            var iconClass = 'hide';
            if (this.options.value != null && this.options.value != '') {
                iconClass = this.options.value;
            }
            $("#" + this.options.id + "Icon").removeClass().addClass(iconClass);
            // 设置labelValue
            $("#" + this.options.id + "IconLabel").text(this.options.labelValue);
            // 设置value
            $("#" + this.options.id).val(this.options.value);
        },

        /**
         * 注册输入框、按钮的事件
         */
        registerEvent: function () {
            var self = this;
            // 需要注册事件的组件
            $('#' + this.options.id + 'Button').on('click', function () {
                self.iconLayer();
                // 设置当前选择
            });
        },

        /**
         * 弹出icon选择对话框
         */
        iconLayer: function () {
            var self = this;
            // 显示的title
            var title = '选择图标';
            if (this.options.title != null && this.options.title != '') {
                title = this.options.title;
            }
            var $layer = $("#" + self.options.id + "Layer-icon").closest('.layer-dialog');

            // 正常打开
            window.top.layer.open({
                type: 1,
                title: title,
                shadeClose: true,
                shade: false,
                maxmin: true, //开启最大化最小化按钮
                area: ['1000px', '650px'],
                zIndex: layer.zIndex,
                content: $layer,
                btn: ['确定', '清除', '关闭'],
                success: function (layero) {
                    var $as = $layer.find(".layer-icon-list .fontawesome-icon-list a");
                    $as.removeClass("icon-active");
                    var $tab = null;
                    $as.each(function () {
                        // label 有很多空格或者回车换行,这里用class比较
                        if ($(this).children("i").attr('class') == self.options.value) {
                            $(this).click();
                            $tab = $(this).closest('.tab-pane');
                            return false;
                        }
                    });
                    // 激活tab
                    if ($tab) {
                        $layer.find('.nav, .nav-tabs').find('a[href="#' + $tab.attr('id') + '"]').trigger('click');
                    }
                    layer.setTop(layero);
                },
                yes: function (index) { //或者使用btn1
                    //按钮【确定】的回调
                    var icon = $layer.find("#" + self.options.id + "Layer-icon").val();
                    var iconName = $layer.find("#" + self.options.id + "Layer-iconName").val();
                    $('#' + self.options.id + 'Icon').attr("class", icon);
                    $('#' + self.options.id + 'IconLabel').text(iconName);
                    $('#' + self.options.id).val(icon);
                    self.options.value = icon;
                    self.options.labelValue = iconName;
                    // 默认关闭事件
                    layer.close(index);
                },
                cancel: function (index) { //或者使用btn2
                    //按钮【清除】的回调
                    $('#' + self.options.id + 'Icon').attr("class", "hide");
                    $('#' + self.options.id + 'IconLabel').text("无");
                    $('#' + self.options.id).val("");
                    self.options.value = "";
                    self.options.labelValue = "";
                    // 调用默认关闭事件，防止不回调
                    layer.close(index);
                },
                btn3: function (index) {
                    //按钮【关闭】的回调
                    layer.close(index);
                }
            });
        }
    };

    return IconSelecter;
}))
;
