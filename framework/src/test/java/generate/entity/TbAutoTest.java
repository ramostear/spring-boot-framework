package generate.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_auto_test")
public class TbAutoTest {
    /**
     * 编号
     */
    @Id
    @SequenceGenerator(name="",sequenceName="SELECT LAST_INSERT_ID()")
    private String id;

    /**
     * 时间
     */
    private Date time;

    /**
     * 状态，0:正常,1:异常
     */
    private Boolean status;

    /**
     * 总数
     */
    @Column(name = "total_count")
    private Long totalCount;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 备注信息
     */
    private Long remarks;

    /**
     * 获取编号
     *
     * @return id - 编号
     */
    public String getId() {
        return id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取时间
     *
     * @return time - 时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置时间
     *
     * @param time 时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取状态，0:正常,1:异常
     *
     * @return status - 状态，0:正常,1:异常
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置状态，0:正常,1:异常
     *
     * @param status 状态，0:正常,1:异常
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取总数
     *
     * @return total_count - 总数
     */
    public Long getTotalCount() {
        return totalCount;
    }

    /**
     * 设置总数
     *
     * @param totalCount 总数
     */
    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 获取描述信息
     *
     * @return description - 描述信息
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述信息
     *
     * @param description 描述信息
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取备注信息
     *
     * @return remarks - 备注信息
     */
    public Long getRemarks() {
        return remarks;
    }

    /**
     * 设置备注信息
     *
     * @param remarks 备注信息
     */
    public void setRemarks(Long remarks) {
        this.remarks = remarks;
    }
}