package com.zeyu.framework.core.common.mapper;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.zeyu.framework.modules.sys.entity.User;
import com.zeyu.framework.utils.SerializeUtils;
import org.junit.Test;
import org.springframework.test.context.TestExecutionListeners;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * json test
 * Created by zeyuphoenix on 16/7/21.
 */
public class JsonMapperTest {

    @Test
    public void test() {
        {
            List<Map<String, Object>> list = Lists.newArrayList();
            Map<String, Object> map = Maps.newHashMap();
            map.put("id", 1);
            map.put("pId", -1);
            map.put("name", "根节点");
            list.add(map);
            map = Maps.newHashMap();
            map.put("id", 2);
            map.put("pId", 1);
            map.put("name", "你好");
            map.put("open", true);
            list.add(map);
            String json = JsonMapper.getInstance().toJson(list);
            System.out.println(json);
            System.out.println(JsonMapper.getInstance().fromJson(json, List.class));

            Map<String, List<String>> empmap = Maps.newHashMap();
            empmap.put("emplist", new ArrayList<>());
            System.out.println(JsonMapper.getInstance().toJson(empmap));

            try {
                System.out.println(URLEncoder.encode("颜色值", "utf-8"));
                System.out.println(URLDecoder.decode(URLEncoder.encode("颜色值", "utf-8"), "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void seria() {
        User u = new User();
        u.setId("xxxxx");
        u.setEmail("zeyu@163.com");

        String v = SerializeUtils.serializeToString(u);
        System.out.println(v);

        User xv = SerializeUtils.deserializeFromString(v);
        System.out.println(xv);

        List<User> us = Lists.newArrayList();
        us.add(u);
        us.add(new User("xxv"));

        String vs = SerializeUtils.serializeToString((Serializable) us);
        System.out.println(vs);

        List<User> xvs = SerializeUtils.deserializeFromString(vs);
        System.out.println(xvs);

    }
}