package com.zeyu.framework.utils;


import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;
import java.util.Random;

/**
 * test process
 * Created by zeyuphoenix on 16/9/2.
 */
public class MultProcessUtilsTest {

    @Test
    public void runWork() {
        List<MultProcessUtils.Worker> workers = Lists.newArrayList();
        for (int i = 0; i < 8; i++) {
            workers.add(new TestWorker(new Random().nextInt(10000) + " infos."));
        }

        try {
            MultProcessUtils.invoker(workers);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * define worker
     */
    private static class TestWorker extends MultProcessUtils.Worker {

        private String tempInfo = null;

        public TestWorker(String tempInfo) {
            this.tempInfo = tempInfo;
        }

        @Override
        public void doWork() {
            try {
                System.out.println(tempInfo);
                Thread.sleep(new Random().nextInt(10000));
            } catch (InterruptedException ignored) {
            }
        }

    }
}