package com.zeyu.framework.utils;

import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.modules.sys.entity.Log;
import org.apache.commons.beanutils.WrapDynaBean;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * test dyna add bean properties
 * Created by zeyuphoenix on 16/8/13.
 */
public class WarpBeanTest {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(WarpBeanTest.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Test
    public void add() {
        Log log = new Log();
        log.setId("12345");
        log.setTitle("test");
        logger.info("{}", JsonMapper.getInstance().toJson(log));

        WrapDynaBean bean = new WrapDynaBean(log);
        bean.set("title", "reconf");
        logger.info("{}", JsonMapper.getInstance().toJson(log));
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
