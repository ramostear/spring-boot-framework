package com.zeyu.framework.utils.excel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * 导入测试
 * Created by zeyuphoenix on 2017/5/23.
 */
public class ImportExcelTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ImportExcelTest.class);

    @Test
    public void importExcel() throws IOException, InvalidFormatException {
        ImportExcel ei = new ImportExcel("target/export.xlsx", 1);

        for (int i = ei.getDataRowNum(); i < ei.getLastDataRowNum(); i++) {
            Row row = ei.getRow(i);
            for (int j = 0; j < ei.getLastCellNum(); j++) {
                Object val = ei.getCellValue(row, j);
                logger.info(val + ", ");
            }
            logger.info("\n");
        }
    }
}