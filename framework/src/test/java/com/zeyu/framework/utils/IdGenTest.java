package com.zeyu.framework.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 测试id generate
 * Created by zeyuphoenix on 16/7/20.
 */
public class IdGenTest {

    @Test
    public void test() {
        // 32位uuid
        System.out.println(IdGen.uuid());
        System.out.println(IdGen.uuid().length());
        // 36位uuid
        System.out.println(IdGen.uuid36());
        System.out.println(IdGen.uuid36().length());
        // activiti id
        System.out.println(new IdGen().getNextId());
        // base64
        for (int i = 0; i < 1000; i++) {
            System.out.println(IdGen.randomLong() + "  " + IdGen.randomBase62(5));
        }
        // twwitter
        for (int i = 0; i < 100; i++) {
            System.out.println(IdGen.nonRepByTwitter());
        }

        // 计算id生产效率
        long avg = 0;
        for (int k = 0; k < 10; k++) {
            List<Callable<Long>> partitions = new ArrayList<>();
            final IdGen.LongIdGen idGen = IdGen.LongIdGen.get();
            for (int i = 0; i < 1400000; i++) {
                partitions.add(idGen::nextId);
            }
            ExecutorService executorPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            try {
                long s = System.currentTimeMillis();
                executorPool.invokeAll(partitions, 10000, TimeUnit.SECONDS);
                long s_avg = System.currentTimeMillis() - s;
                avg += s_avg;
                System.out.println("完成时间需要: " + s_avg / 1.0e3 + "秒");
                executorPool.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("平均完成时间需要: " + avg / 10 / 1.0e3 + "秒");
    }

}