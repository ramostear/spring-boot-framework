package com.zeyu.framework.utils;

import org.junit.Test;

/**
 * mac test
 * Created by zeyuphoenix on 16/7/20.
 */
public class MacUtilsTest {

    @Test
    public void test() {


        // get system type
        String os = MacUtils.getOSName();
        System.out.println("os: " + os);

        // get mac
        if (os.startsWith("windows")) {
            String mac = MacUtils.getWindowsMACAddress();
            System.out.println("mac: " + mac);
        } else if (os.startsWith("linux")) {
            String mac = MacUtils.getLinuxMACAddress();
            System.out.println("mac: " + mac);
        } else {
            String mac = MacUtils.getUnixMACAddress();
            System.out.println("mac: " + mac);
        }

        System.out.println(MacUtils.getMac());
    }
}