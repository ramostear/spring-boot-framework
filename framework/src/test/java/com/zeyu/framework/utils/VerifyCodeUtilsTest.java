package com.zeyu.framework.utils;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

/**
 * 测试验证码图片生成
 * Created by zeyuphoenix on 16/8/23.
 */
public class VerifyCodeUtilsTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(VerifyCodeUtilsTest.class);

    @Test
    public void generateVerifyCode() {
        String  code = VerifyCodeUtils.generateVerifyCode(4);

        logger.info("code is {}", code);
    }
}