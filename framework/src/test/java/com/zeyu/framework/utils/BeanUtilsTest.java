package com.zeyu.framework.utils;

import com.zeyu.framework.tools.console.entity.SSHConfiguration;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * bean utils test
 * Created by zeyuphoenix on 16/7/20.
 */
public class BeanUtilsTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(BeanUtilsTest.class);

    @Test
    public void test() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        SSHConfiguration configuration = new SSHConfiguration();

        configuration.setColor_scheme("64");
        configuration.setEnable_sftp(true);
        configuration.setHostname("192.168.12.212");
        configuration.setPort(3366);
        configuration.setUsername("root");
        configuration.setPassword("123456");

        Map<String, String> map = BeanUtils.describe(configuration);
        //BeanUtils.copyProperties(map, configuration);

        logger.info("configuration is {}", configuration);

        logger.info("result is {}", map);

        SSHConfiguration copyed = new SSHConfiguration();
        logger.info("configuration is {}", copyed);

        BeanUtils.copyProperties(copyed, map);

        logger.info("result is {}", copyed);

    }
}