package com.zeyu.framework.utils;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

/**
 * 二维码测试
 * Created by zeyuphoenix on 16/7/29.
 */
public class QRCodeUtilsTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(QRCodeUtilsTest.class);

    @Test
    public void encode() throws Exception {
        ResourceLoader loader = new DefaultResourceLoader();
        Resource resource = loader.getResource("classpath:/");
        String imgPath = resource.getFile().getPath() + "/123.png";
        // 益达无糖口香糖的条形码
        String contents = "6923450657713";

        int width = 105, height = 50;
        QRCodeUtils.encode(contents, width, height, imgPath);

        logger.info("success, you have finished zxing EAN13 encode.");
    }

    @Test
    public void decode() throws Exception {
        ResourceLoader loader = new DefaultResourceLoader();
        Resource resource = loader.getResource("classpath:ean13.png");
        String imgPath = resource.getFile().getPath();
        String decodeContent = QRCodeUtils.decode(imgPath);
        logger.info("解码内容如下：");
        logger.info(decodeContent);
        logger.info("success, you have finished zxing EAN-13 decode.");
    }

    @Test
    public void encode2() throws Exception {
        ResourceLoader loader = new DefaultResourceLoader();
        Resource resource = loader.getResource("classpath:/");
        String imgPath = resource.getFile().getPath() + "/code_logo.png";
        // 二维码信息
        String contents = "【优秀员工】恭喜您，中奖了！！！领取方式，请拨打电话：18888888888*咨询。";

        resource = loader.getResource("classpath:logo.jpg");
        String logoPath = resource.getFile().getPath();
        QRCodeUtils.encode2(contents,logoPath,imgPath);

        logger.info("success, you have finished zxing logo code encode.");
    }

    @Test
    public void encode2_nologo() throws Exception {
        ResourceLoader loader = new DefaultResourceLoader();
        Resource resource = loader.getResource("classpath:/");
        String imgPath = resource.getFile().getPath() + "/code_nolog.png";
        // 二维码信息
        String contents = "【优秀员工】恭喜您，中奖了！！！领取方式，请拨打电话：18888888888*咨询。";

        QRCodeUtils.encode2(contents,imgPath);

        logger.info("success, you have finished zxing nologo code encode.");
    }

    @Test
    public void decode2() throws Exception {
        ResourceLoader loader = new DefaultResourceLoader();
        Resource resource = loader.getResource("classpath:code_logo.png");
        String imgPath = resource.getFile().getPath();
        String decodeContent = QRCodeUtils.decode2(imgPath);
        logger.info("解码内容如下：");
        logger.info(decodeContent);
        logger.info("success, you have finished zxing code logo decode.");
    }



}