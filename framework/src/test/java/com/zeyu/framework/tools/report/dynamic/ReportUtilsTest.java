package com.zeyu.framework.tools.report.dynamic;

import com.google.common.collect.Lists;
import com.zeyu.framework.tools.report.charts.ChartData;
import com.zeyu.framework.tools.report.charts.ExtendChartData;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 报表工具测试类
 * Created by zeyuphoenix on 16/9/4.
 */
public class ReportUtilsTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ReportUtilsTest.class);

    @Test
    public void test() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"一月", "二月", "三月", "四月",
                "五月", "六月", "七月", "八月", "九月"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();
        CollectionUtils.addAll(serie1, new Number[]{120, 232, 301000000, 434,
                390, 53000000, 32000000, 120, 250});
        List<Number> serie2 = Lists.newArrayList();
        CollectionUtils.addAll(serie2, new Number[]{150, 100060, 100090, 150,
                2000032, 20000001, 1000054, 390, 10000010});
        series.add(serie1);
        series.add(serie2);

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("line");
        extendChartData.setTilte("用户流量趋势图");
        List<String> legends = Lists.newArrayList();
        legends.add("上行流量");
        legends.add("下行流量");
        // legends
        extendChartData.setLegends(legends);
        extendChartData.setDatas(datas);

        logger.info(ReportUtils.flowTransform(0));
        logger.info(ReportUtils.packetTransform(0));
        logger.info(ReportUtils.flowTransform(100));
        logger.info(ReportUtils.packetTransform(100));
        logger.info(ReportUtils.flowTransform(100.0));
        logger.info(ReportUtils.packetTransform(100.0));

        logger.info(ReportUtils.flowTransform(10000));
        logger.info(ReportUtils.packetTransform(10000));
        logger.info(ReportUtils.flowTransform(2334566));
        logger.info(ReportUtils.packetTransform(2323232, 4));
        logger.info(ReportUtils.flowTransform(56786665656L));
        logger.info(ReportUtils.packetTransform(2323565656232L));

        logger.info(ArrayUtils
                .toString(ReportUtils.generateColumnsFromChartData(extendChartData)));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateDatasFromChartData(extendChartData)));

        // 图样式和数据对象
        extendChartData = new ExtendChartData();
        // 图数据对象
        datas = new ChartData();

        // xAxis
        xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"流量", "报文", "速率", "攻击", "病毒"});

        // series
        series = Lists.newArrayList();
        serie1 = Lists.newArrayList();
        CollectionUtils
                .addAll(serie1, new Number[]{820, 1232, 31, 241, 100});
        serie2 = Lists.newArrayList();
        CollectionUtils.addAll(serie2, new Number[]{320, 800, 91, 234, 10});
        series.add(serie1);
        series.add(serie2);

        // max
        List<Number> max = Lists.newArrayList();
        CollectionUtils.addAll(max, new Number[]{1000, 1500, 100, 300, 200});

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);
        datas.setMax(max);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("radar");
        extendChartData.setTilte("用户趋势图");
        legends = Lists.newArrayList();
        legends.add("张三");
        legends.add("李四");
        // legends
        extendChartData.setLegends(legends);
        extendChartData.setDatas(datas);

        logger.info(ArrayUtils
                .toString(ReportUtils.generateColumnsFromChartData(extendChartData)));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateDatasFromChartData(extendChartData)));

        logger.info(ArrayUtils
                .toString(ReportUtils.generateColumnsFromChartData(createLineData())));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateDatasFromChartData(createLineData())));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateColumnsFromChartData(createPieData())));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateDatasFromChartData(createPieData())));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateColumnsFromChartData(createRadarData())));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateDatasFromChartData(createRadarData())));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateColumnsFromChartData(createGaugeData())));
        logger.info(ArrayUtils
                .toString(ReportUtils.generateDatasFromChartData(createGaugeData())));
    }

    /**
     * 根据图和表的对象生产图、图表、表，以后考虑可以根据配置生成图和表对象
     */
    public static ExtendChartData createLineData() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"一月", "二月", "三月", "四月",
                "五月", "六月", "七月", "八月", "九月"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();
        CollectionUtils.addAll(serie1, new Number[]{120, 232, 301000000, 434,
                390, 53000000, 32000000, 120, 250});
        List<Number> serie2 = Lists.newArrayList();
        CollectionUtils.addAll(serie2, new Number[]{150, 100060, 100090, 150,
                2000032, 20000001, 1000054, 390, 10000010});
        series.add(serie1);
        series.add(serie2);

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("line");
        extendChartData.setTilte("用户流量趋势图");
        List<String> legends = Lists.newArrayList();
        legends.add("上行流量");
        legends.add("下行流量");
        // legends
        extendChartData.setLegends(legends);
        extendChartData.setDatas(datas);

        return extendChartData;
    }

    public static ExtendChartData createPieData() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"p2p流量", "qq流量", "ftp流量",
                "web流量", "游戏流量", "其它流量"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();
        CollectionUtils.addAll(serie1, new Number[]{301000000, 43422, 390,
                530, 32000000, 23565});
        series.add(serie1);

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("pie");
        extendChartData.setTilte("用户流量分布图");

        // legends
        extendChartData.setName("用户流量");
        extendChartData.setDatas(datas);

        return extendChartData;
    }

    public static ExtendChartData createGaugeData() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"速率"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();
        CollectionUtils.addAll(serie1, new Number[]{85});
        series.add(serie1);

        // max
        List<Number> max = Lists.newArrayList();
        CollectionUtils.addAll(max, new Number[]{150});
        // min
        List<Number> min = Lists.newArrayList();
        CollectionUtils.addAll(min, new Number[]{0});

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);
        datas.setMax(max);
        datas.setMin(min);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("gauge");
        extendChartData.setTilte("用户速率图");
        // legends
        extendChartData.setName("用户速率");
        extendChartData.setDatas(datas);

        return extendChartData;
    }

    public static ExtendChartData createRadarData() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"流量", "报文", "速率", "攻击",
                "病毒"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();
        CollectionUtils
                .addAll(serie1, new Number[]{820, 1232, 31, 241, 100});
        List<Number> serie2 = Lists.newArrayList();
        CollectionUtils.addAll(serie2, new Number[]{320, 800, 91, 234, 10});
        series.add(serie1);
        series.add(serie2);

        // max
        List<Number> max = Lists.newArrayList();
        CollectionUtils.addAll(max, new Number[]{1000, 1500, 100, 300, 200});

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);
        datas.setMax(max);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("radar");
        extendChartData.setTilte("用户趋势图");
        // legends
        List<String> legends = Lists.newArrayList();
        legends.add("张三");
        legends.add("李四");
        // legends
        extendChartData.setLegends(legends);
        extendChartData.setName("用户趋势");
        extendChartData.setDatas(datas);

        return extendChartData;
    }
}