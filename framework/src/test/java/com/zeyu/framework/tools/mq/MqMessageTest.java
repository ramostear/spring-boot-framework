package com.zeyu.framework.tools.mq;

import com.zeyu.framework.tools.mq.sender.MQMessageSender;
import com.zeyu.framework.tools.mq.struct.CommandType;
import com.zeyu.framework.tools.mq.struct.MQMessage;
import com.zeyu.framework.tools.mq.struct.MessageType;
import com.zeyu.framework.tools.mq.struct.ServiceType;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * 测试mq的发送、接收
 * Created by zeyuphoenix on 2016/12/31.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@FixMethodOrder(MethodSorters.DEFAULT)   //按方法顺序执行
public class MqMessageTest {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(MqMessageTest.class);

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private MQMessageSender mqMessageSender;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Test
    public void sender() throws Exception {
        MQMessage mqMessage = MQMessageSender.buildMQMessage(
                MessageType.NORMAL, ServiceType.PLATFORM, CommandType.FETCH_INFO, "zeyu~xm", "我需要设备信息");
        mqMessageSender.sendCommon(mqMessage);
        mqMessage = MQMessageSender.buildMQMessage(
                MessageType.HEARTBEAT_REQUEST, ServiceType.PLATFORM, CommandType.CONNECTION, "zeyu~xm", "验证设备连接");
        mqMessageSender.sendHeart(mqMessage);
        mqMessage = MQMessageSender.buildMQMessage(
                MessageType.NORMAL, ServiceType.PLATFORM, CommandType.DEVICE_UPDATE_CONFIG, "zeyu~xm", "我需要大家都知道这个信息");
        mqMessageSender.sendPublish(mqMessage);
    }

    @Test
    public void sendRPC() throws Exception {
        MQMessage mqMessage = MQMessageSender.buildMQMessage(
                MessageType.SERVICE_REQUEST, ServiceType.PLATFORM, CommandType.CONNECTION, "zeyu~xm", "我需要设备应答信息");
        mqMessageSender.sendRPC(mqMessage);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
