package com.zeyu.framework.tools.cron.web;

import com.google.common.collect.Lists;
import com.zeyu.framework.tools.cron.entity.CronConst;
import com.zeyu.framework.tools.cron.entity.CronOption;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * test cron
 * Created by zeyuphoenix on 16/9/1.
 */
public class CronControllerTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(CronControllerTest.class);


    @Test
    public void full() {
        CronController controller = new CronController();
        // 反解析测试
        String cronExpression = "0 15 10 * * ? *";
        System.out.println(controller.parse(cronExpression));
        cronExpression = "20 0/5 14 * * ?";
        System.out.println(controller.parse(cronExpression));

        // 解析测试
        List<CronOption> cronOptions = Lists.newArrayList();

        // 0 15 10 * * ? *
        // 秒
        CronOption cronOption = new CronOption();
        cronOption.setOptionType(CronConst.SECOND);
        cronOption.setOptionCronType(CronConst.DEFINED);
        List<Integer> values = Lists.newArrayList();
        values.add(0);
        cronOption.setValues(values);
        cronOptions.add(cronOption);

        // 分
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.MINUTE);
        cronOption.setOptionCronType(CronConst.DEFINED);
        values = Lists.newArrayList();
        values.add(15);
        cronOption.setValues(values);
        cronOptions.add(cronOption);

        // 时
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.HOUR);
        cronOption.setOptionCronType(CronConst.DEFINED);
        values = Lists.newArrayList();
        values.add(10);
        cronOption.setValues(values);
        cronOptions.add(cronOption);

        // 天
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.DAY_OF_MONTH);
        cronOption.setOptionCronType(CronConst.EVERY);
        cronOptions.add(cronOption);

        // 月
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.MONTH);
        cronOption.setOptionCronType(CronConst.EVERY);
        cronOptions.add(cronOption);

        // 周
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.DAY_OF_WEEK);
        cronOption.setOptionCronType(CronConst.NON);
        cronOptions.add(cronOption);

        // 年
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.YEAR);
        cronOption.setOptionCronType(CronConst.EVERY);
        cronOptions.add(cronOption);

        System.out.println(controller.build(cronOptions));

        cronOptions = Lists.newArrayList();

        // 20 0/5 14 * * ?
        // 秒
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.SECOND);
        cronOption.setOptionCronType(CronConst.DEFINED);
        values = Lists.newArrayList();
        values.add(20);
        cronOption.setValues(values);
        cronOptions.add(cronOption);

        // 分
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.MINUTE);
        cronOption.setOptionCronType(CronConst.INCREASE);
        values = Lists.newArrayList();
        values.add(0);
        values.add(5);
        cronOption.setValues(values);
        cronOptions.add(cronOption);

        // 时
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.HOUR);
        cronOption.setOptionCronType(CronConst.DEFINED);
        values = Lists.newArrayList();
        values.add(14);
        cronOption.setValues(values);
        cronOptions.add(cronOption);

        // 天
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.DAY_OF_MONTH);
        cronOption.setOptionCronType(CronConst.EVERY);
        cronOptions.add(cronOption);

        // 月
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.MONTH);
        cronOption.setOptionCronType(CronConst.EVERY);
        cronOptions.add(cronOption);

        // 周
        cronOption = new CronOption();
        cronOption.setOptionType(CronConst.DAY_OF_WEEK);
        cronOption.setOptionCronType(CronConst.NON);
        cronOptions.add(cronOption);

        System.out.println(controller.build(cronOptions));
    }
}