package com.zeyu.framework.tools.mq.utils;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 测试加密解密message
 * Created by zeyuphoenix on 2016/12/31.
 */
public class EncryptUtilsTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(EncryptUtilsTest.class);

    final String message = "我的密码是@#$%Vd19841210";

    @Test
    public void encode() throws Exception {
        logger.info("encode " + EncryptUtils.encode(message));
    }

    @Test
    public void decode() throws Exception {
        logger.info("decode " + EncryptUtils.decode(EncryptUtils.encode(message)));
    }

}