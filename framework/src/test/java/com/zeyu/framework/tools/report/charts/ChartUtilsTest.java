package com.zeyu.framework.tools.report.charts;

import com.zeyu.framework.utils.FileUtils;
import com.zeyu.framework.utils.dynacompile.DynamicEngineTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;

/**
 * 测试图片转换
 * Created by zeyuphoenix on 16/9/3.
 */
public class ChartUtilsTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ChartUtilsTest.class);

    @Test
    public void trans() {
        try {
            URL url = DynamicEngineTest.class.getClassLoader().getResource("");

            if (url != null) {
                String path = url.getPath();
                String imgstr = ChartUtils.encodeImage(path + File.separator + "girl.png");
                logger.info("image encode is {} ", imgstr);
                FileUtils.writeStringToFile(new File(path + File.separator + "girl.txt"), imgstr, "UTF-8");
                ChartUtils.generateImage(imgstr, path + File.separator + "girl-w.png");
            }

        } catch (Exception e) {
            logger.error("", e);
        }
    }

}