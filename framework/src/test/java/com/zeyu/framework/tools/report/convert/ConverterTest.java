package com.zeyu.framework.tools.report.convert;

import com.google.common.collect.Lists;
import com.zeyu.framework.tools.report.charts.ChartData;
import com.zeyu.framework.tools.report.charts.ChartUtils;
import com.zeyu.framework.tools.report.charts.ExtendChartData;
import com.zeyu.framework.utils.dynacompile.DynamicEngineTest;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Random;

/**
 * 图表生成
 * Created by zeyuphoenix on 16/9/4.
 */
public class ConverterTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ConverterTest.class);

    @Test
    public void convert() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"一月", "二月", "三月", "四月",
                "五月", "六月", "七月", "八月", "九月"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();
        CollectionUtils.addAll(serie1, new Number[]{120, 232, 301000000, 434,
                390, 53000000, 32000000, 120, 250});
        List<Number> serie2 = Lists.newArrayList();
        CollectionUtils.addAll(serie2, new Number[]{150, 100060, 100090, 150,
                2000032, 20000001, 1000054, 390, 10000010});

        series.add(serie1);
        series.add(serie2);

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("line");
        extendChartData.setTilte("用户流量趋势图");
        List<String> legends = Lists.newArrayList();
        legends.add("上行流量");
        legends.add("下行流量");
        // legends
        extendChartData.setLegends(legends);
        extendChartData.setDatas(datas);
        try {
            URL url = DynamicEngineTest.class.getClassLoader().getResource("");

            if (url != null) {
                String path = url.getPath();
                // convert
                String output = Converter.getInstance().convert(extendChartData);

                ChartUtils.generateImage(output,
                        path + File.separator + new Random().nextInt(10000) + ".png");

                byte[] bytes = ChartUtils.generateImage(output);
                logger.debug("out bytes: ", bytes);

                ImageIcon icon = new ImageIcon(bytes);
                logger.debug("icon is: ", icon);

                // save file check result
                Image image = icon.getImage();
                BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
                Graphics2D g2d = bufferedImage.createGraphics();
                g2d.drawImage(image, 0, 0, null);
                ImageIO.write(bufferedImage, "png", new File(path + File.separator + new Random().nextInt(10000) + "by.png"));
                g2d.dispose();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("system exit.");
    }

    @Test
    public void convertSmall() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"一月", "二月"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();

        CollectionUtils.addAll(serie1, new Number[]{120, 232});
        List<Number> serie2 = Lists.newArrayList();
        CollectionUtils.addAll(serie2, new Number[]{150, 100060});

        series.add(serie1);
        series.add(serie2);

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("bar");
        extendChartData.setTilte("用户流量趋势图");
        List<String> legends = Lists.newArrayList();
        legends.add("上行流量");
        legends.add("下行流量");
        // legends
        extendChartData.setLegends(legends);
        extendChartData.setDatas(datas);
        try {
            URL url = DynamicEngineTest.class.getClassLoader().getResource("");

            if (url != null) {
                String path = url.getPath();
                // convert
                String output = Converter.getInstance().convert(extendChartData);

                ChartUtils.generateImage(output,
                        path + File.separator + new Random().nextInt(100000) + ".png");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("system exit.");
    }
}