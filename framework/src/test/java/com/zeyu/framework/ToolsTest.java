package com.zeyu.framework;

import com.zeyu.framework.tools.CodeCounter;
import com.zeyu.framework.tools.PasswordGenerate;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;

/**
 * test count and password
 * Created by zeyuphoenix on 16/8/3.
 */
public class ToolsTest {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ToolsTest.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Test
    public void count() {
        CodeCounter codeCounter = new CodeCounter();
        codeCounter.printCountInfo();
    }

    @Test
    public void base64() {
        String info = "{key:1,value:'中国人'}";

        try {
            logger.info("info is : {}", info);
            logger.info("base64 encode is : {}", Base64.encodeBase64String(info.getBytes("UTF-8")));

            logger.info("base64 decode is : {}", new String(Base64.decodeBase64(Base64.encodeBase64String(info.getBytes("UTF-8")))));

        } catch (UnsupportedEncodingException e) {
            logger.error("", e);
        }
    }

    @Test
    public void password() {
        // 原始密码
        System.out.println("构造初始密码: ");
        String plainPassword = "123asd$%^";
        logger.info("plain password is : {}", plainPassword);

        // md5加密
        String md5Password = PasswordGenerate.md5(plainPassword);
        logger.info("md5 password is : {}", md5Password);

        // 散列后密码
        String shaPassword = PasswordGenerate.entryptPassword(md5Password);
        logger.info("sha-1 password is : {}", shaPassword);

        // 验证密码
        boolean result = PasswordGenerate.validatePassword(md5Password, shaPassword);
        logger.info("validate password is : {}", result);
    }

    // ================================================================
    // Private Methods
    // ================================================================

}
