package com.zeyu.framework.modules.sys.service;

import com.zeyu.framework.modules.sys.entity.Office;
import com.zeyu.framework.modules.sys.entity.User;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * office service test
 * Created by zeyuphoenix on 16/8/3.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@FixMethodOrder(MethodSorters.DEFAULT)   //按方法顺序执行
public class OfficeServiceTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(OfficeServiceTest.class);

    @Autowired
    private OfficeService officeService;

    @Test
    public void findList() throws Exception {

        Office office = new Office();
        User user = new User("1");
        user.setOffice(office);
        office.setCurrentUser(user);

        List<Office> officeList = officeService.findList(office);

        logger.info("find list is {}", officeList);
    }

    @Test
    public void save() throws Exception {

    }

    @Test
    public void delete() throws Exception {

    }

    @Test
    public void findAll() throws Exception {
        List<Office> officeList = officeService.findAll();

        logger.info("find list is {}", officeList);
    }

}