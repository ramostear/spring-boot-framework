package com.zeyu.framework.modules.common.openid.provider;

import org.junit.Test;

/**
 * auth weixin test
 * Created by zeyuphoenix on 2017/1/7.
 */
public class WeiXinAuthProviderTest {

    static final String key = "wx77f8b94169f171f4";
    static final String secret = "de7bcbe2c7d485deec79098d575aa31f";

    @Test
    public void auth() throws Exception {
      WeiXinAuthProvider weiXinAuthProvider = new WeiXinAuthProvider();
        weiXinAuthProvider.auth("snspai_userinfo", key, secret);
    }

}