#!/bin/sh
#
echo 'start tomcat service ...'

rm -rf ./tomcat/logs/*
rm -rf ./tomcat/temp/*
rm -rf ./tomcat/work/*

./tomcat/bin/catalina.sh start

