#!/bin/sh
#
# kill.sh: like kill but use process name as the parameter
# usage: kill.sh [-n] [-s signal] pname ...
# version: 1.0
# OS: AIX,HP-UX,Solaris,FreeBSD,Linux
# author: luojian(enigma1983@163.com)
# history:
# 	2008-10-24	1.0 released
#	2008-11-04	option [-v] changed to [-n]
#	2009-06-18	display process name
#
VIRTUAL="no"
SIGNAL="TERM"
while getopts :ns: arg
do
	case $arg in
	n)
		VIRTUAL="yes"
		;;
	s)
		SIGNAL="$OPTARG"
		;;
	:)
		printf "kill.sh: option requires an argument -- %s\n" "$OPTARG"
		printf "usage: kill.sh [-n] [-s signal] pname\n"
		exit 1
		;;
	?)
		printf "kill.sh: illegal option -- %s\n" "$OPTARG"
		printf "usage: kill.sh [-n] [-s signal] pname\n"
		exit 1
		;;
	esac
done
shift `expr $OPTIND - 1`
if [ $# -eq 0 ]
then
	printf "usage: kill.sh [-n] [-s signal] pname\n"
	exit 1
fi
OS=`uname -s`
while [ "$1" != "" ]
do
	case $OS in
		AIX|FreeBSD)
			list=`ps -o pid,comm|sed -n '2,$p'|grep $1|awk '{print $1"|"$2}'`
			;;
		HP-UX|SunOS|Linux)
			list=`ps -ef|grep $1|awk '{print $2"|"$8}'`
			;;
	esac
	for element in $list
	do
		pid=`echo $element|cut -d"|" -f1`
		if [ $VIRTUAL = "no" ]
		then
			kill -s $SIGNAL $pid
		fi
	done
	shift
done
