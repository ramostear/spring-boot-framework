/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50619
Source Host           : localhost:3306
Source Database       : framework

Target Server Type    : MYSQL
Target Server Version : 50619
File Encoding         : 65001

Date: 2016-03-10 16:23:33
*/

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME`    VARCHAR(120) NOT NULL,
  `TRIGGER_NAME`  VARCHAR(200) NOT NULL,
  `TRIGGER_GROUP` VARCHAR(200) NOT NULL,
  `BLOB_DATA`     BLOB,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME`    VARCHAR(120) NOT NULL,
  `CALENDAR_NAME` VARCHAR(200) NOT NULL,
  `CALENDAR`      BLOB         NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME`      VARCHAR(120) NOT NULL,
  `TRIGGER_NAME`    VARCHAR(200) NOT NULL,
  `TRIGGER_GROUP`   VARCHAR(200) NOT NULL,
  `CRON_EXPRESSION` VARCHAR(120) NOT NULL,
  `TIME_ZONE_ID`    VARCHAR(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME`        VARCHAR(120) NOT NULL,
  `ENTRY_ID`          VARCHAR(95)  NOT NULL,
  `TRIGGER_NAME`      VARCHAR(200) NOT NULL,
  `TRIGGER_GROUP`     VARCHAR(200) NOT NULL,
  `INSTANCE_NAME`     VARCHAR(200) NOT NULL,
  `FIRED_TIME`        BIGINT(13)   NOT NULL,
  `SCHED_TIME`        BIGINT(13)   NOT NULL,
  `PRIORITY`          INT(11)      NOT NULL,
  `STATE`             VARCHAR(16)  NOT NULL,
  `JOB_NAME`          VARCHAR(200) DEFAULT NULL,
  `JOB_GROUP`         VARCHAR(200) DEFAULT NULL,
  `IS_NONCONCURRENT`  VARCHAR(1)   DEFAULT NULL,
  `REQUESTS_RECOVERY` VARCHAR(1)   DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`, `INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`, `JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`, `TRIGGER_GROUP`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME`        VARCHAR(120) NOT NULL,
  `JOB_NAME`          VARCHAR(200) NOT NULL,
  `JOB_GROUP`         VARCHAR(200) NOT NULL,
  `DESCRIPTION`       VARCHAR(250) DEFAULT NULL,
  `JOB_CLASS_NAME`    VARCHAR(250) NOT NULL,
  `IS_DURABLE`        VARCHAR(1)   NOT NULL,
  `IS_NONCONCURRENT`  VARCHAR(1)   NOT NULL,
  `IS_UPDATE_DATA`    VARCHAR(1)   NOT NULL,
  `REQUESTS_RECOVERY` VARCHAR(1)   NOT NULL,
  `JOB_DATA`          BLOB,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`, `REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`, `JOB_GROUP`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` VARCHAR(120) NOT NULL,
  `LOCK_NAME`  VARCHAR(40)  NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('scheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME`    VARCHAR(120) NOT NULL,
  `TRIGGER_GROUP` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME`        VARCHAR(120) NOT NULL,
  `INSTANCE_NAME`     VARCHAR(200) NOT NULL,
  `LAST_CHECKIN_TIME` BIGINT(13)   NOT NULL,
  `CHECKIN_INTERVAL`  BIGINT(13)   NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME`      VARCHAR(120) NOT NULL,
  `TRIGGER_NAME`    VARCHAR(200) NOT NULL,
  `TRIGGER_GROUP`   VARCHAR(200) NOT NULL,
  `REPEAT_COUNT`    BIGINT(7)    NOT NULL,
  `REPEAT_INTERVAL` BIGINT(12)   NOT NULL,
  `TIMES_TRIGGERED` BIGINT(10)   NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME`    VARCHAR(120) NOT NULL,
  `TRIGGER_NAME`  VARCHAR(200) NOT NULL,
  `TRIGGER_GROUP` VARCHAR(200) NOT NULL,
  `STR_PROP_1`    VARCHAR(512)   DEFAULT NULL,
  `STR_PROP_2`    VARCHAR(512)   DEFAULT NULL,
  `STR_PROP_3`    VARCHAR(512)   DEFAULT NULL,
  `INT_PROP_1`    INT(11)        DEFAULT NULL,
  `INT_PROP_2`    INT(11)        DEFAULT NULL,
  `LONG_PROP_1`   BIGINT(20)     DEFAULT NULL,
  `LONG_PROP_2`   BIGINT(20)     DEFAULT NULL,
  `DEC_PROP_1`    DECIMAL(13, 4) DEFAULT NULL,
  `DEC_PROP_2`    DECIMAL(13, 4) DEFAULT NULL,
  `BOOL_PROP_1`   VARCHAR(1)     DEFAULT NULL,
  `BOOL_PROP_2`   VARCHAR(1)     DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME`     VARCHAR(120) NOT NULL,
  `TRIGGER_NAME`   VARCHAR(200) NOT NULL,
  `TRIGGER_GROUP`  VARCHAR(200) NOT NULL,
  `JOB_NAME`       VARCHAR(200) NOT NULL,
  `JOB_GROUP`      VARCHAR(200) NOT NULL,
  `DESCRIPTION`    VARCHAR(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` BIGINT(13)   DEFAULT NULL,
  `PREV_FIRE_TIME` BIGINT(13)   DEFAULT NULL,
  `PRIORITY`       INT(11)      DEFAULT NULL,
  `TRIGGER_STATE`  VARCHAR(16)  NOT NULL,
  `TRIGGER_TYPE`   VARCHAR(8)   NOT NULL,
  `START_TIME`     BIGINT(13)   NOT NULL,
  `END_TIME`       BIGINT(13)   DEFAULT NULL,
  `CALENDAR_NAME`  VARCHAR(200) DEFAULT NULL,
  `MISFIRE_INSTR`  SMALLINT(2)  DEFAULT NULL,
  `JOB_DATA`       BLOB,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`, `JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`, `CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`, `TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`, `TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`, `NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id`          VARCHAR(64)    NOT NULL
  COMMENT '编号',
  `value`       VARCHAR(100)   NOT NULL
  COMMENT '数据值',
  `label`       VARCHAR(100)   NOT NULL
  COMMENT '标签名',
  `type`        VARCHAR(100)   NOT NULL
  COMMENT '类型',
  `description` VARCHAR(100)   NOT NULL
  COMMENT '描述',
  `sort`        DECIMAL(10, 0) NOT NULL
  COMMENT '排序（升序）',
  `parent_id`   VARCHAR(64)             DEFAULT '0'
  COMMENT '父级编号',
  `fixed`   INTEGER(1)             DEFAULT '0'
  COMMENT '是否固定, 0默认为不固定，1固定；固定就不能再去修改',
  `create_by`   VARCHAR(64)    NOT NULL
  COMMENT '创建者',
  `create_date` DATETIME       NOT NULL
  COMMENT '创建时间',
  `update_by`   VARCHAR(64)    NOT NULL
  COMMENT '更新者',
  `update_date` DATETIME       NOT NULL
  COMMENT '更新时间',
  `remarks`     VARCHAR(255)            DEFAULT NULL
  COMMENT '备注信息',
  `del_flag`    CHAR(1)        NOT NULL DEFAULT '0'
  COMMENT '删除标记（0：正常；1：删除；2：审核）',
  PRIMARY KEY (`id`),
  KEY `sys_dict_value` (`value`),
  KEY `sys_dict_label` (`label`),
  KEY `sys_dict_del_flag` (`del_flag`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES
  ('1', '0', '正常', 'del_flag', '删除标记', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('2', '1', '删除', 'del_flag', '删除标记', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('3', '1', '显示', 'show_hide', '显示/隐藏', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('4', '0', '隐藏', 'show_hide', '显示/隐藏', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('5', '1', '是', 'yes_no', '是/否', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('6', '0', '否', 'yes_no', '是/否', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('7', 'menu', '菜单', 'menu_type', '菜单的操作类型', '10', '0', 0, '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('8', 'operation', '操作', 'menu_type', '菜单的操作类型', '20', '0', 0, '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('9', 'url', '链接', 'menu_type', '菜单的操作类型', '30', '0', 0, '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('10', '1', '公司', 'sys_office_type', '机构类型', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('11', '2', '部门', 'sys_office_type', '机构类型', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('12', '3', '小组', 'sys_office_type', '机构类型', '30', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('13', '4', '其它', 'sys_office_type', '机构类型', '40', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('14', '1', '一级', 'sys_office_grade', '机构等级', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('15', '2', '二级', 'sys_office_grade', '机构等级', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('16', '3', '三级', 'sys_office_grade', '机构等级', '30', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('17', '4', '四级', 'sys_office_grade', '机构等级', '40', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('18', '1', '系统管理', 'sys_user_type', '用户类型', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('19', '2', '审查用户', 'sys_user_type', '用户类型', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('20', '3', '普通用户', 'sys_user_type', '用户类型', '30', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('21', '1', '男', 'sex', '性别', '10', '0', 0, '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('22', '2', '女', 'sex', '性别', '20', '0', 0, '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('23', '1', '接入日志', 'sys_log_type', '日志类型', '30', '0', 0, '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('24', '2', '异常日志', 'sys_log_type', '日志类型', '40', '0', 0, '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('25', '0', '草稿', 'notify_status_type', '通知通告状态', '10', '0', 0, '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('26', '1', '发布', 'notify_status_type', '通知通告状态', '20', '0', 0, '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('27', '0', '未读', 'notify_read_type', '通知通告状态', '10', '0', 0, '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('28', '1', '已读', 'notify_read_type', '通知通告状态', '20', '0', 0, '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('29', '0', '发布', 'cms_del_flag', '内容状态', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('30', '1', '删除', 'cms_del_flag', '内容状态', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('31', '2', '审核', 'cms_del_flag', '内容状态', '15', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('32', '1', '咨询', 'cms_guestbook', '留言板分类', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('33', '2', '建议', 'cms_guestbook', '留言板分类', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('34', '3', '投诉', 'cms_guestbook', '留言板分类', '30', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('35', '4', '其它', 'cms_guestbook', '留言板分类', '40', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('36', '0', '无', 'schedule_status_type', '任务状态', '10', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('37', '1', '正常', 'schedule_status_type', '任务状态', '20', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('38', '2', '暂停', 'schedule_status_type', '任务状态', '30', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('39', '3', '已完成', 'schedule_status_type', '任务状态', '40', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('40', '4', '错误', 'schedule_status_type', '任务状态', '50', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_dict` VALUES
  ('41', '5', '锁定', 'schedule_status_type', '任务状态', '60', '0', 0, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');


-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id`          VARCHAR(64) NOT NULL
  COMMENT '编号',
  `type`        CHAR(1)      DEFAULT '1'
  COMMENT '日志类型',
  `title`       VARCHAR(255) DEFAULT ''
  COMMENT '日志标题',
  `create_by`   VARCHAR(64)  DEFAULT NULL
  COMMENT '创建者',
  `create_date` DATETIME     DEFAULT NULL
  COMMENT '创建时间',
  `remote_addr` VARCHAR(255) DEFAULT NULL
  COMMENT '操作IP地址',
  `user_agent`  VARCHAR(255) DEFAULT NULL
  COMMENT '用户代理',
  `request_uri` VARCHAR(255) DEFAULT NULL
  COMMENT '请求URI',
  `method`      VARCHAR(5)   DEFAULT NULL
  COMMENT '操作方式',
  `params`      TEXT COMMENT '操作提交的数据',
  `exception`   TEXT COMMENT '异常信息',
  PRIMARY KEY (`id`),
  KEY `sys_log_create_by` (`create_by`),
  KEY `sys_log_request_uri` (`request_uri`),
  KEY `sys_log_type` (`type`),
  KEY `sys_log_create_date` (`create_date`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id`          VARCHAR(64)    NOT NULL
  COMMENT '编号',
  `parent_id`   VARCHAR(64)    NOT NULL
  COMMENT '父级编号',
  `parent_ids`  VARCHAR(2000)  NOT NULL
  COMMENT '所有父级编号',
  `name`        VARCHAR(100)   NOT NULL
  COMMENT '名称',
  `sort`        DECIMAL(10, 0) NOT NULL
  COMMENT '排序',
  `href`        VARCHAR(2000)           DEFAULT NULL
  COMMENT '链接',
  `target`      VARCHAR(20)             DEFAULT NULL
  COMMENT '目标',
  `icon`        VARCHAR(100)            DEFAULT NULL
  COMMENT '图标',
  `is_show`     CHAR(1)        NOT NULL
  COMMENT '是否在菜单中显示',
  `permission`  VARCHAR(200)            DEFAULT NULL
  COMMENT '权限标识',
  `useable`     VARCHAR(64)           DEFAULT '1'
  COMMENT '是否可用',
  `create_by`   VARCHAR(64)    NOT NULL
  COMMENT '创建者',
  `create_date` DATETIME       NOT NULL
  COMMENT '创建时间',
  `update_by`   VARCHAR(64)    NOT NULL
  COMMENT '更新者',
  `update_date` DATETIME       NOT NULL
  COMMENT '更新时间',
  `attach`     VARCHAR(255)            DEFAULT NULL
  COMMENT '名称的高级描述效果',
  `remarks`     VARCHAR(255)            DEFAULT NULL
  COMMENT '备注信息',
  `del_flag`    CHAR(1)        NOT NULL DEFAULT '0'
  COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_menu_parent_id` (`parent_id`),
  KEY `sys_menu_del_flag` (`del_flag`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES
  ('1', '0', '0,', '功能菜单', '0', NULL, 'menu', NULL, '1', NULL, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '菜单的根节点,不可删除', '0');
INSERT INTO `sys_menu` VALUES
  ('2', '1', '0,1,', '系统设置', '1000000', NULL, 'menu', 'fa fa-cog', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', '<em class="bg-color-pink flash animated">!</em>', NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('3', '1', '0,1,', '工具管理', '100000', NULL, 'menu', 'fa fa-cubes', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('4', '1', '0,1,', '监控', '10000', NULL, 'menu', 'fa fa-desktop txt-color-blue', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('5', '2', '0,1,2,', '字典管理', '10', '/sys/dict/', 'menu', 'fa fa-th-list', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('6', '5', '0,1,2,5,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'sys:dict:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('7', '5', '0,1,2,5,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'sys:dict:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('8', '2', '0,1,2,', '日志查询', '20', '/sys/log', 'menu', 'fa fa-search', '1', NULL, '1',  '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', '<span class="badge pull-right inbox-badge bg-color-blue">10/p</span>', NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('9', '8', '0,1,2,8,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'sys:log:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('10', '2', '0,1,2,', '权限管理', '1000', NULL, 'menu', 'fa fa-globe txt-color-red', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('11', '10', '0,1,2,10,', '机构管理', '10', '/sys/office/', 'menu', 'fa fa-th-large', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('12', '11', '0,1,2,10,11,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'sys:office:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('13', '11', '0,1,2,10,11,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'sys:office:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('14', '10', '0,1,2,10,', '用户管理', '20', '/sys/user', 'menu', 'fa fa-user', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', '<span class="badge pull-right inbox-badge bg-color-magenta">admin</span>', NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('15', '14', '0,1,2,10,14,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'sys:user:view',  '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('16', '14', '0,1,2,10,14,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'sys:user:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('17', '10', '0,1,2,10,', '角色管理', '30', '/sys/role/', 'menu', 'fa fa-lock', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('18', '17', '0,1,2,10,17,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'sys:role:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('19', '17', '0,1,2,10,17,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'sys:role:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('20', '10', '0,1,2,10,', '菜单管理', '40', '/sys/menu/', 'menu', 'fa fa-list-alt', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('21', '20', '0,1,2,10,20,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'sys:menu:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('22', '20', '0,1,2,10,20,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'sys:menu:edit',  '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('23', '2', '0,1,2,', '关于帮助', '19841210', NULL, 'menu', 'fa fa-diamond txt-color-pink', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('24', '23', '0,1,2,23', '个人主页', '10', 'http://www.blogjava.net/zeyuphoenix', 'url', 'fa fa-internet-explorer', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('25', '23', '0,1,2,23', '友情赞助', '20', 'http://www.oschina.net', 'url', 'fa fa-cc-visa', '1', NULL, '1',  '1',  '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('26', '23', '0,1,2,23', '交流', '30', 'http://www.iteye.com', 'url', 'fa fa-feed', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('27', '3', '0,1,3,', '任务调度', '10', '/tools/schedule/', 'menu', 'fa fa-tasks', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', '<span class="badge inbox-badge bg-color-greenLight">quartz2.2</span>', NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('28', '27', '0,1,3,27,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'tools:schedule:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('29', '27', '0,1,3,27,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'tools:schedule:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('30', '3', '0,1,3,', '参数配置', '20', '/tools/param/', 'menu', 'fa fa-cogs', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('31', '30', '0,1,3,30,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'tools:param:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('32', '30', '0,1,3,30,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'tools:param:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('33', '3', '0,1,3,', '报表', '30', '/tools/report/', 'menu', 'fa fa-file-text', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('34', '33', '0,1,3,33,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'tools:report:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('35', '33', '0,1,3,33,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'tools:report:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('36', '3', '0,1,3,', '调试', '40', '/tools/debug/', 'menu', 'fa fa-bug txt-color-yellow', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', '<span class="badge pull-right inbox-badge bg-color-yellow">Careful</span>', NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('37', '36', '0,1,3,36,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'tools:debug:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('38', '36', '0,1,3,36,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'tools:debug:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('39', '3', '0,1,3,', 'REST工具', '50', '/tools/rest/', 'menu', 'fa fa-recycle', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('40', '39', '0,1,3,39,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'tools:rest:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('41', '39', '0,1,3,39,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'tools:rest:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('42', '4', '0,1,4,', '数据库', '10', NULL, 'menu', 'fa fa-database', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('43', '42', '0,1,4,42,', 'Mysql', '10', '/monitors/mysql/', 'menu', 'fa fa-maxcdn', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('44', '43', '0,1,4,42,43,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'mon:mysql:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('45', '43', '0,1,4,42,43,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'mon:mysql:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('46', '42', '0,1,4,42,', 'Redis', '20', '/monitors/redis/', 'menu', 'fa fa-cubes', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('47', '46', '0,1,4,42,46,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'mon:redis:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('48', '46', '0,1,4,42,46,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'mon:redis:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('49', '42', '0,1,4,42,', 'MongoDB', '30', '/monitors/mongodb/', 'menu', 'fa fa-hdd-o', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', '<span class="badge inbox-badge bg-color-greenLight">v3.x</span>', NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('50', '49', '0,1,4,42,49,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'mon:mongo:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('51', '49', '0,1,4,42,49,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'mon:mongo:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('52', '4', '0,1,4,', 'Server', '20', '/monitors/server/', 'menu', 'fa fa-linux', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('53', '52', '0,1,4,52,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'mon:server:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('54', '52', '0,1,4,52,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'mon:server:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('55', '4', '0,1,4,', 'Web服务器', '30', '/monitors/web/', 'menu', 'fa fa-cloud', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', '<span class="badge pull-right inbox-badge bg-color-purple">tomcat</span>', NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('56', '55', '0,1,4,55,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'mon:web:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('57', '55', '0,1,4,55,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'mon:web:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('58', '4', '0,1,4,', '网络设备', '40', '/monitors/device/', 'menu', 'fa fa-deaf', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('59', '58', '0,1,4,58,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'mon:device:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('60', '58', '0,1,4,58,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'mon:device:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');

INSERT INTO `sys_menu` VALUES
  ('61', '3', '0,1,3,', '终端', '60', '/tools/console/', 'menu', 'fa fa-terminal', '1', NULL, '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('62', '61', '0,1,3,61,', '查看', '10', NULL, 'operation', 'fa fa-eye', '1', 'tools:console:view', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');
INSERT INTO `sys_menu` VALUES
  ('63', '61', '0,1,3,61,', '修改', '20', NULL, 'operation', 'fa fa-edit', '1', 'tools:console:edit', '1',  '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, NULL, '0');


-- ----------------------------
-- Table structure for sys_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_office`;
CREATE TABLE `sys_office` (
  `id`             VARCHAR(64)    NOT NULL
  COMMENT '编号',
  `parent_id`      VARCHAR(64)    NOT NULL
  COMMENT '父级编号',
  `parent_ids`     VARCHAR(2000)  NOT NULL
  COMMENT '所有父级编号',
  `name`           VARCHAR(100)   NOT NULL
  COMMENT '名称',
  `sort`           DECIMAL(10, 0) NOT NULL
  COMMENT '排序',
  `code`           VARCHAR(100)            DEFAULT NULL
  COMMENT '区域编码',
  `type`           CHAR(1)        NOT NULL
  COMMENT '机构类型',
  `grade`          CHAR(1)        NOT NULL
  COMMENT '机构等级',
  `address`        VARCHAR(255)            DEFAULT NULL
  COMMENT '联系地址',
  `zip_code`       VARCHAR(100)            DEFAULT NULL
  COMMENT '邮政编码',
  `master`         VARCHAR(100)            DEFAULT NULL
  COMMENT '负责人',
  `phone`          VARCHAR(200)            DEFAULT NULL
  COMMENT '电话',
  `fax`            VARCHAR(200)            DEFAULT NULL
  COMMENT '传真',
  `email`          VARCHAR(200)            DEFAULT NULL
  COMMENT '邮箱',
  `USEABLE`        VARCHAR(64)             DEFAULT NULL
  COMMENT '是否启用',
  `PRIMARY_PERSON` VARCHAR(64)             DEFAULT NULL
  COMMENT '主负责人',
  `DEPUTY_PERSON`  VARCHAR(64)             DEFAULT NULL
  COMMENT '副负责人',
  `create_by`      VARCHAR(64)    NOT NULL
  COMMENT '创建者',
  `create_date`    DATETIME       NOT NULL
  COMMENT '创建时间',
  `update_by`      VARCHAR(64)    NOT NULL
  COMMENT '更新者',
  `update_date`    DATETIME       NOT NULL
  COMMENT '更新时间',
  `remarks`        VARCHAR(255)            DEFAULT NULL
  COMMENT '备注信息',
  `del_flag`       CHAR(1)        NOT NULL DEFAULT '0'
  COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_office_parent_id` (`parent_id`),
  KEY `sys_office_del_flag` (`del_flag`),
  KEY `sys_office_type` (`type`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '机构表';

-- ----------------------------
-- Records of sys_office
-- ----------------------------
INSERT INTO `sys_office` VALUES
  ('1', '0', '0,', '总公司', '10', '100000', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, '1',
                                                                         '2013-05-27 08:00:00', '1',
                                                                         '2013-05-27 08:00:00', '所有机构的根节点,不可删除', '0');
-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id`          VARCHAR(64)  NOT NULL
  COMMENT '编号',
  `name`        VARCHAR(100) NOT NULL
  COMMENT '角色名称',
  `enname`      VARCHAR(255)          DEFAULT NULL
  COMMENT '英文名称',
  `type`   VARCHAR(255)          DEFAULT NULL
  COMMENT '角色类型',
  `is_sys`      VARCHAR(64)           DEFAULT NULL
  COMMENT '是否系统数据',
  `useable`     VARCHAR(64)           DEFAULT NULL
  COMMENT '是否可用',
  `create_by`   VARCHAR(64)  NOT NULL
  COMMENT '创建者',
  `create_date` DATETIME     NOT NULL
  COMMENT '创建时间',
  `update_by`   VARCHAR(64)  NOT NULL
  COMMENT '更新者',
  `update_date` DATETIME     NOT NULL
  COMMENT '更新时间',
  `remarks`     VARCHAR(255)          DEFAULT NULL
  COMMENT '备注信息',
  `del_flag`    CHAR(1)      NOT NULL DEFAULT '0'
  COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_role_del_flag` (`del_flag`),
  KEY `sys_role_enname` (`enname`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES
  ('1', '系统管理', 'administrator', 'security-role', '1', '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');
INSERT INTO `sys_role` VALUES
  ('2', '普通用户', 'normal', 'user', '1', '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', NULL, '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` VARCHAR(64) NOT NULL
  COMMENT '角色编号',
  `menu_id` VARCHAR(64) NOT NULL
  COMMENT '菜单编号',
  PRIMARY KEY (`role_id`, `menu_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '角色-菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '10');
INSERT INTO `sys_role_menu` VALUES ('1', '11');
INSERT INTO `sys_role_menu` VALUES ('1', '12');
INSERT INTO `sys_role_menu` VALUES ('1', '13');
INSERT INTO `sys_role_menu` VALUES ('1', '14');
INSERT INTO `sys_role_menu` VALUES ('1', '15');
INSERT INTO `sys_role_menu` VALUES ('1', '16');
INSERT INTO `sys_role_menu` VALUES ('1', '17');
INSERT INTO `sys_role_menu` VALUES ('1', '18');
INSERT INTO `sys_role_menu` VALUES ('1', '19');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '20');
INSERT INTO `sys_role_menu` VALUES ('1', '21');
INSERT INTO `sys_role_menu` VALUES ('1', '22');
INSERT INTO `sys_role_menu` VALUES ('1', '23');
INSERT INTO `sys_role_menu` VALUES ('1', '24');
INSERT INTO `sys_role_menu` VALUES ('1', '25');
INSERT INTO `sys_role_menu` VALUES ('1', '26');
INSERT INTO `sys_role_menu` VALUES ('1', '27');
INSERT INTO `sys_role_menu` VALUES ('1', '28');
INSERT INTO `sys_role_menu` VALUES ('1', '29');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '30');
INSERT INTO `sys_role_menu` VALUES ('1', '31');
INSERT INTO `sys_role_menu` VALUES ('1', '32');
INSERT INTO `sys_role_menu` VALUES ('1', '33');
INSERT INTO `sys_role_menu` VALUES ('1', '34');
INSERT INTO `sys_role_menu` VALUES ('1', '35');
INSERT INTO `sys_role_menu` VALUES ('1', '36');
INSERT INTO `sys_role_menu` VALUES ('1', '37');
INSERT INTO `sys_role_menu` VALUES ('1', '38');
INSERT INTO `sys_role_menu` VALUES ('1', '39');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '40');
INSERT INTO `sys_role_menu` VALUES ('1', '41');
INSERT INTO `sys_role_menu` VALUES ('1', '42');
INSERT INTO `sys_role_menu` VALUES ('1', '43');
INSERT INTO `sys_role_menu` VALUES ('1', '44');
INSERT INTO `sys_role_menu` VALUES ('1', '45');
INSERT INTO `sys_role_menu` VALUES ('1', '46');
INSERT INTO `sys_role_menu` VALUES ('1', '47');
INSERT INTO `sys_role_menu` VALUES ('1', '48');
INSERT INTO `sys_role_menu` VALUES ('1', '49');
INSERT INTO `sys_role_menu` VALUES ('1', '5');
INSERT INTO `sys_role_menu` VALUES ('1', '50');
INSERT INTO `sys_role_menu` VALUES ('1', '51');
INSERT INTO `sys_role_menu` VALUES ('1', '52');
INSERT INTO `sys_role_menu` VALUES ('1', '53');
INSERT INTO `sys_role_menu` VALUES ('1', '54');
INSERT INTO `sys_role_menu` VALUES ('1', '55');
INSERT INTO `sys_role_menu` VALUES ('1', '56');
INSERT INTO `sys_role_menu` VALUES ('1', '57');
INSERT INTO `sys_role_menu` VALUES ('1', '58');
INSERT INTO `sys_role_menu` VALUES ('1', '59');
INSERT INTO `sys_role_menu` VALUES ('1', '6');
INSERT INTO `sys_role_menu` VALUES ('1', '60');
INSERT INTO `sys_role_menu` VALUES ('1', '61');
INSERT INTO `sys_role_menu` VALUES ('1', '62');
INSERT INTO `sys_role_menu` VALUES ('1', '63');
INSERT INTO `sys_role_menu` VALUES ('1', '7');
INSERT INTO `sys_role_menu` VALUES ('1', '8');
INSERT INTO `sys_role_menu` VALUES ('1', '9');
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '10');
INSERT INTO `sys_role_menu` VALUES ('2', '11');
INSERT INTO `sys_role_menu` VALUES ('2', '12');
INSERT INTO `sys_role_menu` VALUES ('2', '13');
INSERT INTO `sys_role_menu` VALUES ('2', '14');
INSERT INTO `sys_role_menu` VALUES ('2', '15');
INSERT INTO `sys_role_menu` VALUES ('2', '16');
INSERT INTO `sys_role_menu` VALUES ('2', '17');
INSERT INTO `sys_role_menu` VALUES ('2', '18');
INSERT INTO `sys_role_menu` VALUES ('2', '19');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '20');
INSERT INTO `sys_role_menu` VALUES ('2', '21');
INSERT INTO `sys_role_menu` VALUES ('2', '22');
INSERT INTO `sys_role_menu` VALUES ('2', '23');
INSERT INTO `sys_role_menu` VALUES ('2', '24');
INSERT INTO `sys_role_menu` VALUES ('2', '25');
INSERT INTO `sys_role_menu` VALUES ('2', '26');
INSERT INTO `sys_role_menu` VALUES ('2', '27');
INSERT INTO `sys_role_menu` VALUES ('2', '28');
INSERT INTO `sys_role_menu` VALUES ('2', '29');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '30');
INSERT INTO `sys_role_menu` VALUES ('2', '31');
INSERT INTO `sys_role_menu` VALUES ('2', '32');
INSERT INTO `sys_role_menu` VALUES ('2', '33');
INSERT INTO `sys_role_menu` VALUES ('2', '34');
INSERT INTO `sys_role_menu` VALUES ('2', '35');
INSERT INTO `sys_role_menu` VALUES ('2', '36');
INSERT INTO `sys_role_menu` VALUES ('2', '37');
INSERT INTO `sys_role_menu` VALUES ('2', '38');
INSERT INTO `sys_role_menu` VALUES ('2', '39');
INSERT INTO `sys_role_menu` VALUES ('2', '4');
INSERT INTO `sys_role_menu` VALUES ('2', '40');
INSERT INTO `sys_role_menu` VALUES ('2', '41');
INSERT INTO `sys_role_menu` VALUES ('2', '42');
INSERT INTO `sys_role_menu` VALUES ('2', '43');
INSERT INTO `sys_role_menu` VALUES ('2', '44');
INSERT INTO `sys_role_menu` VALUES ('2', '45');
INSERT INTO `sys_role_menu` VALUES ('2', '46');
INSERT INTO `sys_role_menu` VALUES ('2', '47');
INSERT INTO `sys_role_menu` VALUES ('2', '48');
INSERT INTO `sys_role_menu` VALUES ('2', '49');
INSERT INTO `sys_role_menu` VALUES ('2', '5');
INSERT INTO `sys_role_menu` VALUES ('2', '50');
INSERT INTO `sys_role_menu` VALUES ('2', '51');
INSERT INTO `sys_role_menu` VALUES ('2', '52');
INSERT INTO `sys_role_menu` VALUES ('2', '53');
INSERT INTO `sys_role_menu` VALUES ('2', '54');
INSERT INTO `sys_role_menu` VALUES ('2', '55');
INSERT INTO `sys_role_menu` VALUES ('2', '56');
INSERT INTO `sys_role_menu` VALUES ('2', '57');
INSERT INTO `sys_role_menu` VALUES ('2', '58');
INSERT INTO `sys_role_menu` VALUES ('2', '59');
INSERT INTO `sys_role_menu` VALUES ('2', '6');
INSERT INTO `sys_role_menu` VALUES ('2', '60');
INSERT INTO `sys_role_menu` VALUES ('2', '7');
INSERT INTO `sys_role_menu` VALUES ('2', '8');
INSERT INTO `sys_role_menu` VALUES ('2', '9');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id`          VARCHAR(64)  NOT NULL
  COMMENT '编号',
  `name`           VARCHAR(100)   NOT NULL
  COMMENT '名称',
  `office_id`   VARCHAR(64)  NOT NULL
  COMMENT '归属部门',
  `login_name`  VARCHAR(100) NOT NULL
  COMMENT '登录名',
  `password`    VARCHAR(100) NOT NULL
  COMMENT '密码',
  `no`          VARCHAR(100)          DEFAULT NULL
  COMMENT '工号',
  `sex`       INTEGER(1)          DEFAULT 0
  COMMENT '性别',
  `age`       VARCHAR(4)          DEFAULT 0
  COMMENT '年龄',
  `email`       VARCHAR(200)          DEFAULT NULL
  COMMENT '邮箱',
  `phone`       VARCHAR(200)          DEFAULT NULL
  COMMENT '电话',
  `mobile`      VARCHAR(200)          DEFAULT NULL
  COMMENT '手机',
  `type`   CHAR(1)               DEFAULT NULL
  COMMENT '用户类型',
  `photo`       VARCHAR(1000)         DEFAULT NULL
  COMMENT '用户头像',
  `login_ip`    VARCHAR(100)          DEFAULT NULL
  COMMENT '最后登陆IP',
  `login_date`  DATETIME              DEFAULT NULL
  COMMENT '最后登陆时间',
  `login_flag`  VARCHAR(64)           DEFAULT NULL
  COMMENT '是否可登录',
  `status`       INTEGER(1)          DEFAULT 0
  COMMENT '用户当前状态 (0:正常;1:锁定;2:已过期;)',
  `create_by`   VARCHAR(64)  NOT NULL
  COMMENT '创建者',
  `create_date` DATETIME     NOT NULL
  COMMENT '创建时间',
  `update_by`   VARCHAR(64)  NOT NULL
  COMMENT '更新者',
  `update_date` DATETIME     NOT NULL
  COMMENT '更新时间',
  `remarks`     VARCHAR(255)          DEFAULT NULL
  COMMENT '备注信息',
  `del_flag`    CHAR(1)      NOT NULL DEFAULT '0'
  COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_user_office_id` (`office_id`),
  KEY `sys_user_login_name` (`login_name`),
  KEY `sys_user_update_date` (`update_date`),
  KEY `sys_user_del_flag` (`del_flag`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES
  ('1', '系统管理员', '1',  'admin', 'ef323ef06bc6ed80e2e83b8f7f428cdce59f0e81fb5d9a320c6ca217', '0001',0 , 18,
        'zeyuphoenix@163.com', '12345678', '18515445588', '1', '',
                                                  '127.0.0.1', '2016-01-17 14:46:13', '1', 0, '1', '2013-05-27 08:00:00',
                                                  '1', '2015-11-17 17:40:00', '最高管理员', '0');

-- ----------------------------
-- Table structure for sys_user_login_provider
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_login_provider`;
CREATE TABLE `sys_user_login_provider` (
  `id`      VARCHAR(64)  NOT NULL
  COMMENT '编号',
  `user_id` VARCHAR(64) NOT NULL
  COMMENT '用户编号',
  `validate_id` VARCHAR(64) NOT NULL
  COMMENT '第三方编号',
  `type` VARCHAR(12)  DEFAULT ''
  COMMENT '类型(0:微信;1:QQ;2:新浪微博;3:Google;4:Yahoo;5:Hotmail;6:GitHub;7:OsChina)',
  `status`  INTEGER(1)  DEFAULT 0
  COMMENT '状态',
  `url` VARCHAR(255) DEFAULT ''
  COMMENT '第三方主页url地址',
  `create_date` DATETIME     NOT NULL
  COMMENT '创建时间',
  `remarks`     VARCHAR(255)          DEFAULT NULL
  COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `sys_user_provider_user_id` (`user_id`),
  KEY `sys_user_provider_provider_id` (`validate_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '用户-角色';

-- ----------------------------
-- Records of sys_user_login_provider
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` VARCHAR(64) NOT NULL
  COMMENT '用户编号',
  `role_id` VARCHAR(64) NOT NULL
  COMMENT '角色编号',
  PRIMARY KEY (`user_id`, `role_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '用户-角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');

-- ----------------------------
-- Table structure for sys_param
-- ----------------------------
DROP TABLE IF EXISTS `sys_param`;
CREATE TABLE `sys_param` (
  `id`         VARCHAR(64)    NOT NULL
  COMMENT '编号',
  `parent_id`  VARCHAR(64)    NOT NULL
  COMMENT '父级编号',
  `parent_ids` VARCHAR(2000)  NOT NULL
  COMMENT '所有父级编号',
  `name`       VARCHAR(100)   NOT NULL
  COMMENT '参数名称,一般作为key使用',
  `sort`       DECIMAL(10, 0) NOT NULL
  COMMENT '排序',
  `value`      VARCHAR(512)   NOT NULL
  COMMENT '参数值',
  `label`      VARCHAR(100)   NOT NULL
  COMMENT '显示名',
  `type`       VARCHAR(10)   NOT NULL
  COMMENT '类型',
  `description` VARCHAR(255)   NOT NULL
  COMMENT '描述',
  `icon`        VARCHAR(100)            DEFAULT NULL
  COMMENT '图标',
  `fixed`       INTEGER(1)             DEFAULT '0'
  COMMENT '是否固定, 0默认为不固定，1固定；固定就不能再去修改',
  `create_by`   VARCHAR(64)    NOT NULL
  COMMENT '创建者',
  `create_date` DATETIME       NOT NULL
  COMMENT '创建时间',
  `update_by`   VARCHAR(64)    NOT NULL
  COMMENT '更新者',
  `update_date` DATETIME       NOT NULL
  COMMENT '更新时间',
  `remarks`     VARCHAR(255)            DEFAULT NULL
  COMMENT '备注信息',
  `del_flag`    CHAR(1)        NOT NULL DEFAULT '0'
  COMMENT '删除标记（0：正常；1：删除；2：审核）',
  PRIMARY KEY (`id`),
  KEY `sys_param_name` (`name`),
  KEY `sys_param_parent_id` (`parent_id`),
  KEY `sys_param_del_flag` (`del_flag`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '系统参数配置表';

-- ----------------------------
-- Records of sys_param
-- ----------------------------

INSERT INTO `sys_param` VALUES ('1', '0', '0,', 'root', '10', '根节点', '参数配置', '1', '参数配置的根节点，不可删除',
                                     null, '1', '1', '2016-09-21 17:03:22', '1', '2016-09-21 17:03:28', null, '0');

-- ----------------------------
-- Table structure for tool_console_param
-- ----------------------------
DROP TABLE IF EXISTS `tool_console_param`;
CREATE TABLE `tool_console_param` (
  `id`         VARCHAR(64)    NOT NULL
  COMMENT '编号',
  `conn_id`       VARCHAR(100)   NOT NULL
  COMMENT '连接id',
  `value`      VARCHAR(512)    NOT NULL
  COMMENT '参数值',
  `label`      VARCHAR(100)    NOT NULL
  COMMENT '显示名',
  `type`       VARCHAR(20)     NOT NULL
  COMMENT '类型',
  `remarks`     VARCHAR(255)   DEFAULT NULL
  COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `tool_console_param_conn_id` (`conn_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = 'Console参数配置表';

-- ----------------------------
-- Records of tool_console_param
-- ----------------------------

-- ----------------------------
-- Table structure for monitor_mysql
-- ----------------------------
DROP TABLE IF EXISTS `monitor_mysql`;
CREATE TABLE `monitor_mysql` (
  `id` VARCHAR(64) NOT NULL COMMENT '编号',
  `time` DATETIME NOT NULL COMMENT '采集时间',
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '数据库状态，0:正常,1:异常',
  `thread_connections` INTEGER(4) NOT NULL DEFAULT '0' COMMENT '当前连接数',
  `max_connections` INTEGER(4) NOT NULL DEFAULT '0' COMMENT '最大连接数',
  `qps` FLOAT(10,0) DEFAULT '0' COMMENT '每秒sql执行数目',
  `tps` FLOAT(10,0) DEFAULT '0' COMMENT '每秒事务执行数',
  `key_hits` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT '缓存命中率',
  `key_read_hits` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT '缓存读命中率',
  `key_write_hits` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT '缓存写命中率',
  `send_bytes` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT '平均每秒钟的输入流量，单位为B',
  `receive_bytes` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT '平均每秒钟的输出流量，单位为B',
  PRIMARY KEY (`id`),
  KEY `monitor_mysql_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'Mysql监控数据表';

-- ----------------------------
-- Table structure for `monitor_server`
-- ----------------------------
DROP TABLE IF EXISTS `monitor_server`;
CREATE TABLE `monitor_server` (
  `id` VARCHAR(64) NOT NULL COMMENT '编号',
  `time` DATETIME NOT NULL COMMENT '采集时间',
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Server状态，0:正常,1:异常',
  `total_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '总物理内存',
  `used_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '已使用的物理内存',
  `free_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '剩余的物理内存',
  `used_memory_percent` BIGINT(20) DEFAULT '0' COMMENT '可使用内存',
  `actual_used_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '剩余内存',
  `max_memory` BIGINT(20) DEFAULT '0' COMMENT '最大可使用内存(MB)',
  `swap_total_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '总物理内存',
  `swap_used_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '已使用的物理内存',
  `swap_free_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '剩余的物理内存',
  `jvm_total_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'JVM总物理内存',
  `jvm_max_memory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'JVM最大物理内存',
  `jvm_free_emory` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'JVM剩余物理内存',
  PRIMARY KEY (`id`),
  KEY `monitor_server_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Server通用状态表';

-- ----------------------------
-- Table structure for `monitor_server_cpu`
-- ----------------------------
DROP TABLE IF EXISTS `monitor_server_cpu`;
CREATE TABLE `monitor_server_cpu` (
  `id` VARCHAR(64) NOT NULL COMMENT '编号',
  `server_id` VARCHAR(64) NOT NULL COMMENT 'Server编号',
  `time` DATETIME NOT NULL COMMENT '采集时间',
  `ind` INTEGER(4) NOT NULL DEFAULT '0' COMMENT 'cpu的索引',
  `cpu_ratio` FLOAT(10,4) NOT NULL DEFAULT '0.00' COMMENT 'CPU利用率',
  `cpu_sys_ratio` FLOAT(10,4) NOT NULL DEFAULT '0.00' COMMENT 'CPU SYS利用率',
  `cpu_usr_ratio` FLOAT(10,4) NOT NULL DEFAULT '0.00' COMMENT 'CPU USR利用率',
  PRIMARY KEY (`id`),
  KEY `monitor_mongodb_server_cpu_id` (`server_id`),
  KEY `monitor_server_cpu_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Server CPU状态表';

-- ----------------------------
-- Table structure for `monitor_mongodb`
-- ----------------------------
DROP TABLE IF EXISTS `monitor_mongodb`;
CREATE TABLE `monitor_mongodb` (
  `id` VARCHAR(64) NOT NULL COMMENT '编号',
  `time` DATETIME NOT NULL COMMENT '采集时间',
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '数据库状态，0:正常,1:异常',
  `mem_resident` bigint(14) NOT NULL DEFAULT '0' COMMENT '使用的物理内存大小(m)',
  `mem_virtual` bigint(14) NOT NULL DEFAULT '0' COMMENT '使用的虚拟内存大小(m)',
  `mem_mapped` bigint(14) NOT NULL DEFAULT '0' COMMENT '映射的内存大小(m)',
  `mem_mapped_with_journal` bigint(14) NOT NULL DEFAULT '0' COMMENT '具有日志的映射的内存大小',
  `connections_current` int(10) NOT NULL DEFAULT '0' COMMENT '当前连接数',
  `connections_available` int(10) NOT NULL DEFAULT '0' COMMENT '可用连接数',
  `extra_info_page_faults` bigint(10) NOT NULL DEFAULT '0' COMMENT '加载磁盘内容时发生页错误的次数',
  `network_bytes_in` bigint(10) NOT NULL DEFAULT '0' COMMENT '网络读取字节数(byte)',
  `network_bytes_out` bigint(10) NOT NULL DEFAULT '0' COMMENT '网络发送字节数(byte)',
  `network_num_requests` bigint(10) NOT NULL DEFAULT '0' COMMENT '网络请求数',
  `opcounters_insert` bigint(10) NOT NULL DEFAULT '0' COMMENT '插入操作数',
  `opcounters_query` bigint(10) NOT NULL DEFAULT '0' COMMENT '查询操作数',
  `opcounters_update` bigint(10) NOT NULL DEFAULT '0' COMMENT '更新操作数',
  `opcounters_delete` bigint(10) NOT NULL DEFAULT '0' COMMENT '删除操作数',
  `opcounters_command` bigint(10) NOT NULL DEFAULT '0' COMMENT '其它操作数',
  `index_counters_accesses` bigint(10) NOT NULL DEFAULT '0' COMMENT '访问索引次数',
  `index_counters_hits` bigint(10) NOT NULL DEFAULT '0' COMMENT '内存命中索引次数',
  `index_counters_misses` bigint(10) NOT NULL DEFAULT '0' COMMENT '内存丢失索引次数',
  `index_counters_resets` bigint(10) NOT NULL DEFAULT '0' COMMENT '索引计数器重置次数',
  PRIMARY KEY (`id`),
  KEY `monitor_mongodb_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'MongoDB监控数据表';

-- ----------------------------
-- Table structure for `monitor_redis`
-- ----------------------------
DROP TABLE IF EXISTS `monitor_redis`;
CREATE TABLE `monitor_redis` (
  `id` VARCHAR(64) NOT NULL COMMENT '编号',
  `time` DATETIME NOT NULL COMMENT '采集时间',
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '数据库状态，0:正常,1:异常',
  `used_cpu_sys` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT 'Redis服务器耗费的系统 CPU',
  `used_cpu_user` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT 'Redis服务器耗费的用户 CPU ',
  `used_memory` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT '由 Redis 分配器分配的内存总量，以字节（byte）为单位',
  `used_memory_rss` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT '从操作系统的角度，返回 Redis 已分配的内存总量（俗称常驻集大小）。这个值和 top 、 ps等命令的输出一致',
  `used_memorypeak` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT 'Redis 的内存消耗峰值（以字节为单位）',
  `mem_fragmentation_ratio` FLOAT(10,0) NOT NULL DEFAULT '0' COMMENT 'used_memory_rss 和 used_memory 之间的比率',
  `instantaneous_ops_per_sec` int(4) NOT NULL DEFAULT '0' COMMENT '服务器每秒钟执行的命令数量',
  `connected_clients` int(4) NOT NULL DEFAULT '0' COMMENT '已连接客户端的数量（不包括通过从属服务器连接的客户端）',
  `total_net_output_bytes` bigint(10) NOT NULL DEFAULT '0' COMMENT '总输出字节数',
  `total_net_input_bytes` bigint(10) NOT NULL DEFAULT '0' COMMENT '总输入字节数',
  PRIMARY KEY (`id`),
  KEY `monitor_redis_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'Redis监控数据表';

-- ----------------------------
-- Table structure for `monitor_webserver`
-- ----------------------------
DROP TABLE IF EXISTS `monitor_webserver`;
CREATE TABLE `monitor_webserver` (
  `id` VARCHAR(64) NOT NULL COMMENT '编号',
  `time` DATETIME NOT NULL COMMENT '采集时间',
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '数据库状态，0:正常,1:异常',
  `total_memory` bigint(14) NOT NULL DEFAULT '0' COMMENT '总共的物理内存大小(m)',
  `free_memory` bigint(14) NOT NULL DEFAULT '0' COMMENT '可用的内存大小(m)',
  `max_memory` bigint(14) NOT NULL DEFAULT '0' COMMENT '最大的内存大小(m)',
  `total_started_thread_count` bigint(14) NOT NULL DEFAULT '0' COMMENT '启动线程数',
  `max_threads` int(10) NOT NULL DEFAULT '0' COMMENT '最大线程数',
  `current_thread_count` int(10) NOT NULL DEFAULT '0' COMMENT '当前线程总数',
  `current_threads_busy` bigint(10) NOT NULL DEFAULT '0' COMMENT '繁忙线程数',
  `keep_alive_count` bigint(10) NOT NULL DEFAULT '0' COMMENT '保存连接线程数',
  `max_time` bigint(10) NOT NULL DEFAULT '0' COMMENT '最大执行时间',
  `processing_time` bigint(10) NOT NULL DEFAULT '0' COMMENT '执行时间',
  `request_count` bigint(10) NOT NULL DEFAULT '0' COMMENT '总请求数',
  `error_count` bigint(10) NOT NULL DEFAULT '0' COMMENT '错误请求数',
  `bytes_received` bigint(10) NOT NULL DEFAULT '0' COMMENT '接收字节数',
  `bytes_sent` bigint(10) NOT NULL DEFAULT '0' COMMENT '发送字节数',
  PRIMARY KEY (`id`),
  KEY `monitor_webserver_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'Web服务器监控数据表';

-- ----------------------------
-- Table structure for `tb_auto_test`
-- ----------------------------
DROP TABLE IF EXISTS `tb_auto_test`;
CREATE TABLE `tb_auto_test` (
  `id` VARCHAR(64) NOT NULL COMMENT '编号',
  `time` DATETIME NOT NULL COMMENT '时间',
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '状态，0:正常,1:异常',
  `total_count` bigint(14) NOT NULL DEFAULT '0' COMMENT '总数',
  `description` VARCHAR(255) DEFAULT NULL COMMENT '描述信息',
  `remarks` bigint(10) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `tb_auto_test_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '测试自动增删改查表';

-- ----------------------------
-- Table structure for `tool_schedule_history`
-- ----------------------------
DROP TABLE IF EXISTS `tool_schedule_history`;
CREATE TABLE `tool_schedule_history` (
  `id` VARCHAR(64) NOT NULL COMMENT '编号',
  `group` VARCHAR(40) NOT NULL DEFAULT '' COMMENT '任务所属组',
  `name` VARCHAR(40) NOT NULL DEFAULT '' COMMENT '任务名称',
  `start_time` DATETIME NOT NULL COMMENT '任务开始时间',
  `finish_time` DATETIME NOT NULL COMMENT '任务结束时间',
  `duration` int(4) NOT NULL DEFAULT '0' COMMENT '任务持续时间',
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '任务结果，0:正常,1:异常',
  `type` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '任务类型: 0: 周期,1:cron表达式',
  `cron` VARCHAR(40) DEFAULT NULL COMMENT '任务执行时间信息',
  `description` VARCHAR(255) DEFAULT NULL COMMENT '描述信息',
  PRIMARY KEY (`id`),
  KEY `tool_schedule_history_group_name` (`group`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '任务执行历史表';